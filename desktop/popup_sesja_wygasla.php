<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">         

      <section class="main" role="main">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="Form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>SESJA WYGASŁA</strong></div>
                        
                        <div class="fr-popup__text">
                        Nie odnotowaliśmy zakupu biletow w ciągu 40 minut.<br/>
                        Ze względów bezpieczeństwa Twoja sesja wygasła.
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn--half-popup btn btn--large btn--white">ZAMKNIJ</a>
                              <a href="#" class="form__btn--half-popup btn btn--large btn--brown">POWRÓT DO SKLEPU</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>
                    
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>