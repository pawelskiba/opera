let butikSettings = {
  searchReady: true
};

$(document).ready(function() {
  if (
    $(".butik_desc_head__main").length > 0 &&
    $(".butik_desc_head__main").hasClass("slick-initialized") === false
  ) {
    $(".butik_desc_head__main").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: ".butik_desc_head__thumbs"
    });
  }

  if (
    $(".butik_desc_head__thumbs").length > 0 &&
    $(".butik_desc_head__thumbs").hasClass("slick-initialized") === false
  ) {
    //console.log('test');
    $(".butik_desc_head__thumbs").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: ".butik_desc_head__main",
      dots: false,
      arrows: false,
      vertical: true,
      focusOnSelect: true
    });
  }

  if (
    $(".butik__main-slider").length > 0 &&
    $(".butik__main-slider").hasClass("slick-initialized") === false
  ) {
    $(".butik__main-slider").slick({
      infinite: false,
      slidesToShow: 1,
      dots: false,
      prevArrow:
        '<button type="button" class="slick-prev"><img src="/assets/images/arrow_white.png"></button>',
      nextArrow:
        '<button type="button" class="slick-next"><img src="/assets/images/arrow_white.png"></button>',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            infinite: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1
          }
        },
        {
          breakpoint: 300,
          settings: "unslick" // destroys slick
        }
      ]
    });
  }

  if (
    $(".butik__news__slider").length > 0 &&
    $(".butik__news__slider").hasClass("slick-initialized") === false
  ) {
    $(".butik__news__slider").slick({
      infinite: false,
      slidesToShow: 1,
      prevArrow:
        '<button type="button" class="slick-prev"><img src="/assets/images/arrow.png"></button>',
      nextArrow:
        '<button type="button" class="slick-next"><img src="/assets/images/arrow.png"></button>',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            infinite: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1
          }
        },
        {
          breakpoint: 300,
          settings: "unslick" // destroys slick
        }
      ]
    });
  }

  if (
    $(".butik_desc_head__main").length > 0 &&
    $(".butik_desc_head__main").hasClass("slick-initialized") === false
  ) {
    $(".butik_desc_head__main").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      arrows: true,
      prevArrow:
        '<button type="button" class="slick-prev"><img src="/assets/images/arrow.png"></button>',
      nextArrow:
        '<button type="button" class="slick-next"><img src="/assets/images/arrow.png"></button>'
    });
  }

  function showDialog(message, closeDialogCallback) {
    $("#popup-modal-content").html(message);
    $("#popup-modal").modal("show");

    if (closeDialogCallback && typeof closeDialogCallback === "function") {
      $("#popup-modal").on("hidden.bs.modal", closeDialogCallback);
    }
  }

  $(".butik__search__engine__cta").click(function() {
    let searchText = $(".butik__search__engine__text").val();
    if (!searchText || searchText === "") {
      showDialog("Podaj frazę do wyszukiwania", function() {
        $(".butik__search__engine__text").focus();
      });
    } else {
      searchText = searchText.replace(/\s/g, "%20");
      window.location.href = "/towary/index.html?q=" + searchText;
    }
  });

  $(".butik__search__engine__text").keyup(function(e) {
    if (parseInt(e.keyCode) === 13) {
      $(".butik__search__engine__cta").click();
      return;
    }

    let szukanaFraza = $(".butik__search__engine__text").val();

    if (szukanaFraza.length < 3) {
      $(".butik-search-hint").hide();
    }

    if (szukanaFraza.length >= 3 && butikSettings.searchReady === true) {
      butikSettings.searchReady = false;

      $.ajax({
        url: "/koszyk/index.html",
        method: "get",
        data: {
          json: true,
          ajax_action: "szukaj_frazy",
          szukanaFraza: szukanaFraza
        },
        success: function(result) {
          setTimeout(function() {
            butikSettings.searchReady = true;
          }, 700);
          if (result && result.status === "complete") {
            let opcje = "";
            if (result.data.length > 0) {
              $.each(result.data, function(index, row) {
                opcje +=
                  '<li class="butik-search-hint-item"><a href="/towary/szczegoly.html?id=' +
                  row.towarCmsId +
                  '">' +
                  row.nazwa +
                  "</a></li>";
              });
            }
            $(".butik-search-hint")
              .show()
              .css(
                "width",
                $(".butik__search__engine__text").width() + 34 + "px"
              )
              .html(opcje);
          }
        },
        error: function(xhr, status, error) {
          setTimeout(function() {
            butikSettings.searchReady = true;
          }, 700);
          console.error(status);
          console.error(error);
        }
      });
    }
  });
});
