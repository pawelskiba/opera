/**
 * SoftCOM Spółka jawna - http://www.softcom.wroc.pl
 * @author SoftCOM Spółka jawna <biuro@softcom.wroc.pl>
 * @copyright Copyright (C) 2011, SoftCOM Spółka jawna
 * $LastChangedDate$
 * $Rev$
 */

$(document).ready(function(){
	$.countdown.setDefaults({layout: '{mnn}:{snn}', expiryUrl: '/index/limit-czasu.html'});
	$('#clock').hover( function() { $('#block-clock-opis').fadeIn(); }, function() { $('#block-clock-opis').fadeOut(); } );
	delayedHideMessages();
});

/**
 * jQuery postJSON plugin
 */
jQuery.postJSON = function(u,d,callback) {
	return jQuery.post(u, d, callback, 'json');
}

/**
 * Funkcja detectCookies - sprawdzenie obsługi ciasteczek
 */
function detectCookies() {
	if (!navigator.cookieEnabled) {
		var parametry = 'browser=' + navigator.userAgent;
		document.location.href = '/brak-obslugi-ciasteczek.html?browser=' + navigator.userAgent;
	}
}

/**
 * Funkcja showMessage - wyświetla komunikat w podanej warstwie (divAppError, divAppWarning, divAppInformation)
 */
function showMessage(divName, message) {
	$('#' + divName).html(message);
	$('#' + divName).show();
	delayedHideMessages();
}

/**
 * Funkcja hideMessages - ukrywa warstwy komunikatów (divAppError, divAppWarning, divAppInformation)
 */
function hideMessages() {
	$('#messageError').hide();
	$('#messageWarning').hide();
	$('#messageInfo').hide();
}

/**
 * Funkcja delayedHideMessages - ukrywa komunikaty po 5 sekundach
 */
function delayedHideMessages() {
	window.setTimeout(hideMessages, 5000);
}