<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<div class="page__main">
   <section class="main" role="main">
      <div class="container">
         <nav class="butik__nav">
            <ul>
               <li><a href="#"><img src="../images-frogriot/icon-home.png" alt=""></a></li>
               <li><a href="#">Publikacje</a></li>
               <li><a href="#">Gadżety</a></li>
               <li><a href="#">Stacjonarnie</a></li>
               <li><a href="#">Moniuszko200</a></li>
               <li><a href="#">Dla dzieci</a></li>
               <li><a href="#">Plakaty</a></li>
               <li><a href="#">Bilety</a></li>
               
               <li><a href="#"><img src="../images-frogriot/icon-loupe.png" alt=""></a></li>
            </ul>
         </nav>
         <div class="butik__search__engine">
            <div class="container-inner">
               <input type="text" class="butik__search__engine__text">
               <button class="butik__search__engine__cta btn btn--brown btn--large">
               Szukaj
               </button>
            </div>
         </div>
         <div class="butik" style="background-color: #fff">
            <div class="butik__main-slider"  style="background: #22135c">
               <div class="butik__main-slider__item">
                  <div class="butik__main-slider__item__row">
                     <div class="img-box">
                        <img src="../images-frogriot/butik/butik-slider.png" alt="">
                     </div>
                     <div class="txt-box">
                        <div class="txt">Znajdź prezent na Święta Bożego Narodzenia</div>
                        <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
                     </div>
                  </div>
               </div>
               <div class="butik__main-slider__item">
                  <div class="butik__main-slider__item__row">
                     <div class="img-box">
                        <img src="../images-frogriot/butik/butik-slider.png" alt="">
                     </div>
                     <div class="txt-box">
                        <div class="txt">Znajdź prezent na Święta Bożego Narodzenia</div>
                        <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
                     </div>
                  </div>
               </div>
               <div class="butik__main-slider__item">
                  <div class="butik__main-slider__item__row">
                     <div class="img-box">
                        <img src="../images-frogriot/butik/butik-slider.png" alt="">
                     </div>
                     <div class="txt-box">
                        <div class="txt">Znajdź prezent na Święta Bożego Narodzenia</div>
                        <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
                     </div>
                  </div>
               </div>
               <div class="butik__main-slider__item">
                  <div class="butik__main-slider__item__row">
                     <div class="img-box">
                        <img src="../images-frogriot/butik/butik-slider.png" alt="">
                     </div>
                     <div class="txt-box">
                        <div class="txt">Znajdź prezent na Święta Bożego Narodzenia</div>
                        <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-inner">
               <div class="butik__news">
                  <h2>Bilety</h2>
                  <div class="butik__news__row">
                     <div class="butik__news__images">
                        <div class="butik__news__images__item"><img src="../images-frogriot/butik/akt_spektakl.jpg" alt=""></div>
                        <div class="butik__news__images__item"><img src="../images-frogriot/butik/akt_spektakl-02.jpg" alt=""></div>
                        <div class="butik__news__images__item"><img src="../images-frogriot/butik/akt_spektakl-03.jpg" alt=""></div>
                        <div class="butik__news__images__item"><img src="../images-frogriot/butik/akt_spektakl.jpg" alt=""></div>
                        <div class="butik__news__images__item"><img src="../images-frogriot/butik/akt_spektakl-02.jpg" alt=""></div>
                        <div class="butik__news__images__item"><img src="../images-frogriot/butik/akt_spektakl-03.jpg" alt=""></div>
                     </div>
                     <div class="butik__news__txt">
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan dolor sit amet lacus accumsan.
                        </p>
                        <a href="#">Zobacz wszystkie plakaty ></a>
                        <a href="#">Zobacz wszystkie programy ></a>
                        <a href="#">Zobacz wszystkie katalogi ></a>
                     </div>
                  </div>
               </div>
               <div class="butik__publikacje">
                  <h2>Publikacje</h2>
                  <div class="special__products">
                     <div class="row special__products__slider">
                        <div class="col-sm-4">
                           <div class="product__item product__item--bestseller">
                              <div class="product__item__image">
                                 <img src="../images-frogriot/butik/produkt-02.jpg" alt="">
                              </div>
                              <div class="product__item__header">
                                 <div class="header">
                                    Tomasz Ciecierski katalog wystawy
                                 </div>
                                 <div class="price">
                                    <div class="actual">125.00 PLN</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="product__item product__item--bestseller">
                              <div class="product__item__image">
                                 <img src="../images-frogriot/butik/produkt-03.jpg" alt="">
                              </div>
                              <div class="product__item__header">
                                 <div class="header">
                                    Pągowski w Operze katalog wystawy
                                 </div>
                                 <div class="price">
                                    <div class="disqount">125.00 PLN</div>
                                    <div class="actual">
                                       50.00 PLN
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="product__item product__item--bestseller">
                              <div class="product__item__image">
                                 <img src="../images-frogriot/butik/produkt-04.jpg" alt="">
                              </div>
                              <div class="product__item__header">
                                 <div class="header">
                                    Ryszard Winiarski katalogo wystawy
                                 </div>
                                 <div class="price">
                                    <div class="actual">
                                       125.00 PLN
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="product__item product__item--bestseller">
                              <div class="product__item__image">
                                 <img src="../images-frogriot/butik/produkt-02.jpg" alt="">
                              </div>
                              <div class="product__item__header">
                                 <div class="header">
                                    Tomasz Ciecierski katalog wystawy
                                 </div>
                                 <div class="price">
                                    <div class="actual">125.00 PLN</div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="product__item product__item--bestseller">
                              <div class="product__item__image">
                                 <img src="../images-frogriot/butik/produkt-03.jpg" alt="">
                              </div>
                              <div class="product__item__header">
                                 <div class="header">
                                    Pągowski w Operze katalog wystawy
                                 </div>
                                 <div class="price">
                                    <div class="disqount">125.00 PLN</div>
                                    <div class="actual">
                                       50.00 PLN
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="product__item product__item--bestseller">
                              <div class="product__item__image">
                                 <img src="../images-frogriot/butik/produkt-04.jpg" alt="">
                              </div>
                              <div class="product__item__header">
                                 <div class="header">
                                    Ryszard Winiarski katalogo wystawy
                                 </div>
                                 <div class="price">
                                    <div class="actual">
                                       125.00 PLN
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="special_edition">
                     <h2>Edycja limitowana</h2>
                     <div class="special_edition__box">
                        <div class="images">  
                           <img src="../images-frogriot/butik/napoj_milosny.jpg" alt="">
                           <img src="../images-frogriot/butik/napoj_milosny-02.jpg" alt="">
                        </div>
                        <div class="special_edition__items">
                           <div class="special_edition__item">
                              <div class="header">
                                 <div class="name">Miodownik</div>
                                 <div class="price">125.00 PLN</div>
                              </div>
                              <div class="txt">
                                 Inspirowany architekturą<br>
                                 plastra miodu
                              </div>
                           </div>
                           <div class="special_edition__item">
                              <div class="header">
                                 <div class="name">Napój Miłosny</div>
                                 <div class="price">55.00 PLN</div>
                              </div>
                              <div class="txt">
                                 Operowy miód wielokwiatowy<br> z teatralnej pasieki
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="shopping_box">
                     <div class="shopping_box__icons">
                        <h2>Łatwe zakupy online</h2>
                        <div class="items">
                           <div class="item">
                              <div class="img-box">
                                 <img src="../images-frogriot/butik/ikona-zakupy.jpg" alt="">
                              </div>
                              <div class="txt">Bezpieczne<br>zakupy</div>
                           </div>
                           <div class="item">
                              <div class="img-box">
                                 <img src="../images-frogriot/butik/ikona-samolot.jpg" alt="">
                              </div>
                              <div class="txt">Wysyłamy<br>wszędzie</div>
                           </div>
                           <div class="item">
                              <div class="img-box">
                                 <img src="../images-frogriot/butik/ikona-kalendarz.jpg" alt="">
                              </div>
                              <div class="txt">30 dni<br>na zwrot</div>
                           </div>
                           <div class="item">
                              <div class="img-box">
                                 <img src="../images-frogriot/butik/ikona-kciuk.jpg" alt="">
                              </div>
                              <div class="txt">Pozytywne opinie<br>klientów</div>
                           </div>
                        </div>
                     </div>
                     <div class="shopping_box__payments">
                        <h2>Metody płatności</h2>
                        <div class="icons">
                           <div class="item">
                              <img src="../images-frogriot/butik/ikona_platnosci.jpg" alt="">
                           </div>
                           <div class="item">
                              <img src="../images-frogriot/butik/ikona_platnosci-02.jpg" alt="">
                           </div>
                           <div class="item">
                              <img src="../images-frogriot/butik/ikona_platnosci-03.jpg" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="shooping__shop">
               <div class="img-box">    
                  <img src="../images-frogriot/butik/zakupy.jpg" alt="">
               </div>
               <div class="shopping__shop__txt">
                  <h2>Zakupy w naszym sklepie stacjonarnym</h2>
                  <p>
                     Lorem ipsum dolor sit amet, consectetur 
                     adipiscing elit. Aenean euismod bibendum 
                     laoreet. Proin gravida dolor sit amet lacus 
                     accumsan et viverra justo commodo. Proin
                     sodales pulvinar tempor. Cum sociis
                  </p>
               </div>
            </div>
         </div>
         <?php include 'include/footer-butik.php' ?>   
      </div>
   </section>
</div>