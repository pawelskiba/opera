<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">

      <section class="main" role="main">

         <div class="fr-wrapper">

             

            <div class="container fr-container">

               <!-- Nav tabs -->
               <ul class="nav nav-tabs cart-tabs" id="cartTab" role="tablist">
                  <li class="nav-item cart-tabs__item">
                     <a class="nav-link active" data-toggle="tab" href="#koszyk1" role="tab" aria-controls="koszyk1" aria-selected="true">
                        <span>
                           <span class="first">1.</span>
                           <span>TWÓJ KOSZYK</span>
                        </span>
                     </a>
                  </li>
                  <li class="nav-item cart-tabs__item">
                     <a class="nav-link" data-toggle="tab" href="#koszyk2" role="tab" aria-controls="koszyk2" aria-selected="false">
                        <span>
                           <span class="first">2.</span>
                           <span>DOSTAWA</span>
                        </span>
                     </a>
                  </li>
                  <li class="nav-item cart-tabs__item cart-tabs__item--last">
                     <a class="nav-link" data-toggle="tab" href="#koszyk3" role="tab" aria-controls="koszyk3" aria-selected="false">
                        <span>
                           <span class="first">3.</span>
                           <span>PODSUMOWANIE<br/>I PŁATNOŚĆ</span>
                        </span>
                     </a>
                  </li>
               </ul>

               <div class="lockbar">
                  <div class="lockbar__inner">
                     <div class="lockbar__clock">
                        <img src="../ikony/zegar.png" alt="ikona" />
                        <div>14:59</div>
                     </div>
                     <p>
                        Miejsca zostały obecnie zablokowane na potrzeby transakcji.<br/>
                        Po upływie 40 minut miejsca zostaną automatycznie odblokowane, a transakcję należy powtórzyć.
                     </p>
                  </div>
               </div>

               <!-- Tab panes -->
               <form class="cart-form">
                  <div class="tab-content">
                      <div class="container__narrow"> 
                     <div class="tab-pane fade show active" id="koszyk1" role="tabpanel" aria-labelledby="koszyk1">

                        <section>
                           <div class="cart-container">

                              <div class="form__section-header"><strong>Abonamenty</strong></div>

                              <div class="cart-prod">
                                 <div class="cart-prod__content">
                                    <h3>OPERA I BALET<br/>A TO POLSKA WŁAŚNIE</h3>
                                 </div>
                              </div>

                              <div class="cart-table">

                                 <div class="row cart-table__row">
                                    <div class="col-5 d-flex align-items-center">
                                       <div class="cart-table__sit"><strong>Liczba abonamentów</strong></div>
                                    </div>
                                    <div class="col-4 cart-table__discount-type d-flex align-items-center">
                                       <strong>Strefa cenowa</strong>
                                    </div>
                                    <div class="col-3 cart-table__price text-right d-flex align-items-center">
                                       <strong>Cena</strong>
                                    </div>
                                    <div class="col-12">
                                       <div class="cart-table__separator"></div>
                                    </div>
                                 </div>

                                 <div class="row cart-table__row">
                                    <div class="col-5 d-flex align-items-center">
                                       <div class="cart-table__sit cart-table__sit--icon">1</div>
                                    </div>
                                    <div class="col-4 d-flex align-items-center">
                                       I strefa
                                    </div>
                                    <div class="col-3 cart-table__price text-right d-flex align-items-center">
                                       <strong>250,00 PLN</strong>
                                       <a href="#"><img src="../ikony/usun.png" alt="ikona" /></a>
                                    </div>
                                 </div>

                                 <div class="row cart-table__row">
                                    <div class="col-5 d-flex align-items-center">
                                       <div class="cart-table__sit cart-table__sit--icon">2</div>
                                    </div>
                                    <div class="col-4 d-flex align-items-center">
                                       I strefa
                                    </div>
                                    <div class="col-3 cart-table__price text-right d-flex align-items-center">
                                       <strong>250,00 PLN</strong>
                                       <a href="#"><img src="../ikony/usun.png" alt="ikona" /></a>
                                    </div>
                                 </div>

                              </div>

                              <div class="row cart-table__summary cart-table__summary--mb-0">
                                 <div class="col-12">
                                    <div class="cart-table__separator-2"></div>
                                 </div>
                                 <div class="col-6 cart-table__sit">
                                    <strong>Suma</strong>
                                 </div>
                                 <div class="col-6 text-right cart-table__price">
                                    <strong>500,00 PLN</strong>
                                 </div>
                              </div>

                              <div class="row subscription">

                                 <div class="col-10 offset-1">
                                 
                                    <h3 class="subscription__header">Spektakle w ramach abonamentu</h3>

                                    <div class="subscription__item">
                                    
                                       <div class="cart-prod">
                                          <div class="cart-prod__thumb">
                                             <img src="pliki-serwer/jezioro_labedzie.jpg" alt="miniaturka" />
                                          </div>
                                          <div class="cart-prod__content">
                                             <h3>JEZIORO ŁABĘDZIE</h3>
                                             <p>22 sierpnia 2018, poniedziałek 19:00</p>
                                             <p>Warszawa, sala Moniuszki</p>
                                          </div>
                                       </div>

                                       <div class="cart-table">

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit"><strong>Miejsce</strong></div>
                                             </div>
                                             <div class="col-12">
                                                <div class="cart-table__separator"></div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit cart-table__sit--icon">Balkon I, loża I, miejsce 3</div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit cart-table__sit--icon">Balkon I, loża I, miejsce 4</div>
                                             </div>
                                          </div>

                                       </div>

                                    </div>

                                    <div class="subscription__item">
                                    
                                       <div class="cart-prod">
                                          <div class="cart-prod__thumb">
                                             <img src="pliki-serwer/czarodziejski_flet.png" alt="miniaturka" />
                                          </div>
                                          <div class="cart-prod__content">
                                             <h3>CZARODZIEJSKI FLET</h3>
                                             <p>22 sierpnia 2018, poniedziałek 19:00</p>
                                             <p>Warszawa, sala Moniuszki</p>
                                          </div>
                                       </div>

                                       <div class="cart-table">

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit"><strong>Miejsce</strong></div>
                                             </div>
                                             <div class="col-12">
                                                <div class="cart-table__separator"></div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit">
                                                   <span class="cart-error"><strong>Wskazane przez Ciebie miejsce jest już zajęte. Proszę wybrać inne.</strong></span>
                                                </div>
                                             </div>
                                          </div>

                                       </div>

                                    </div>

                                    <div class="subscription__item">
                                    
                                       <div class="cart-prod">
                                          <div class="cart-prod__thumb">
                                             <img src="pliki-serwer/dziadek_do_orzechow_i_krol_myszy.png" alt="miniaturka" />
                                          </div>
                                          <div class="cart-prod__content">
                                             <h3>DZIADEK DO ORZECHÓW I KRÓL MYSZY</h3>
                                             <p>22 sierpnia 2018, poniedziałek 19:00</p>
                                             <p>Warszawa, sala Moniuszki</p>
                                          </div>
                                       </div>

                                       <div class="cart-table">

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit"><strong>Miejsce</strong></div>
                                             </div>
                                             <div class="col-12">
                                                <div class="cart-table__separator"></div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit cart-table__sit--icon">Balkon I, loża I, miejsce 3</div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit cart-table__sit--icon">Balkon I, loża I, miejsce 4</div>
                                             </div>
                                          </div>

                                       </div>

                                    </div>

                                    <div class="subscription__item">
                                    
                                       <div class="cart-prod">
                                          <div class="cart-prod__thumb">
                                             <img src="pliki-serwer/cud_albo_krakowiaki_i_gorale.png" alt="miniaturka" />
                                          </div>
                                          <div class="cart-prod__content">
                                             <h3>CUD ALBO KRAKOWIAKI I GÓRALE</h3>
                                             <p>22 sierpnia 2018, poniedziałek 19:00</p>
                                             <p>Warszawa, sala Moniuszki</p>
                                          </div>
                                       </div>

                                       <div class="cart-table">

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit"><strong>Miejsce</strong></div>
                                             </div>
                                             <div class="col-12">
                                                <div class="cart-table__separator"></div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit cart-table__sit--icon">Balkon I, loża I, miejsce 3</div>
                                             </div>
                                          </div>

                                          <div class="row cart-table__row">
                                             <div class="col-12 d-flex align-items-center">
                                                <div class="cart-table__sit cart-table__sit--icon">Balkon I, loża I, miejsce 4</div>
                                             </div>
                                          </div>

                                       </div>

                                    </div>

                                    <div class="subscription__gift">
                                       <img src="pliki-serwer/prezent.png" alt="prezent" />
                                       <p>
                                          Przy zakupie abonamentu każdy widz otrzymuję książkę sezonu oraz program <br/>
                                          przedstawień repertuarowych zgodnie z wybranym abonamentem.
                                       </p>
                                    </div>

                                 </div>

                              </div>

                           </div>
                        </section>

                        <section class="recprod">
                           <div class="cart-container">
                              <div class="form__section-header"><strong>Rekomendowane produkty</strong></div>

                              <div class="recprod__inner">

                                 <div class="recprod__item">
                                    <div class="recprod__thumb"><img src="pliki-serwer/produkt-2.jpg" alt="produkt" /></div>
                                    <div class="recprod__title">JEZIORO ŁABĘDZIE'17 - program</div>
                                    <div class="recprod__price">20,00 PLN</div>
                                    <a class="fr-add" href="#">
                                       <span class="fr-add__icon"></span>
                                       Dodaj do zamówienia
                                    </a>
                                 </div>

                                 <div class="recprod__item">
                                    <div class="recprod__thumb"><img src="pliki-serwer/produkt-1.jpg" alt="produkt" /></div>
                                    <div class="recprod__title">JEZIORO ŁABĘDZIE'17 - plakat</div>
                                    <div class="recprod__price">45,00 PLN</div>
                                    <a class="fr-add" href="#">
                                       <span class="fr-add__icon"></span>
                                       Dodaj do zamówienia
                                    </a>
                                 </div>

                                 <div class="recprod__item">
                                    <div class="recprod__thumb"><img src="pliki-serwer/produkt-3.jpg" alt="produkt" /></div>
                                    <div class="recprod__title">PASTOR</div>
                                    <div class="recprod__price">125,00 PLN</div>
                                    <a class="fr-add" href="#">
                                       <span class="fr-add__icon"></span>
                                       Dodaj do zamówienia
                                    </a>
                                 </div>

                              </div>

                           </div>
                        </section>

                        <section class="ordersumm">
                           <div class="cart-container">
                              <div class="form__section-header"><strong>Podsumowanie zamówienia</strong></div>

                              <div class="row">
                                 <div class="col">
                                    <a class="fr-add fr-add--border-margin" href="#">
                                       <span class="fr-add__icon"></span>
                                       Dodaj kupon rabatowy
                                    </a>
                                 </div>
                              </div>

                              <div class="cart-hideprice">
                                 <div class="checkbox">
                                    <label class="checkbox__inner checkbox__inner--center">
                                       <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                       <span class="checkbox__checkmark"></span>
                                       <div class="checkbox__content checkbox__content--small">Ukryj cenę na biletach<br/>Zamówione bilety nie będą zawierać informacji o cenie wybranego spektaklu.</div>
                                    </label>
                                 </div>
                              </div> 

                              <div class="ordersumm__row-wrp">
                                 <div class="row">
                                    <div class="col-8">
                                       Prowizja za obsługę transakcji
                                    </div>
                                    <div class="col-4 text-right">
                                       <strong>2,00 PLN</strong>
                                    </div>
                                 </div>
                              </div>

                              <div class="ordersumm__row-wrp">
                                 <div class="row">
                                    <div class="col-8">
                                      <strong>Rabat okolicznościowy</strong>
                                    </div>
                                    <div class="col-4 text-right">
                                       <strong>-100,00 PLN</strong>
                                    </div>
                                 </div>
                              </div>

                              <div class="ordersumm__row-wrp ordersumm__row-wrp--color">
                                 <div class="row">
                                    <div class="col-8">
                                       <strong>Do zapłaty</strong>
                                    </div>
                                    <div class="col-4 text-right">
                                       <strong>731,00 PLN</strong>
                                    </div>
                                 </div>
                              </div>

                              <div class="row ordersumm__btns">
   
                                 <div class="col-6 pr-30">
                                    <a href="#" class="btn btn--large btn--brown">ZALOGUJ SIĘ</a>
                                 </div>

                                 <div class="col-6 pl-30">
                                    <a href="#" class="btn btn--large btn--gray">KUP GOŚCINNIE BEZ REJESTRACJI</a>
                                 </div>

                                 <div class="col-6 pr-30">
                                    <div class="pb-10">Nie masz konta?</div>
                                    <a href="#" class="btn btn--large btn--white">ZAREJESTRUJ SIĘ</a>
                                 </div>

                              </div>

                           </div>
                        </section>

                     </div>
                     <div class="tab-pane fade" id="koszyk2" role="tabpanel" aria-labelledby="koszyk2">koszyk2</div>
                     <div class="tab-pane fade" id="koszyk3" role="tabpanel" aria-labelledby="koszyk3">koszyk3</div>
                      </div>      
                  </div>
               </form>

            </div>

          </div>
                    
      </section>
          
      </div>      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>