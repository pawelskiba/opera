<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">     

      <section class="main" role="main">

         <!--   -->

         <div class="container">

         <nav class="butik__nav">
                      <ul>
                          <li><a href="#"><img src="../images-frogriot/icon-home.png" alt=""></a></li>
                          <li><a href="#">Publikacje</a></li>
                          <li><a href="#">Gadżety</a></li>
                          <li><a href="#">Stacjonarnie</a></li>
                          <li><a href="#">Moniuszko200</a></li>
                          <li><a href="#">Dla dzieci</a></li>
                          <li><a href="#">Plakaty</a></li>
                          <li><a href="#">Bilety</a></li>
                          
                          <li><a href="#"><img src="../images-frogriot/icon-loupe.png" alt=""></a></li>
                      </ul>
                  </nav>
                
                <div class="container__inner">
                    
                    <div class="container__inner__gap container__inner__gap--vertical">

                  <h1 class="fr-h1 fr-h1--nmb">Zarejestruj się już dziś, a zyskasz.</h1>

                  <ul class="form__list">
                     <li>szybszy dostęp do biletów i abonamentów</li>
                     <li>dostęp do promocji</li>
                     <li>dostęp do historii zamówień</li>
                  </ul>

                  <!-- Dane kupującego -->
                  <div class="form__section">

                     <div class="form__section-header"><strong>Dane Kupującego</strong></div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>IMIĘ</strong></label>
                           <input class="form__input" type="text" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>NAZWISKO</strong></label>
                           <input class="form__input" type="text" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>ADRES E-MAIL</strong></label>
                           <input class="form__input" type="email" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>HASŁO</strong></label>
                           <input class="form__input" type="password" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>NUMER TELEFONU</strong></label>
                           <input class="form__input" type="text" />
                        </div>
                     </div>

                  </div>

                  <!-- Zgody prawne -->
                  <div class="form__section form__section--no-border">
                     
                     <div class="form__section-header"><strong>Zgody prawne</strong></div>

                     <div class="checkbox">
                        <h3 class="checkbox__header">Zgoda na przetwarzanie danych osobowych</h3>
                        <label class="checkbox__inner">
                           <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                           <span class="checkbox__checkmark"></span>
                           <div class="checkbox__content">Wyrażam zgodę na przechowywanie, przetwarzanie oraz przekazywanie organizatorowi spektaklu / imprezy podanych wyżej danych osobowych z zastrzeżeniem wykorzystania jedynie do realizacji niniejszej rezerwacji.</div>
                        </label>
                     </div>

                     <div class="checkbox">
                        <h3 class="checkbox__header">Regulamin</h3>
                        <label class="checkbox__inner checkbox__inner--center">
                           <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                           <span class="checkbox__checkmark"></span>
                           <div class="checkbox__content">Oświadczam, że zapoznałem/am się z <a href="#"><strong>Regulaminem sprzedaży online</strong></a> i akceptuję jego ustalenia.</div>
                        </label>
                     </div>

                     <div class="checkbox">
                        <h3 class="checkbox__header">Regulamin sklepu</h3>
                        <label class="checkbox__inner checkbox__inner--center">
                           <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                           <span class="checkbox__checkmark"></span>
                           <div class="checkbox__content">Akceptuję <a href="#">Regulamin sklepu</a>.</div>
                        </label>
                     </div>

                  </div>

                  <!-- Przyciski -->
                  <div class="form__section form__section--last">
                     <div class="form__btns">
                        <a href="#" class="form__btn--fullw btn btn--large btn--brown">ZAREJESTRUJ SIĘ</a>
                     </div>
                  </div>

               </form> 

            </div>
                </div>
         </div>
           
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>