<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">

      <section class="main" role="main">

         <div class="fr-wrapper">

             

            <div class="container fr-container">

               <div class="summary__order">
                   
                     <div class="summary__order__head">
                         <div class="logo">
                             <img src="../images-frogriot/logo_black.png" alt="">
                         </div>
                         <div class="header">Dziękujemy<br> za złożenie zamówienia</div>
                         <div class="note">
                             Twoje zamówienie zostało zarejestrowane w naszym systemie pod numerem <span>202073734737</span> na łączną kwotę <span><b>965,00 PLN</b></span>
                         </div>
                     </div>

                     <div class="fr-label fr-summ-header">PODSUMOWANIE ZAMÓWIENIA</div>

                     <!-- bilety -->
                     <div class="fr-summ">

                        <div class="fr-label">Bilety</div>

                        <div class="row">
                           <div class="col-7"><strong>Balet: Jezioro Łabędzie</strong></div>
                           <div class="col-2 text-right"><strong>Ilość</strong></div>
                           <div class="col-3 text-right"><strong>Cena</strong></div>
                        </div>

                        <div class="row fr-mb-15">
                           <div class="col-7">Normalny</div>
                           <div class="col-2 text-right">2</div>
                           <div class="col-3 text-right">60,00 PLN</div>
                        </div>

                        <div class="row">
                           <div class="col-9">Prowizja za obsługę transakcji</div>
                           <div class="col-3 text-right">2,00 PLN</div>
                           <div class="w-100 fr-summ-sepa"></div>
                        </div>

                        <div class="row">
                           <div class="col-9"><strong>Suma</strong></div>
                           <div class="col-3 text-right"><strong>62,00 PLN</strong></div>
                        </div>

                        <div class="row">
                           <div class="col-9">Rabat</div>
                           <div class="col-3 text-right">-20,00 PLN</div>
                           <div class="w-100 fr-summ-sepa ="></div>
                        </div>

                        <div class="row">
                           <div class="col-6"><strong>Suma po rabacie</strong></div>
                           <div class="col-6 text-right"><strong>42,00 PLN</strong></div>
                        </div>

                     </div>

                     <!-- abonamenty -->
                     <div class="fr-summ">

                        <div class="fr-label">Abonamenty</div>

                        <div class="row">
                           <div class="col-7"><strong>Opera i balet: a to Polska właśnie</strong></div>
                           <div class="col-2 text-right"><strong>Ilość</strong></div>
                           <div class="col-3 text-right"><strong>Cena</strong></div>
                        </div>

                        <div class="row">
                           <div class="col-7">Stefa I</div>
                           <div class="col-2 text-right">2</div>
                           <div class="col-3 text-right">500,00 PLN</div>
                           <div class="w-100 fr-summ-sepa"></div>
                        </div>

                        <div class="row">
                           <div class="col-6"><strong>Suma</strong></div>
                           <div class="col-6 text-right"><strong>500,00 PLN</strong></div>
                        </div>

                     </div>

                     <!-- Wycieczki -->
                     <div class="fr-summ">

                        <div class="fr-label">Wycieczki</div>

                        <div class="row">
                           <div class="col-7"><strong>22 Sierpnia 2018, poniedziałek 19:00</strong></div>
                           <div class="col-2 text-right"><strong>Ilość</strong></div>
                           <div class="col-3 text-right"><strong>Cena</strong></div>
                        </div>

                        <div class="row">
                           <div class="col-7">Normalny</div>
                           <div class="col-2 text-right">2</div>
                           <div class="col-3 text-right">60,00 PLN</div>
                        </div>

                        <div class="row">
                           <div class="col-7">Ulgowy</div>
                           <div class="col-2 text-right">2</div>
                           <div class="col-3 text-right">300,00 PLN</div>
                        </div>

                        <div class="row">
                           <div class="col-7">Dla opiekuna</div>
                           <div class="col-2 text-right">1</div>
                           <div class="col-3 text-right">0 PLN</div>
                           <div class="w-100 fr-summ-sepa"></div>
                        </div>

                        <div class="row">
                           <div class="col-6"><strong>Suma</strong></div>
                           <div class="col-6 text-right"><strong>360,00 PLN</strong></div>
                        </div>

                     </div>

                     <!-- Produkty -->
                     <div class="fr-summ fr-summ--last">

                        <div class="fr-label">Produkty</div>

                        <div class="row">
                           <div class="col-7">Miodownik</div>
                           <div class="col-2 text-right">1</div>
                           <div class="col-3 text-right">20,00 PLN</div>
                        </div>

                        <div class="row">
                           <div class="col-7">JEZIORO ŁABĘDZIE '17 - program</div>
                           <div class="col-2 text-right">2</div>
                           <div class="col-3 text-right">45,00 PLN</div>
                        </div>

                        <div class="row">
                           <div class="col-9">Przesyłka kurierska UPC</div>
                           <div class="col-3 text-right">15,00 PLN</div>
                           <div class="w-100 fr-summ-sepa"></div>
                        </div>

                        <div class="row">
                           <div class="col-9"><strong>Suma</strong></div>
                           <div class="col-3 text-right"><strong>65,00 PLN</strong></div>
                           <div class="w-100 fr-summ-sepa fr-summ-sepa--x2"></div>
                        </div>

                     </div>

                     <!-- Zapłacono -->
                     <div class="row">
                        <div class="col-8 fr-label">Zapłacono (w tym VAT)</div>
                        <div class="col-4 fr-label text-right">967,00 PLN</div>
                     </div>
                   
                   <div class="summary__order__foot">
                       <div class="address">
                           <div class="header">Dane do wysyłki produktu</div>
                           <div class="content">
                               Jan kowalski<br>
                               ul. Szkolna 12<br>
                               15-649<br>
                               Warszawa
                           </div>       
                       </div> 
                       <div class="note">
                           W <b>załączniku wiadomości znajdują się bilety</b> prosimy o wydrukowanie ich i okazanie biletów przed wejściem na wydarzenie oraz dokument zakupu.<br><br>
                           
                           <b>Ważne informacje dotyczące wizyty w Teatrze Wielkim:</b>
                           
                           <ul class="summary__order__foot__list">
                               <li>
                                   Bądź pnuktualny, osoby, które spóźnią się na wydarzenie będą wpuszczne dopiero po przerwie
                               </li>
                               <li>
                                   Jeżeli bilet jest zakupiony ze zniżką nie zapomnij wziąć dokumentu uprawniającego do zakupu tej zniżki np: legitymadcja szkolna, karta dużej rodziny.
                               </li>
                               <li>
                                   Jeżeli potrzebujesz specjalnych udogodnień to poinformuj o tym telefonicznie Biuro Obsługi Widza i zadzoń na numer XXXX lub napisz e-maila VVVVVV, być może będziemy w stanie je spełnić.
                               </li>
                               <li>
                                   Pamiętaj, że Teatr Wielki dysponuje specjalnymi poduszkami na siedzenia, które dzieciom lub niewysokim osobom umożlwiają lepszy widok ze sceny. Ich liczba jest ograniczona. Zaptyaj o nie w szatni.
                               </li>
                           </ul>
                           
                           
                           
                           Pozdrawiamy serdecznie,<br>
                           Zespół Teatru Wielkiego
                       </div>
                       <div class="data__contact">
                           <div class="header">Dane kontaktowe</div>
                           <div class="content">
                               Teatr Wielki - Opera Narodowa<br>
                               Plac Teatralny 1, 00-950 Warszawa
                               Telefon: +48 22 826 50 19<br>
                               E-mail: <a href="mailto:office@teatrwielki.pl">office@teatrwielki.pl</a>
                           </div>
                       </div>
                   </div>    

                  </div>

            </div>

          </div>
                    
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>