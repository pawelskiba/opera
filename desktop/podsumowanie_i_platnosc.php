<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">    

      <section class="main" role="main">

         <div class="fr-wrapper">

             

            <div class="container fr-container">

               <!-- Nav tabs -->
               <ul class="nav nav-tabs cart-tabs" id="cartTab" role="tablist">
                  <li class="nav-item cart-tabs__item">
                     <a class="nav-link" data-toggle="tab" href="#koszyk1" role="tab" aria-controls="koszyk1" aria-selected="true">
                        <span>
                           <span class="first">1.</span>
                           <span>TWÓJ KOSZYK</span>
                        </span>
                     </a>
                  </li>
                  <li class="nav-item cart-tabs__item">
                     <a class="nav-link" data-toggle="tab" href="#koszyk2" role="tab" aria-controls="koszyk2" aria-selected="false">
                        <span>
                           <span class="first">2.</span>
                           <span>DOSTAWA</span>
                        </span>
                     </a>
                  </li>
                  <li class="nav-item cart-tabs__item cart-tabs__item--last">
                     <a class="nav-link active" data-toggle="tab" href="#koszyk3" role="tab" aria-controls="koszyk3" aria-selected="true">
                        <span>
                           <span class="first">3.</span>
                           <span>PODSUMOWANIE<br/>I PŁATNOŚĆ</span>
                        </span>
                     </a>
                  </li>
               </ul>

               <div class="lockbar">
                  <div class="lockbar__inner">
                     <div class="lockbar__clock">
                        <img src="../ikony/zegar.png" alt="ikona" />
                        <div>14:59</div>
                     </div>
                     <p>
                        Miejsca zostały obecnie zablokowane na potrzeby transakcji.<br/>
                        Po upływie 40 minut miejsca zostaną automatycznie odblokowane, a transakcję należy powtórzyć.
                     </p>
                  </div>
               </div>

               <!-- Tab panes -->
               <form class="form--cart">
                  <div class="tab-content">
                     <div class="container__inner__gap container__inner__gap--left">    
                     <div class="tab-pane fade" id="koszyk1" role="tabpanel" aria-labelledby="koszyk1"></div>
                     <div class="tab-pane fade" id="koszyk2" role="tabpanel" aria-labelledby="koszyk2"></div>
                     <div class="tab-pane fade show active" id="koszyk3" role="tabpanel" aria-labelledby="koszyk3">
                        <div class="cart-container">
                              
                           <div class="row">

                              <div class="col-6">

                                 <!-- Dane kupującego -->
                                 <div class="form__section">

                                    <div class="form__section-header"><strong>Dane Kupującego</strong></div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>IMIĘ</strong></label>
                                          <div class="form__output">Maria Anna</div>
                                       </div>
                                       <div class="form__col form__col--lastname">
                                          <label class="form__label form__label--nmb"><strong>NAZWISKO</strong></label>
                                          <div class="form__output">Kowalska</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>ADRES E-MAIL</strong></label>
                                          <div class="form__output">mariakowalska@gmail.com</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>NUMER TELEFONU</strong>&nbsp;(opcjonalnie)</label>
                                          <div class="form__output">501 265 897</div>
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Dane do faktury -->
                                 <div class="form__section">

                                    <div class="form__section-header"><strong>Faktura VAT</strong></div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>NAZWA FIRMY</strong></label>
                                          <div class="form__output">Kovalsky Sp. z o.o</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>NIP</strong></label>
                                          <div class="form__output">99 00 343 565</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                                          <div class="form__output">Wrocławska</div>
                                       </div>
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                                          <div class="form__output">12/13</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                                          <div class="form__output">156-40</div>
                                       </div>
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                                          <div class="form__output">Warszawa</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                                          <div class="form__output">Polska</div>
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Dostawa produktów -->
                                 <div class="form__section form__section--no-border">

                                    <div class="form__section-header"><strong>Dostawa produktów</strong></div>

                                    <div class="delivery">
                                       <img class="delivery__img" src="../ikony/icon-tickets.svg" alt="Bilety" />
                                       <div>
                                          <div class="delivery__text">Bilety otrzymasz drogą mailową na adres podany w formularzu.</div>
                                          <div class="delivery__text delivery__text--second">
                                             Po otrzymaniu biletów prosimy o kontakt telefoniczny lub e-mailowy z Biurem Obsługi Widowni w celu
                                             dostosowania komfortu oglądania spektaklu.
                                          </div>
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Sposoby dostawy produktów -->
                                 <div class="form__section form__section--no-border">

                                    <div class="form__section-header"><strong>SPOSOBY DOSTAWY PRODUKTÓW Z BUTIKU</strong></div>

                                    <label class="radiobox radiobox--fake">
                                       <div class="radiobox__content">
                                          <strong>Przesyłka kurierska UPC (+15,00 PLN)</strong><br/>
                                          Tylko na terenie Polski. Doręczenie w ciągu kolejnego dnia roboczego od daty nadania.
                                       </div>
                                    </label>

                                 </div>
                                 
                                 <!-- Adres dostawy -->
                                 <div class="form__section form__section--no-border form__section--no-margin-bottom">

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>IMIĘ I NAZWISKO/NAZWA</strong></label>
                                          <div class="form__output">Maria Anna Kowalska</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>IMIĘ I NAZWISKO/NAZWA CD.</strong></label>
                                          <div class="form__output">Kovalsky Sp. z o.o</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                                          <div class="form__output">Krucza</div>
                                       </div>
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                                          <div class="form__output">5/76</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                                          <div class="form__output">005-41</div>
                                       </div>
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                                          <div class="form__output">Warszawa</div>
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                                          <div class="form__output">Polska</div>
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Przyciski -->
                                 <div class="form__section form__section--last">
                                    <div class="form__btns align-items-start">
                                       <a href="#" class="form__btn--50 btn btn--large btn--white">COFNIJ</a>
                                       <div class="form__btn--50">
                                          <a href="#" class="btn btn--large btn--brown">KUPUJĘ I PŁACĘ</a>
                                          <div class="redirect-text">Nastąpi przekierowanie do <br/>Przelewy24.pl</div>
                                       </div>
                                    </div>
                                 </div>

                              </div>

                              <div class="col-5 offset-1">

                                 <div class="fr-label fr-summ-header">PODSUMOWANIE ZAMÓWIENIA</div>

                                 <!-- bilety -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Bilety</div>

                                    <div class="row">
                                       <div class="col-7"><strong>Balet: Jezioro Łabędzie</strong></div>
                                       <div class="col-2 text-right"><strong>Ilość</strong></div>
                                       <div class="col-3 text-right"><strong>Cena</strong></div>
                                    </div>

                                    <div class="row fr-mb-15">
                                       <div class="col-7">Normalny</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">60,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9">Prowizja za obsługę transakcji</div>
                                       <div class="col-3 text-right">2,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9"><strong>Suma</strong></div>
                                       <div class="col-3 text-right"><strong>62,00 PLN</strong></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9">Rabat</div>
                                       <div class="col-3 text-right">-20,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa ="></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-6"><strong>Suma po rabacie</strong></div>
                                       <div class="col-6 text-right"><strong>42,00 PLN</strong></div>
                                    </div>

                                 </div>

                                 <!-- abonamenty -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Abonamenty</div>

                                    <div class="row">
                                       <div class="col-7"><strong>Opera i balet: a to Polska właśnie</strong></div>
                                       <div class="col-2 text-right"><strong>Ilość</strong></div>
                                       <div class="col-3 text-right"><strong>Cena</strong></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Stefa I</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">500,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-6"><strong>Suma</strong></div>
                                       <div class="col-6 text-right"><strong>500,00 PLN</strong></div>
                                    </div>

                                 </div>

                                 <!-- Wycieczki -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Wycieczki</div>

                                    <div class="row">
                                       <div class="col-7"><strong>22 Sierpnia 2018, poniedziałek 19:00</strong></div>
                                       <div class="col-2 text-right"><strong>Ilość</strong></div>
                                       <div class="col-3 text-right"><strong>Cena</strong></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Normalny</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">60,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Ulgowy</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">300,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Dla opiekuna</div>
                                       <div class="col-2 text-right">1</div>
                                       <div class="col-3 text-right">0 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-6"><strong>Suma</strong></div>
                                       <div class="col-6 text-right"><strong>360,00 PLN</strong></div>
                                    </div>

                                 </div>

                                 <!-- Produkty -->
                                 <div class="fr-summ fr-summ--last">

                                    <div class="fr-label">Produkty</div>

                                    <div class="row">
                                       <div class="col-7">Miodownik</div>
                                       <div class="col-2 text-right">1</div>
                                       <div class="col-3 text-right">20,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">JEZIORO ŁABĘDZIE '17 - program</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">45,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9">Przesyłka kurierska UPC</div>
                                       <div class="col-3 text-right">15,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9"><strong>Suma</strong></div>
                                       <div class="col-3 text-right"><strong>65,00 PLN</strong></div>
                                       <div class="w-100 fr-summ-sepa fr-summ-sepa--x2"></div>
                                    </div>

                                 </div>
                                  
                                 <div class="fr-summ">
                                     <!-- Zapłacono -->
                                     <div class="row">
                                        <div class="col-8 fr-label">Do zapłaty (w tym VAT)</div>
                                        <div class="col-4 fr-label text-right">967,00 PLN</div>
                                     </div>
                                 </div> 
                                  
                                <div class="pb-10">Wiadomość dla sprzedającego:</div>  
                                <textarea class="form__textarea" rows="6"></textarea>
                                 
                              </div>

                           </div>

                        </div>
                     </div>
                      </div>     
                  </div>
               </form>

            </div>

          </div>
                    
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>