<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<div class="page__main">
   <section class="main" role="main">
      <div class="container">
         <div class="tour__main-header">
            <h1>Kalendarium</h1>
            <ul class="category-list category-list-season" aria-label="Sezony">
               <li class="selected"><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/09">2017/2018</a></li>
            </ul>
         </div>
         <div class="tour__head">
            <div class="tour__filter__header">
               <div class="tour__filter__header__label">Miesiące</div>
               <ul class="category-list category-list-months" aria-label="Miesiące">
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/09">wrzesień</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/10">październik</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/11">listopad</a>
                  </li>
                  <li class="selected"><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/12">grudzień</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/01">styczeń</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/02">luty</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/03">marzec</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/04">kwiecień</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/05">maj</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/06">czerwiec</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/07">lipiec</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/08">sierpień</a>
                  </li>
               </ul>
            </div>
            <div class="tour__filter__header">
               <div class="tour__filter__header__label">Dni</div>
               <ul id="events-days-list" class="month-list" tabindex="-1" aria-hidden="true">
                  <li class="saturday" data-day="1"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-01">1</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="2"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-02">2</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="3"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-03">3</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday" data-day="4"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-04">4</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="5"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-05">5</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="6"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-06">6</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="7"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-07">7</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="8"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-08">8</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="9"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-09">9</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="10"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-10">10</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday first last selected" data-day="11"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-11">11</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="12"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-12">12</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="13"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-13">13</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="14"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-14">14</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="15"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-15">15</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="16"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-16">16</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="17"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-17">17</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday" data-day="18"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-18">18</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="19"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-19">19</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="20"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-20">20</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="21"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-21">21</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="22"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-22">22</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="23"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-23">23</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="24"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-24">24</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday" data-day="25"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-25">25</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="26"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-26">26</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="27"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-27">27</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="28"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-28">28</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="29"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-29">29</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="30"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-30">30</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="31"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-31">31</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
               </ul>
            </div>
            <div class="tour__filter__header">
               <div class="tour__filter__header__label">Filtry</div>
               <ul id="events-category-list" class="category-list" aria-label="Kategorie">
                  <li class="data-category-1"><a href="#/1">Wycieczki indywidualne</a></li>
                  <li class="data-category-2"><a href="#/2">Wycieczki grupowe</a></li>
               </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
               <section id="content">
                  <ul id="events-list" class="month-full-list">
                     <li class="data-multiple data-event data-day-1 data-category-7 data-category-2 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">1</span>
                        <span class="weekday">sobota</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-1 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/PADEREWSKI045_kalendarium_370px.jpg" alt="Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/termin/2018-12-01_11-00/">
                                       <!--CONTENT_replaced-->Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">11:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Galeria Opera</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Warsztaty przy wystawie I.J. Paderewski. Anatomia geniuszu
                                    <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/termin/2018-12-01_11-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-1 data-category-2 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Darkness_mini__1_18_19.jpg" alt="Darkness"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-01_19-00/">
                                       <!--CONTENT_replaced-->Darkness<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Balet</span> / <span class="hall"><a href="https://teatrwielki.pl/teatr/miejsce/sale/sala-mlynarskiego/">Sala Młynarskiego</a></span></p>
                                 <h4>Bach, Vivaldi, Glass, Amar / Izadora Weiss</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Balet w&nbsp;dwóch częściach | Libretto: Izadora Weiss <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-01_19-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Darkness</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-2 data-category-7 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">2</span>
                        <span class="weekday">niedziela</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-2 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/PADEREWSKI045_kalendarium_370px.jpg" alt="Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/termin/2018-12-02_11-00/">
                                       <!--CONTENT_replaced-->Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">11:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Galeria Opera</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Warsztaty przy wystawie I.J. Paderewski. Anatomia geniuszu
                                    <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/termin/2018-12-02_11-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-2 data-category-1 premiere hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Krol_Roger_miniaturka.jpg" alt="Król Roger"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-02_18-00/">
                                       <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                                    </a>
                                    <span class="badge">Premiera</span>
                                 </h3>
                                 <p><span class="hour">18:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Karol Szymanowski</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-02_18-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Król Roger</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-4 data-category-4 data-category-7 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">4</span>
                        <span class="weekday">wtorek</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-4 data-category-4 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/opera-w-kadrze/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/opera_w_kadrze_mini_.jpg" alt="Opera w kadrze"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/opera-w-kadrze/termin/2018-12-04_12-00/">
                                       <!--CONTENT_replaced-->Opera w&nbsp;kadrze<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">12:00</span> / <span class="category">Wydarzenia specjalne, Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4>Projekt edukacyjny</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced--><!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/opera-w-kadrze/termin/2018-12-04_12-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Opera w kadrze</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-4 data-category-1 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Krol_Roger_miniaturka.jpg" alt="Król Roger"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-04_19-00/">
                                       <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Karol Szymanowski</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-04_19-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Król Roger</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-event data-day-5 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">5</span>
                        <span class="weekday">środa</span>
                        </time>
                        <div class="event-img">
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/madame_miniatura_do_lkalendarium.jpg" alt="Madame Butterfly"></a>
                        </div>
                        <div class="event">
                           <h3>
                              <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/termin/2018-12-05_19-00/">
                                 <!--CONTENT_replaced-->Madame Butterfly<!--CONTENT_replaced-->
                              </a>
                           </h3>
                           <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                           <h4>Giacomo Puccini</h4>
                           <p class="teaser">
                              <!--CONTENT_replaced-->Tragedia japońska w&nbsp;trzech aktach | Libretto: Giuseppe Giacosa, Luigi Illica | Oryginalna włoska wersja językowa z&nbsp;polskimi napisami<!--CONTENT_replaced-->
                           </p>
                        </div>
                        <div class="price">
                           <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/termin/2018-12-05_19-00/">Szczegóły</a>
                        </div>
                     </li>
                     <li class="data-event data-day-6 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">6</span>
                        <span class="weekday">czwartek</span>
                        </time>
                        <div class="event-img">
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Krol_Roger_miniaturka.jpg" alt="Król Roger"></a>
                        </div>
                        <div class="event">
                           <h3>
                              <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-06_19-00/">
                                 <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                              </a>
                           </h3>
                           <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                           <h4>Karol Szymanowski</h4>
                           <p class="teaser">
                              <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                           </p>
                        </div>
                        <div class="price">
                           <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-06_19-00/">Szczegóły</a>
                        </div>
                     </li>
                     <li class="data-multiple data-event data-day-7 data-category-2 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">7</span>
                        <span class="weekday">piątek</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-7 data-category-2 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Darkness_mini__1_18_19.jpg" alt="Darkness"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-07_18-00/">
                                       <!--CONTENT_replaced-->Darkness<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">18:00</span> / <span class="category">Balet</span> / <span class="hall"><a href="https://teatrwielki.pl/teatr/miejsce/sale/sala-mlynarskiego/">Sala Młynarskiego</a></span></p>
                                 <h4>Bach, Vivaldi, Glass, Amar / Izadora Weiss</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Balet w&nbsp;dwóch częściach | Libretto: Izadora Weiss <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-07_18-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Darkness</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-7 data-category-1 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/madame_miniatura_do_lkalendarium.jpg" alt="Madame Butterfly"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/termin/2018-12-07_19-00/">
                                       <!--CONTENT_replaced-->Madame Butterfly<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Giacomo Puccini</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Tragedia japońska w&nbsp;trzech aktach | Libretto: Giuseppe Giacosa, Luigi Illica | Oryginalna włoska wersja językowa z&nbsp;polskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/termin/2018-12-07_19-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Madame Butterfly</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-8 data-category-4 data-category-7 data-category-2 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">8</span>
                        <span class="weekday">sobota</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-8 data-category-4 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-doroslych/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/UW_dorosli_mini_.jpg" alt="Uniwersytet wiedzy operowej dla dorosłych"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-doroslych/termin/2018-12-08_10-00/">
                                       <!--CONTENT_replaced-->Uniwersytet wiedzy operowej dla dorosłych<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">10:00</span> / <span class="category">Wydarzenia specjalne</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Program edukacyjny; wykład dla dorosłych<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-doroslych/termin/2018-12-08_10-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Uniwersytet wiedzy operowej dla dorosłych</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-8 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-dzieci/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/UW_dla_dzieci_mini_.jpg" alt="Uniwersytet wiedzy operowej dla dzieci"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-dzieci/termin/2018-12-08_12-00/">
                                       <!--CONTENT_replaced-->Uniwersytet wiedzy operowej dla dzieci<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">12:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Program edukacyjny; wykład dla dzieci w&nbsp;wieku 10-13 lat<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-dzieci/termin/2018-12-08_12-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Uniwersytet wiedzy operowej dla dzieci</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-8 data-category-2 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Darkness_mini__1_18_19.jpg" alt="Darkness"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-08_18-00/">
                                       <!--CONTENT_replaced-->Darkness<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">18:00</span> / <span class="category">Balet</span> / <span class="hall"><a href="https://teatrwielki.pl/teatr/miejsce/sale/sala-mlynarskiego/">Sala Młynarskiego</a></span></p>
                                 <h4>Bach, Vivaldi, Glass, Amar / Izadora Weiss</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Balet w&nbsp;dwóch częściach | Libretto: Izadora Weiss <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-08_18-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Darkness</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-8 data-category-1 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/Krol_Roger_miniaturka.jpg" alt="Król Roger"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-08_19-00/">
                                       <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Karol Szymanowski</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-08_19-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Król Roger</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-9 data-category-7 data-category-3 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">9</span>
                        <span class="weekday">niedziela</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-9 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/poranek-muzyczny/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/poranek_muzyczny_mini.jpg" alt="Poranek muzyczny"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/poranek-muzyczny/termin/2018-12-09_12-00/">
                                       <!--CONTENT_replaced-->Poranek muzyczny<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">12:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Niedzielne spotkania dla dzieci łączące koncert z&nbsp;opowieściami o&nbsp;instrumentach i&nbsp;kompozytorach, a&nbsp;także zagadkami muzycznymi<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/poranek-muzyczny/termin/2018-12-09_12-00/">Szczegóły<span class="visuallyhidden"> wydarzenia Poranek muzyczny</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-9 data-category-3 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/bmw-art-club/" tabindex="-1"><img src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/bmw_jazz_club_2018_kwadrat.jpg" alt="BMW Art Club"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/bmw-art-club/termin/2018-12-09_19-00/">
                                       <!--CONTENT_replaced-->BMW Art Club<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Koncert</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Koncert muzyki K. Pendereckiego, W. Lutosławskiego, H. M. Góreckiego i&nbsp;A. Panufnika
                                    <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn" href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/bmw-art-club/termin/2018-12-09_19-00/">Szczegóły<span class="visuallyhidden"> wydarzenia BMW Art Club</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-event data-day-11 data-category-2 data-event__tour" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">22</span>
                        <span class="weekday">piątek</span>
                        </time>
                        <div class="data-event__events">
                           <div class="data-event__item">
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                                       <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                                    </a>
                                    <div class="data-event__hour">
                                       9:15
                                    </div>
                                 </h3>
                              </div>
                              <div class="price data-event__price">
                                 <a class="btn btn--brown data-event__button" href="#">kup bilet</a>
                                 <div class="data-event__available_tickets"></div>
                              </div>
                           </div>
                           <div class="data-event__item">
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                                       <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                                    </a>
                                    <div class="data-event__hour">
                                       9:15
                                    </div>
                                 </h3>
                              </div>
                              <div class="price data-event__price">
                                 <a class="btn btn--brown data-event__button" href="#">kup bilet</a>
                                 <div class="data-event__available_tickets"></div>
                              </div>
                           </div>
                           <div class="data-event__item">
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                                       <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                                    </a>
                                    <div class="data-event__hour">
                                       9:15
                                    </div>
                                 </h3>
                              </div>
                              <div class="price data-event__price">
                                 <a class="btn btn--brown data-event__button" href="#">kup bilet</a>
                                 <div class="data-event__available_tickets"></div>
                              </div>
                           </div>
                           <div class="data-event__item">
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                                       <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                                    </a>
                                    <div class="data-event__hour">
                                       9:15
                                    </div>
                                 </h3>
                              </div>
                              <div class="price data-event__price">
                                 <a class="btn btn--grey btn--inactive data-event__button" href="#">kup bilet</a>
                                 <div class="data-event__available_tickets">Brak dostępnych miejsc</div>
                              </div>
                           </div>
                           <div class="data-event__item">
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                                       <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                                    </a>
                                    <div class="data-event__hour">
                                       9:15
                                    </div>
                                 </h3>
                              </div>
                              <div class="price data-event__price">
                                 <a class="btn btn--brown data-event__button" href="#">kup bilet</a>
                                 <div class="data-event__available_tickets">Dostępne bilety: 5</div>
                              </div>
                           </div>
                        </div>
                     </li>
                     <li class="data-event data-day-11 data-category-1 data-event__tour data-event__tour--delay" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">23</span>
                        <span class="weekday">sobota</span>
                        </time>
                        <div class="data-event__events">
                           <div class="data-event__item data-event__item__delay">
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                                       <!--CONTENT_replaced-->Wycieczka indywidualna<!--CONTENT_replaced-->
                                    </a>
                                    <div class="data-event__hour">
                                       9:15
                                    </div>
                                 </h3>
                              </div>
                              <div class="price data-event__price">
                                 <span class="btn btn--large btn--black">Sprzedaż wycieczek ruszy od 14 grudnia 2017</span>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </section>
            </div>
            <aside class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
               <div class="tour__side">
                  <h2>Pamiętaj</h2>
                  <div class="tour__side__item">
                     <div class="header"><img src="../images-frogriot/png/icon__tour.png" alt=""><span class="txt">Bilety</span></div>
                     <div class="txt">
                        Darmowy bilet dla opiekuna<br>
                        przysługuje na każde<br>
                        10 biletów ulgowych<br><br>
                        Bilet ulgowy przysługuje osobom,<br> 
                        które nie ukończyły 18 roku życia
                     </div>
                  </div>
                  <div class="tour__side__item">
                     <div class="header"><img src="../images-frogriot/png/icon__tour-02.png" alt=""><span class="txt">Poinformuj</span></div>
                     <div class="txt">
                        Powiedz nam o specjalnych 
                        potrzebach uczestników:
                        <ul>
                           <li>udogodnienia dla osób niepełnosprawnych</li>
                        </ul>
                     </div>
                  </div>
                  <div class="tour__side__item">
                     <div class="header"><img src="../images-frogriot/png/icon__tour-03.png" alt=""><span class="txt">Oprowadzanie w języku obcym</span></div>
                     <div class="txt">
                        Prosimy o wcześniejszy kontakt 
                        telefoniczny  
                     </div>
                  </div>
                  <div class="tour__side__item">
                     <div class="header"><img src="../images-frogriot/png/icon__tour-04.png" alt=""><span class="txt">Skontakuj się z nami</span></div>
                     <div class="txt">
                        <a href="mailto:wycieczki@teatrwielki.pl">wycieczki@teatrwielki.pl</a>
                        <a href="tel:+48 22 692 02 00">+48 22 692 02 00</a>
                     </div>
                  </div>
               </div>
            </aside>
         </div>
         <a class="scroll-top" href="https://teatrwielki.pl/#main-header">do góry</a>
      </div>
      <div id="parallax" style="transform: translate3d(0px, 0px, 0px); position: fixed;"><img class="b-lazy b-loaded" src="./Kalendarium&nbsp;_ Teatr Wielki Opera Narodowa_files/csm_kalendarium-bg_02_bf6264b6d5.jpg" alt=""></div>
      <span class="search-img"></span>
      <div id="overlay"></div>
   </section>
</div>
<div class="container">
    <?php include 'include/footer-kalendarium.php' ?>
</div>