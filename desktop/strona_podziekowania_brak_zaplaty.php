<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">

      <section class="main" role="main">

         <div class="fr-wrapper">

             

            <div class="container fr-container">

               <div class="fr-container__inner">
                   
                  <div class="container__inner container__inner__gap container__inner__gap--vertical">      

                  <div class="form__section-header form__section-header--center"><strong>Dziękujemy za złożenie zamówienia</strong></div>
                  <div class="form__output form__output--center">
                     Aktualnie czekamy na potwierdzenie z Twojego banku o dokonaniu płatności. Po jej zaksięgowaniu otrzymasz informację na adres e-mail.
                     W przypadku nie otrzymania biletów na e-maila prosimy o kontakt z Biurem Obsługi Widzów.
                  </div>
                  <div class="form-typ-btn-wrapper">
                     <a href="#" class="btn btn--brown form-typ-btn">WRÓĆ DO SKLEPU</a>
                  </div>

                  <div class="row">
                     <div class="col-12"><div class="form__section-header"><strong>ZOBACZ INNE SPEKTAKLE</strong></div></div>
                     <div class="col-6 form-typ-col-left">
                        <div class="cart-prod">
                           <div class="cart-prod__thumb">
                              <img src="pliki-serwer/jezioro_labedzie.jpg" alt="miniaturka">
                           </div>
                           <div class="cart-prod__content">
                              <h3>JEZIORO ŁABĘDZIE</h3>
                              <p>Warszawa, sala Moniuszki</p>
                           </div>
                        </div>
                        <a href="#" class="btn btn--large btn--brown">KUP BILET</a>
                     </div>
                     <div class="col-6 form-typ-col-right">
                        <div class="cart-prod">
                           <div class="cart-prod__thumb">
                              <img src="pliki-serwer/dziadek_do_orzechow_i_krol_myszy.png" alt="miniaturka">
                           </div>
                           <div class="cart-prod__content">
                              <h3>DZIADEK DO ORZECHÓW I KRÓL MYSZY</h3>
                              <p>Warszawa, sala Moniuszki</p>
                           </div>
                        </div>
                        <a href="#" class="btn btn--large btn--brown">KUP BILET</a>
                     </div>
                  </div>
                      
                  </div>      

               </div>

            </div>
                    
      </section>
          
          </div>      
      
          <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>