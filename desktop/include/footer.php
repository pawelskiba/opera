<footer id="footer">
         <!--CONTENT_replaced-->
         <ul>
            <li>
               <h2>Repertuar</h2>
               <ul>
                  <li><a href="http://teatrwielki.pl/repertuar/sezon-201819/" target="_blank">Sezon 2018/19</a></li>
                  <li><a href="http://teatrwielki.pl/?id=14" target="_blank">Kalendarium</a></li>
               </ul>
               <h2>Bilety</h2>
               <ul>
                  <li><a href="http://teatrwielki.pl/bilety/abonamenty-201819/" target="_blank">Abonamenty</a></li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/#/f/0/1-31" target="_blank">Zakup biletów</a></li>
                  <li><a href="https://teatrwielki.pl/bilety/cennik-1718/">Cennik 17/18</a></li>
                  <li><a href="https://teatrwielki.pl/fileadmin/media/files/regulaminy/regulaminy_sprzedazy_biletow_16_17/Regulamin_sprzedazy_online_biletow_w_Teatrze_Wielkim_-_Operze_Narodowej.pdf" target="_blank">Regulamin sprzedaży online</a></li>
                  <li><a href="http://teatrwielki.pl/dzialalnosc/edukacja/wycieczki-po-teatrze/" target="_blank">Wycieczki po&nbsp;Teatrze</a></li>
               </ul>
            </li>
            <li>
               <h2>Teatr </h2>
               <ul>
                  <li><a href="https://teatrwielki.pl/teatr/aktualnosci/">Aktualności</a></li>
                  <li><a href="https://teatrwielki.pl/teatr/polski-balet-narodowy/">Polski Balet Narodowy</a></li>
                  <li><a href="https://teatrwielki.pl/teatr/ludzie/">Ludzie</a></li>
                  <li><a href="https://teatrwielki.pl/teatr/miejsce/">Miejsce</a></li>
                  <li><a href="https://teatrwielki.pl/teatr/historia/">Historia</a></li>
                  <li><a href="https://teatrwielki.pl/teatr/kulisy/">Kulisy</a></li>
                  <li><a href="https://teatrwielki.pl/teatr/multimedia/">Multimedia</a></li>
                  <li><a href="https://teatrwielki.pl/kontakt/">Kontakt</a></li>
               </ul>
            </li>
            <li>
               <h2>Działalność </h2>
               <ul>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/edukacja/">Edukacja</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/galeria-opera/">Galeria Opera</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/muzeum-teatralne/">Muzeum Teatralne</a></li>
                  <li><a href="http://archiwum.teatrwielki.pl/" target="_blank">Archiwum</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/akademia-operowa/">Akademia Operowa</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/konkurs-moniuszkowski/">Konkurs Moniuszkowski</a></li>
                  <li><a href="http://butik.teatrwielki.pl/" target="_blank">Butik</a></li>
                  <li><a href="https://teatrwielki.pl/nc/dzialalnosc/wynajem-kostiumow/">Wynajem kostiumów</a></li>
                  <li><a href="https://teatrwielki.pl/nc/dzialalnosc/wynajem-rekwizytow/">Wynajem rekwizytów</a></li>
               </ul>
            </li>
            <li>
               <h2>Inne </h2>
               <ul>
                  <li><a href="https://teatrwielki.pl/bilety/informacje-dla-widzow/" class="internal-link">Informacje dla widzów</a></li>
                  <li><a href="https://teatrwielki.pl/bilety/informacje-dla-widzow/dostepnosc/">Dostępność</a></li>
                  <li><a href="https://teatrwielki.pl/mapa-strony/" class="internal-link">Mapa strony</a></li>
                  <li><a href="https://teatrwielki.pl/newsletter/">Newsletter</a></li>
                  <li><a href="https://teatrwielki.pl/polityka-prywatnosci-cookies/">Polityka prywatności / cookies</a></li>
                  <li><a href="https://teatrwielki.pl/wejdz-w-obiektyw/" target="4436">Wejdź w&nbsp;obiektyw</a></li>
                  <li><a href="https://teatrwielki.pl/pasieka/">Pasieka w&nbsp;Teatrze</a></li>
               </ul>
            </li>
            <li>
               <h2>Współpraca</h2>
               <ul>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/partnerzy-i-sponsorzy/">Partnerzy i&nbsp;sponsorzy</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/wspolpraca/">Współpraca i&nbsp;wsparcie</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/organizacja-imprez/">Organizacja imprez</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/ogloszenia/">Ogłoszenia</a></li>
                  <li><a href="https://teatrwielki.pl/dzialalnosc/oferty-pracy/">Oferty pracy</a></li>
                  <li><a href="https://teatrwielki.pl/nc/dla-mediow/">Dla mediów</a></li>
                  <li><a href="https://teatrwielki.pl/zamowienia-publiczne/">Zamówienia publiczne</a></li>
                  <li><a href="http://teatrwielki.nowybip.pl/uslugi-spoleczne-i-inne-szczegolne-uslugi---ogloszenia" target="_blank">Zamówienia na&nbsp;usługi społeczne</a></li>
                  <li><a href="http://teatrwielki.nowybip.pl/" target="_blank">Biuletyn Informacji Publicznej</a></li>
               </ul>
            </li>
         </ul>
         <!--CONTENT_replaced-->
         <div class="go-to-mobile"><a href="https://teatrwielki.pl/mobile/" class="btn pull-left mobile-view" style="margin-left: 0 !important;">Wersja mobilna</a>
            <a href="https://www.facebook.com/operanarodowa/" target="_blank"><img alt="Logo Facebook" src="../images-serwer/FB.png"></a>
            <a href="https://www.youtube.com/user/operanarodowa" target="_blank"><img alt="Logo YouTube" src="../images-serwer/YT.png"></a>
            <a href="https://www.instagram.com/operanarodowa/" target="_blank"><img alt="Logo Instagram" src="../images-serwer/Logo_Instagrama_Fiolet_.png"></a>
            <a href="http://teatrwielki.nowybip.pl/" target="_blank" style="margin-left: 30px;"><img alt="Logo Biuletyn Informacji Publicznej" src="../images-serwer/BIP.png"></a>
         </div>
         <div class="footer-address clearfix">
            <div class="pull-left">
               <address>Teatr Wielki - Opera Narodowa, Plac Teatralny 1, 00-950 Warszawa, skrytka pocztowa 59</address>
               <p class="direct-contact">
                  Rezerwacja miejsc: <a href="tel:48228265019">+48 22 826 50 19</a>
                  Centrala: <a href="tel:48226920200">+48 22 692 02 00</a>
                  E-mail: <a href="mailto:office@teatrwielki.pl">office@teatrwielki.pl</a>
               </p>
            </div>
            <div class="pull-right">
               <a href="http://www.pois.gov.pl/" target="_blank"><img src="../images-serwer/ue.png" alt="Europejski Fundusz Rozwoju Regionalne"></a>
            </div>
         </div>
      </footer>

      <?php include 'include/javascripts.php' ?>
      



      

          
      


