<header id="main-header">

     <div class="container">
        <div class="fr-nav">
           <div class="row">

              <div class="col-4 fr-nav__left d-flex">

                 <a class="fr-nav__settings" href="#">
                    <img src="../ikony/icon-settings.svg" alt="ikona" />
                 </a>

                 <a class="fr-lang-switch" href="#">PL</a>

                 <!-- Tymczasowy switcher -->

                 <div class="color-switcher">
                    <div class="buttons">
                        <button class="switch-item one active" data-color="">A</button>
                        <button class="switch-item two" data-color="color-1">A</button>
                        <button class="switch-item three" data-color="color-2">A</button>
                        <button class="switch-item four" data-color="color-3">A</button>
                        <button class="switch-item five" data-color="color-4">A</button>
                    </div>
                </div>  

                <style>
                    .color-switcher {
                        margin-left: 15px;
                    }

                    .color-switcher .buttons {
                        overflow: hidden;
                        background: #fff;
                        padding: 10px;
                    }

                    .color-switcher .switch-item {
                        display: block;
                        font-size: 14px;
                        width: 29px;
                        height: 29px;
                        border: none;
                        float: left;
                        margin-right: 10px;
                        
                    }

                    .color-switcher .switch-item.active {
                        position: relative;
                        z-index: 1;
                        outline: none;
                    }
                    
                    .color-switcher .switch-item.active:before {
                        position: absolute;
                        content: "";
                        display: block;
                        left: -6px;
                        top: -6px;
                        right: -6px;
                        bottom: -6px;
                        border: 3px solid #000;
                        z-index: -1;
                    }

                    .color-switcher .switch-item.one {
                        background: url(../images-frogriot/default-contrast.gif) 0 0 no-repeat;
                    }

                    .color-switcher .switch-item.two {
                        background: #fff;
                        border: 1px solid #000;
                    }

                    .color-switcher .switch-item.three {
                        background: #000;
                        border: 1px solid #000;
                        color : #fff;
                    }

                    .color-switcher .switch-item.four {
                        background: #ff0;
                        color: #000;
                    }

                    .color-switcher .switch-item.five {
                        background: #000;
                        color: #ff0;
                    }
                </style>

                

                <!-- Tymczasowy switcher END -->  

              </div>

              <div class="col-4">
                 <a class="fr-logo" href="butik.php" style="display: table; margin: 0 auto">
                    <img src="../ikony/logo.jpg" alt="logo" />
                 </a>
              </div>

              <div class="col-4 d-flex">

                  <div class="fr-cart-wrapper">
                      <a class="fr-cart" href="#">
                        <img src="../ikony/icon-cart.svg" alt="Koszyk" />
                        <span class="fr-cart__counter">7</span>
                        <span class="fr-cart__label"><strong>Koszyk</strong></span>
                        <span class="fr-cart__amount">95 PLN</span>
                     </a>
                      <div class="fr-cart-short">
                          <!--<div class="fr-popup__close fr-close"></div>-->
                          <div class="cart-short__header">TWÓJ KOSZYK <span>(7)</span></div>
                          <div class="cart-short__items">
                              <div class="short-cart-item">
                                  <div class="short-cart-item__header">Bilety</div>
                                  <div class="short-cart-item__content">
                                      <div class="img-box">
                                          <img src="../../images-frogriot/img-spektakl.jpg" alt="">
                                      </div>
                                      <div class="content-box">
                                          <div class="name">jezioro łabędzie</div>
                                          <div class="price">120,00 PLN</div>
                                          <div class="num-box">
                                              <span class="num-txt">Liczba:</span>
                                              <span class="num">4</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="short-cart-item">
                                  <div class="short-cart-item__header">Abonamenty</div>
                                  <div class="short-cart-item__content">
                                      <div class="img-box">
                                          <img src="../../images-frogriot/abonamenty_header.jpg" alt="">
                                      </div>
                                      <div class="content-box">
                                          <div class="name">A to polska właśnie</div>
                                          <div class="price">120,00 PLN</div>
                                          <div class="num-box">
                                              <span class="num-txt">Liczba:</span>
                                              <span class="num">1</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="short-cart-item">
                                  <div class="short-cart-item__header">Produkty</div>
                                  <div class="short-cart-item__content">
                                      <div class="img-box">
                                          <img src="../../images-frogriot/product_search-03.jpg" alt="">
                                      </div>
                                      <div class="content-box">
                                          <div class="name">Mója tata tańczy w balecie</div>
                                          <div class="price-prev">49,00 PLN</div>
                                          <div class="price">39,00 PLN</div>
                                          <div class="num-box">
                                              <span class="num-txt">Liczba:</span>
                                              <span class="num">1</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              
                          </div>
                          <div class="short-cart-summary">
                              <div class="header">
                                  <div class="txt">Suma</div>
                                  <div class="price">127 PLN</div>
                              </div>
                              <div class="content">
                                  <a href="#" class="btn btn--brown">PRZEJDŹ DO PŁATNOŚCI</a>
                                  <a href="#" class="btn btn--white">ZOBACZ KOSZYK</a>
                              </div>
                          </div>
                      </div>
                  </div>
                 

                 <a class="fr-myacc" href="twoje_konto.html">
                    <img src="../ikony/icon-user2.svg" alt="Moje konto" />
                    <span>Moje<br/>konto</span>
                 </a>

              </div>

           </div>
        </div>
     </div>

  </header>    