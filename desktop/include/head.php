<!DOCTYPE html>
<html class=" pl js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="pl"style="">
   <!--<![endif]-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="generator" content="TYPO3 CMS">
      <meta name="description" content="Teatr Wielki Warszawa - sprawdź Repertuar, Bilety online, spektakle dla widzów dorosłych i dla dzieci.">
      <meta name="keywords" content="teatr wielki, opera narodowa">
      <link rel="stylesheet" type="text/css" media="all" href="pliki-serwer/head-b3130a2286cd8a5e13691a191d2f1038.merged.css">
      <title>Kalendarium&nbsp;: Teatr Wielki Opera Narodowa</title>
      <style type="text/css">
         :root [href^="//69oxt4q05.com/"],
         :root .header-adblock,
         :root #adnp,
         :root [class^="ad-"],
         :root [class^="advertisement"],
         :root [class$="-ads"],
         :root [class*="cg-template"],
         :root #widget-shop,
         :root .widget-ads-content,
         :root .sliderads,
         :root .ppa-slot,
         :root #pixad-box,
         :root .onnetwork-video,
         :root .linkSponsorowany,
         :root #advertisments,
         :root .adsense,
         :root .ads,
         :root .adform-slot,
         :root [id^="crt-"],
         :root #top-reklama,
         :root #top_advertisement,
         :root #advertising_box,
         :root .single_ad_300x250,
         :root .td-a-rec,
         :root div.ads-s,
         :root #image_pino,
         :root .reklamastoproc,
         :root .reklama-top,
         :root .reklama-rectangle,
         :root .reklama_gora,
         :root #playerads,
         :root #pasekSingle,
         :root #LinkArea\:gry,
         :root [id^="ad_widget"],
         :root #content-stream,
         :root .cg3-products,
         :root .anal-post-popularne,
         :root [href^="http://tc.tradetracker.net/"],
         :root [href^="http://s.click.aliexpress.com/"],
         :root a[href*="://converti.se/click/"],
         :root a[href*="://clicks.fortunaaffiliates.com/"],
         :root .adv_container,
         :root .adsbyg,
         :root .adbox,
         :root #ads,
         :root .trizer-bnr,
         :root [id*="ad-container"],
         :root .ad,
         :root a[href*="/adserwer/"]
         { display: none !important; }
      </style>
      <!-- End Google Tag Manager -->
      <meta name="rating" content="general">
      <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
      <meta name="viewport" content="width=1000">
      <script>var mobile_404;</script>   
       
      <script type="text/javascript" src="pliki-serwer/head-0904bffbd0f9478660217a5a53964080.merged.js"></script>
      <link rel="stylesheet" type="text/css" media="screen" href="pliki-serwer/head-fe7cc1a70a9ae716a15c7b0cd1fe1e81.merged.css">
      <link rel="icon" type="image/gif" href="https://teatrwielki.pl/fileadmin/templates/default/img/favicon.gif">
      <link rel="stylesheet" media="screen" class="theme-default" href="pliki-serwer/theme-default.css">
      <noscript>
      <link rel="stylesheet" type="text/css" media="screen"  href="pliki-serwer/noscript-c729e5b37d8bb721354b346cc35e985d.min.css" />
      </noscript>
      <link rel="stylesheet" type="text/css" media="screen and (orientation:landscape) and (max-height: 810px),(orientation:portrait)" href="pliki-serwer/head-f456ecb483759b9a7213f8fde694efd5.merged.css">
      <link rel="stylesheet" type="text/css" media="screen and (orientation:landscape) and (min-height: 811px) and (max-height: 1070px)" href="pliki-serwer/head-b2edbfa2125ccbb0b4b80b1b9da831a9.merged.css">
      <link rel="stylesheet" type="text/css" media="screen and (orientation:landscape) and (min-height: 1071px)" href="pliki-serwer/head-0ef01080425afd967e8601ae650bd9b7.merged.css">
      <link rel="stylesheet" media="screen and (orientation:landscape) and (max-height: 810px), (orientation:portrait)" class="theme-default-1" href="pliki-serwer/theme-default-1.css">
      <link rel="stylesheet" media="screen and (orientation:landscape) and (min-height: 811px) and (max-height: 1070px)" class="theme-default-2" href="pliki-serwer/theme-default-2.css">
      <link rel="stylesheet" media="screen and (orientation:landscape) and (min-height: 1071px)" class="theme-default-3" href="pliki-serwer/theme-default-3.css">
       
       
      <link rel="stylesheet" type="text/css" media="print" href="pliki-serwer/head-ba34ab2b68a3f7d7fb7638f94482f3b4.merged.css">
      <meta property="og:title" content="Kalendarium">
      <meta property="og:description" content="Teatr Wielki Opera Narodowa">
      <meta property="og:type" content="website">
      <meta property="og:site_name" content="Teatr Wielki Opera Narodowa">
      <link rel="icon" type="image/icon" href="https://teatrwielki.pl/fileadmin/templates/default/_img/_layout/favicon.ico">
      <style type="text/css"></style>
      <style type="text/css">.backpack.dropzone {
         font-size: 15px;
         text-align: center;
         display: flex;
         flex-direction: column;
         justify-content: center;
         align-items: center;
         width: 250px;
         height: 150px;
         font-weight: lighter;
         color: white;
         will-change: right;
         z-index: 2147483647;
         bottom: 20%;
         background: #333;
         position: fixed;
         user-select: none;
         transition: left .5s, right .5s;
         right: 0px; }
         .backpack.dropzone .animation {
         height: 80px;
         width: 250px;
         background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/hoverstate.png") left center; }
         .backpack.dropzone .title::before {
         content: 'Save to'; }
         .backpack.dropzone.closed {
         right: -250px; }
         .backpack.dropzone.hover .animation {
         animation: sxt-play-anim-hover 0.91s steps(21);
         animation-fill-mode: forwards;
         background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/hoverstate.png") left center; }
         @keyframes sxt-play-anim-hover {
         from {
         background-position: 0px; }
         to {
         background-position: -5250px; } }
         .backpack.dropzone.saving .title::before {
         content: 'Saving to'; }
         .backpack.dropzone.saving .animation {
         background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/saving_loop.png") left center;
         animation: sxt-play-anim-saving steps(59) 2.46s infinite; }
         @keyframes sxt-play-anim-saving {
         100% {
         background-position: -14750px; } }
         .backpack.dropzone.saved .title::before {
         content: 'Saved to'; }
         .backpack.dropzone.saved .animation {
         background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/saved.png") left center;
         animation: sxt-play-anim-saved steps(20) 0.83s forwards; }
         @keyframes sxt-play-anim-saved {
         100% {
         background-position: -5000px; } }
      </style>
      <style type="text/css">
         :root #content > #center > .dose > .dosesingle,
         :root #content > #right > .dose > .dosesingle
         { display: none !important; }
      </style>
       
       <link rel="stylesheet" href="pliki-serwer/style.css">
       
       <link rel="stylesheet" href="../css/font-awesome.css">
       
       <link rel="stylesheet" href="../fonts/fonts.css">
       
       <link rel="stylesheet" href="../css/etiquete.css">
       
       <link rel="stylesheet" href="../css/breadcrumbs.css">
       
       <link rel="stylesheet" href="../css/global.css">
       
       <link rel="stylesheet" href="../css/slick.css">
       
       <link rel="stylesheet" href="../css/miejsca.css">
       
       <link rel="stylesheet" href="../css/mapy.css">
       
       <link rel="stylesheet" href="../css/seat.css">
       
       <link rel="stylesheet" href="../css/radio.css">
       
       
       
       <link rel="stylesheet" href="../dist/desktop.min.css">  
       
       
       
   </head>
   <body class="desktop fade-in" style="">
