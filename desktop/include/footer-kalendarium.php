<div class="foot__butik">
    <div class="foot__butik__col--left">
        <h2>Produkty w butiku</h2>
        <div class="foot__butik__items">
        <div class="item">
            <h3>Publikacje</h3>
            <ul class="links">
                <li><a href="#">Książki</a></li>
                <li><a href="#">Albumy</a></li>
                <li><a href="#">Katalogi</a></li>
                <li><a href="#">Nasze publikacje</a></li>
                <li><a href="#">Programy</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Plakaty</h3>
            <ul class="links">
                <li><a href="#">Opera</a></li>
                <li><a href="#">Balet</a></li>
                <li><a href="#">Koncert</a></li>
                <li><a href="#">Wydarzenia specjalne</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Płyty</h3>
            <ul class="links">
                <li><a href="#">Opera</a></li>
                <li><a href="#">Balet</a></li>
                <li><a href="#">Koncert</a></li>
                <li><a href="#">Wydarzenia specjalne</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Opera</h3>
        </div>
        <div class="item">
            <h3>Balet</h3>
        </div>
        <div class="item">
            <h3>Pamiątki z teatru</h3>
            <ul class="links">
                <li><a href="#">Przypinki i breloki</a></li>
                <li><a href="#">Gry</a></li>
                <li><a href="#">Artykuły papiernicze</a></li>
                <li><a href="#">Lalki</a></li>
                <li><a href="#">Torby</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Na prezent</h3>
            <ul class="links">
                <li><a href="#">Dla miłośników opery</a></li>
                <li><a href="#">Dla miłośników baletu</a></li>
                <li><a href="#">Dla dzieci</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Edycja limitowana</h3>
            <ul class="links">
                <li><a href="#">Stroje do tańca</a></li>
                <li><a href="#">Apart dla Opery Narodowej</a></li>
                <li><a href="#">Operowy miód</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Nowości w butiku</h3>
        </div>
        <div class="item">
            <h3>Zakupy i zwroty</h3>
            <ul class="links">
                <li><a href="#">Koszt przesyłki i dostawa</a></li>
                <li><a href="#">Sposoby płatności</a></li>
                <li><a href="#">Reklamacje i zwroty</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Konto</h3>
            <ul class="links">
                <li><a href="#">Zaloguj się</a></li>
                <li><a href="#">Zarejestruj się</a></li>
            </ul>
        </div>
        <div class="item">
            <h3>Przydatne informacje</h3>
            <ul class="links">
                <li><a href="#">Regulamin</a></li>
                <li><a href="#">Polityka prywatności</a></li>
                <li><a href="#">Kontakt</a></li>
                <li><a href="#">FAQ</a></li>
            </ul>
        </div>
    </div> 
    </div>
    <div class="foot__butik__col--right">
        <a class="foot__butik__logo" href="https://teatrwielki.pl/mobile/" class="logo">

        <img alt="Teatr Wielki Opera Narodowa" src="../ikony/logo.jpg">

    </a>
    <div class="btn btn--brown btn--large">Przejdź do strony teatru wielkiego</div>  
    </div>
    
    
    
  </div>  

      <?php include 'include/javascripts-kalendarium.php' ?>
      



      

          
      


