<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">     

      <section class="main" role="main">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="form--popup">

                     <p class="fr-popup__text">
                        Czy na pewno chcesz usunąć tę pozycje z koszyka?
                     </p>

                     <!-- Przyciski -->
                     <div class="form__section form__section--last">
                        <div class="form__btns">
                           <a href="#" class="form__btn--half-popup btn btn--large btn--brown">TAK</a>
                           <a href="#" class="form__btn--half-popup btn btn--large btn--white">NIE</a>
                        </div>
                     </div>

                  </form>

               </div>

            </div>

         </div>
                    
      </section>
          
      </div>    
        
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>