<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">         

      <section class="main" role="main">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="Form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>POWIADOM O DOSTĘPNOŚCI BILETÓW</strong></div>
                        
                        <div class="fr-popup__text">
                        Twój mail dodano do bazy dostępności biletów na <br/>spektakl "Czarodziejski flet".
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="btn btn--large btn--brown">ZAMKNIJ</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>
                    
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>