<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">     

      <section class="main" role="main">

         <div class="fr-wrapper">

             

            <div class="container fr-container">

               <!-- Nav tabs -->
               <ul class="nav nav-tabs cart-tabs" id="cartTab" role="tablist">
                  <li class="nav-item cart-tabs__item">
                     <a class="nav-link" data-toggle="tab" href="#koszyk1" role="tab" aria-controls="koszyk1" aria-selected="true">
                        <span>
                           <span class="first">1.</span>
                           <span>TWÓJ KOSZYK</span>
                        </span>
                     </a>
                  </li>
                  <li class="nav-item cart-tabs__item">
                     <a class="nav-link active" data-toggle="tab" href="#koszyk2" role="tab" aria-controls="koszyk2" aria-selected="false">
                        <span>
                           <span class="first">2.</span>
                           <span>DOSTAWA</span>
                        </span>
                     </a>
                  </li>
                  <li class="nav-item cart-tabs__item cart-tabs__item--last">
                     <a class="nav-link" data-toggle="tab" href="#koszyk3" role="tab" aria-controls="koszyk3" aria-selected="false">
                        <span>
                           <span class="first">3.</span>
                           <span>PODSUMOWANIE<br/>I PŁATNOŚĆ</span>
                        </span>
                     </a>
                  </li>
               </ul>

               <div class="lockbar">
                  <div class="lockbar__inner">
                     <div class="lockbar__clock">
                        <img src="../ikony/zegar.png" alt="ikona" />
                        <div>14:59</div>
                     </div>
                     <p>
                        Miejsca zostały obecnie zablokowane na potrzeby transakcji.<br/>
                        Po upływie 40 minut miejsca zostaną automatycznie odblokowane, a transakcję należy powtórzyć.
                     </p>
                  </div>
               </div>

               <!-- Tab panes -->
               <form class="form--cart">
                  <div class="tab-content">
                      <div class="container__inner__gap container__inner__gap--left">
                     <div class="tab-pane fade" id="koszyk1" role="tabpanel" aria-labelledby="koszyk1"></div>
                     <div class="tab-pane fade show active" id="koszyk2" role="tabpanel" aria-labelledby="koszyk2">

                        <div class="cart-container">
                           <div class="row">

                              <div class="col-6">

                                 <!-- Dane kupującego -->
                                 <div class="form__section">

                                    <div class="form__section-header"><strong>Dane Kupującego</strong></div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>IMIĘ</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>NAZWISKO</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>ADRES E-MAIL</strong></label>
                                          <input class="form__input" type="email" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>NUMER TELEFONU</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Faktura VAT -->
                                 <div class="form__section">

                                    <div class="form__section-header"><strong>Faktura VAT</strong></div>

                                    <div class="checkbox" data-toggle="collapse" href="#collapseFaktura" role="button" aria-expanded="false" aria-controls="collapseFaktura">
                                       <label class="checkbox__inner checkbox__inner--center">
                                          <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                          <span class="checkbox__checkmark"></span>
                                          <div class="checkbox__content">Chcę otrzymać fakturę VAT</div>
                                       </label>
                                    </div>

                                 </div>

                                 <!-- Dane do faktury -->
                                 <div class="form__section form__section--no-border form__section--no-margin-bottom collapse collapse-form" id="collapseFaktura">


                                    <div class="collapse-inner">

                                        <div class="form__section-header"><strong>Dane do faktury</strong></div>

                                        <!-- form row -->
                                        <div class="radioboxes radioboxes--horizontal" id="invoice-radioboxes">
                                        <label class="radiobox">
                                            <div class="radiobox__content">Osoba prywatna</div>
                                            <input class="radiobox__input" type="radio" value="" name="person/company" id="invoice-privat_person">
                                            <span class="radiobox__checkmark"></span>
                                        </label>
                                        <label class="radiobox">
                                            <div class="radiobox__content">Firma / instytucja</div>
                                            <input class="radiobox__input" type="radio" value="" checked="checked" name="person/company" id="invoice-company">
                                            <span class="radiobox__checkmark"></span>
                                        </label>
                                        </div>

                                        <!-- form row -->
                                       <div class="form__row company_invoice">
                                          <div class="form__col">
                                             <label class="form__label"><strong>NAZWA</strong></label>
                                             <input class="form__input" type="text" />
                                          </div>
                                       </div>

                                       <!-- form row -->
                                       <div class="form__row private_invoice" style="display: none">
                                          <div class="form__col">
                                             <label class="form__label"><strong>IMIE</strong></label>
                                             <input class="form__input" type="text" />
                                          </div>
                                       </div>

                                       <!-- form row -->
                                       <div class="form__row private_invoice" style="display: none">
                                          <div class="form__col">
                                             <label class="form__label"><strong>NAZWISKO</strong></label>
                                             <input class="form__input" type="text" />
                                          </div>
                                       </div>

                                        <!-- form row -->
                                        <div class="form__row" id="nip">
                                        <div class="form__col">
                                            <label class="form__label"><strong>NIP</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        </div>

                                        <!-- form row -->
                                        <div class="form__row">
                                        <div class="form__col">
                                            <label class="form__label"><strong>ULICA</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        <div class="form__col form__col--small form__col--right">
                                            <label class="form__label"><strong>NUMER</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        </div>

                                        <!-- form row -->
                                        <div class="form__row">
                                        <div class="form__col form__col--small form__col--left">
                                            <label class="form__label"><strong>KOD POCZTOWY</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        <div class="form__col">
                                            <label class="form__label"><strong>MIEJSCOWOŚĆ</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        </div>

                                        <!-- form row -->
                                        <div class="form__row">
                                        <div class="form__col form__col--half">
                                            <label class="form__label"><strong>PAŃSTWO</strong></label>
                                            <select class="form__select">
                                                <option>Polska</option>
                                                <option>Polska</option>
                                                <option>Polska</option>
                                            </select>
                                        </div>
                                        </div>

                                    </div>    

                                 </div>

                                 <!-- Adres Kupującego inny niż na fakturze -->
                                 <div class="form__section">

                                    <div class="checkbox" data-toggle="collapse" href="#collapseAdres" role="button" aria-expanded="false" aria-controls="collapseAdres">
                                       <label class="checkbox__inner checkbox__inner--center">
                                          <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                          <span class="checkbox__checkmark"></span>
                                          <div class="checkbox__content">Adres Kupującego inny niż na fakturze</div>
                                       </label>
                                    </div>

                                 </div>

                                 <!-- Adres -->
                                 <div class="form__section collapse collapse-form" id="collapseAdres">

                                    <div class="collapse-inner">

                                        <div class="form__section-header"><strong>Adres</strong></div>

                                        <!-- form row -->
                                        <div class="form__row">
                                        <div class="form__col">
                                            <label class="form__label"><strong>ULICA</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        <div class="form__col form__col--small form__col--right">
                                            <label class="form__label"><strong>NUMER</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        </div>

                                        <!-- form row -->
                                        <div class="form__row">
                                        <div class="form__col form__col--small form__col--left">
                                            <label class="form__label"><strong>KOD POCZTOWY</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        <div class="form__col">
                                            <label class="form__label"><strong>MIEJSCOWOŚĆ</strong></label>
                                            <input class="form__input" type="text" />
                                        </div>
                                        </div>

                                        <!-- form row -->
                                        <div class="form__row">
                                        <div class="form__col form__col--half">
                                            <label class="form__label"><strong>PAŃSTWO</strong></label>
                                            <select class="form__select">
                                                <option>Polska</option>
                                                <option>Polska</option>
                                                <option>Polska</option>
                                            </select>
                                        </div>
                                        </div>
                                    
                                    </div>    

                                 </div>

                                 <!-- Dostawa produktów -->
                                 <div class="form__section padding-top-35">

                                    <div class="form__section-header"><strong>Dostawa produktów</strong></div>

                                    <div class="delivery">
                                       <img class="delivery__img" src="../ikony/icon-tickets.svg" alt="Bilety" />
                                       <div>
                                          <div class="delivery__text">Bilety otrzymasz drogą mailową na adres podany w formularzu.</div>
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Sposoby dostawy produktów -->
                                 <div class="form__section form__section--no-border">

                                    <div class="form__section-header"><strong>SPOSOBY DOSTAWY PRODUKTÓW Z BUTIKU</strong></div>

                                    <label class="radiobox">
                                       <div class="radiobox__content">
                                          <strong>Przesyłka kurierska UPC (+15,00 PLN)</strong><br/>
                                          Tylko na terenie Polski. Doręczenie w ciągu kolejnego dnia roboczego od daty nadania.
                                       </div>
                                       <input class="radiobox__input" type="radio" checked="checked" name="radio">
                                       <span class="radiobox__checkmark"></span>
                                    </label>

                                    <label class="radiobox">
                                       <div class="radiobox__content">
                                          <strong>Poczta Polska (+6,50 PLN)</strong><br/>
                                          Tylko na terenie Polski. Doręczenie w ciągu 1-5 dni roboczych od nadania
                                       </div>
                                       <input class="radiobox__input" type="radio" name="radio">
                                       <span class="radiobox__checkmark"></span>
                                    </label>

                                    <label class="radiobox">
                                       <div class="radiobox__content">
                                          <strong>Odbiór w sklepie po przedpłacie</strong><br/>
                                       </div>
                                       <input class="radiobox__input" type="radio" name="radio">
                                       <span class="radiobox__checkmark"></span>
                                    </label>

                                 </div>

                                 <!-- Adres dostawy -->
                                 <div class="form__section">

                                    <div class="form__section-header"><strong>ADRES DOSTAWY</strong></div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>IMIĘ I NAZWISKO LUB NAZWA</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>IMIĘ I NAZWISKO LUB NAZWA CD.</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="row">
                                       <div class="form__mb col-xs-12 col-sm-6">
                                          <label class="form__label"><strong>ULICA</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                       <div class="form__mb col-xs-12 col-sm-6">
                                          <label class="form__label"><strong>NUMER</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="row">
                                       <div class="form__mb col-xs-12 col-sm-6">
                                          <label class="form__label"><strong>KOD POCZTOWY</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                       <div class="form__mb col-xs-12 col-sm-6">
                                          <label class="form__label"><strong>MIEJSCOWOŚĆ</strong></label>
                                          <input class="form__input" type="text" />
                                       </div>
                                    </div>

                                    <!-- form row -->
                                    <div class="form__row">
                                       <div class="form__col">
                                          <label class="form__label"><strong>PAŃSTWO</strong></label>
                                          <select class="form__select">
                                             <option>Polska</option>
                                             <option>Polska</option>
                                             <option>Polska</option>
                                          </select>
                                       </div>
                                    </div>

                                 </div>

                                 <!-- Zgody prawne -->
                                 <div class="form__section form__section--no-border">
                                    
                                    <div class="form__section-header"><strong>Zgody prawne</strong></div>

                                    <div class="checkbox">
                                       <h3 class="checkbox__header">Zgoda na przetwarzanie danych osobowych</h3>
                                       <label class="checkbox__inner">
                                          <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                          <span class="checkbox__checkmark"></span>
                                          <div class="checkbox__content">Wyrażam zgodę na przechowywanie, przetwarzanie oraz przekazywanie organizatorowi spektaklu / imprezy podanych wyżej danych osobowych z zastrzeżeniem wykorzystania jedynie do realizacji niniejszej rezerwacji.</div>
                                       </label>
                                    </div>

                                    <div class="checkbox">
                                       <h3 class="checkbox__header">Regulamin</h3>
                                       <label class="checkbox__inner">
                                          <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                          <span class="checkbox__checkmark"></span>
                                          <div class="checkbox__content">Oświadczam, że zapoznałem/am się z <a href="#"><strong>Regulaminem sprzedaży online</strong></a> i akceptuję jego ustalenia.</div>
                                       </label>
                                    </div>

                                    <div class="checkbox">
                                       <h3 class="checkbox__header">Regulamin sklepu</h3>
                                       <label class="checkbox__inner checkbox__inner--center">
                                          <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                          <span class="checkbox__checkmark"></span>
                                          <div class="checkbox__content">Akceptuję <a href="#">Regulamin sklepu</a>.</div>
                                       </label>
                                    </div>

                                 </div>

                                 <!-- Przyciski -->
                                 <div class="form__section form__section--last">
                                    <div class="form__btns">
                                       <a href="#" class="form__btn--half btn btn--large btn--white">COFNIJ</a>
                                       <a href="#" class="form__btn--half btn btn--large btn--brown">DALEJ</a>
                                    </div>
                                 </div>

                              </div>

                              <div class="col-5 offset-1">

                                 <div class="fr-label fr-summ-header">PODSUMOWANIE ZAMÓWIENIA</div>

                                 <!-- bilety -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Bilety</div>

                                    <div class="row">
                                       <div class="col-7"><strong>Balet: Jezioro Łabędzie</strong></div>
                                       <div class="col-2 text-right"><strong>Ilość</strong></div>
                                       <div class="col-3 text-right"><strong>Cena</strong></div>
                                    </div>

                                    <div class="row fr-mb-15">
                                       <div class="col-7">Normalny</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">60,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9">Prowizja za obsługę transakcji</div>
                                       <div class="col-3 text-right">2,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9"><strong>Suma</strong></div>
                                       <div class="col-3 text-right"><strong>62,00 PLN</strong></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9">Rabat</div>
                                       <div class="col-3 text-right">-20,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa ="></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-6"><strong>Suma po rabacie</strong></div>
                                       <div class="col-6 text-right"><strong>42,00 PLN</strong></div>
                                    </div>

                                 </div>

                                 <!-- abonamenty -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Abonamenty</div>

                                    <div class="row">
                                       <div class="col-7"><strong>Opera i balet: a to Polska właśnie</strong></div>
                                       <div class="col-2 text-right"><strong>Ilość</strong></div>
                                       <div class="col-3 text-right"><strong>Cena</strong></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Stefa I</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">500,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-6"><strong>Suma</strong></div>
                                       <div class="col-6 text-right"><strong>500,00 PLN</strong></div>
                                    </div>

                                 </div>

                                 <!-- Wycieczki -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Wycieczki</div>

                                    <div class="row">
                                       <div class="col-7"><strong>22 Sierpnia 2018, poniedziałek 19:00</strong></div>
                                       <div class="col-2 text-right"><strong>Ilość</strong></div>
                                       <div class="col-3 text-right"><strong>Cena</strong></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Normalny</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">60,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Ulgowy</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">300,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">Dla opiekuna</div>
                                       <div class="col-2 text-right">1</div>
                                       <div class="col-3 text-right">0 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-6"><strong>Suma</strong></div>
                                       <div class="col-6 text-right"><strong>360,00 PLN</strong></div>
                                    </div>

                                 </div>

                                 <!-- Produkty -->
                                 <div class="fr-summ">

                                    <div class="fr-label">Produkty</div>

                                    <div class="row">
                                       <div class="col-7">Miodownik</div>
                                       <div class="col-2 text-right">1</div>
                                       <div class="col-3 text-right">20,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-7">JEZIORO ŁABĘDZIE '17 - program</div>
                                       <div class="col-2 text-right">2</div>
                                       <div class="col-3 text-right">45,00 PLN</div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9">Przesyłka kurierska UPC</div>
                                       <div class="col-3 text-right">15,00 PLN</div>
                                       <div class="w-100 fr-summ-sepa"></div>
                                    </div>

                                    <div class="row">
                                       <div class="col-9"><strong>Suma</strong></div>
                                       <div class="col-3 text-right"><strong>65,00 PLN</strong></div>
                                       <div class="w-100 fr-summ-sepa fr-summ-sepa--x2"></div>
                                    </div>

                                    <!-- Zapłacono -->
                                 
                                    <div class="row">
                                        <div class="col-8 fr-label">Zapłacono (w tym VAT)</div>
                                        <div class="col-4 fr-label text-right">967,00 PLN</div>
                                    </div>
   
                                 </div>

                                 <div class="pb-10">Wiadomość dla sprzedającego:</div>  
                                 <textarea class="form__textarea" rows="6"></textarea>
                                 
                              </div>

                              </div>

                           </div>
                        </div>

                     <div class="tab-pane fade" id="koszyk3" role="tabpanel" aria-labelledby="koszyk3">koszyk3</div>
                      </div>      
                  </div>
               </form>

            </div>

          </div>
                    
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>