<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<div class="page__main">
   <section class="main" role="main">
      <div class="container">
         <div class="tour__main-header">
            <h1>Kalendarium</h1>
            <ul class="category-list category-list-season" aria-label="Sezony">
               <li class="selected"><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/09">2017/2018</a></li>
            </ul>
         </div>
         <div class="tour__head">
            <div class="tour__filter__header">
               <div class="tour__filter__header__label">Miesiące</div>
               <ul class="category-list category-list-months" aria-label="Miesiące">
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/09">wrzesień</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/10">październik</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/11">listopad</a>
                  </li>
                  <li class="selected"><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2018/12">grudzień</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/01">styczeń</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/02">luty</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/03">marzec</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/04">kwiecień</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/05">maj</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/06">czerwiec</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/07">lipiec</a>
                  </li>
                  <li><a href="https://teatrwielki.pl/repertuar/kalendarium/data/2019/08">sierpień</a>
                  </li>
               </ul>
            </div>
            <div class="tour__filter__header">
               <div class="tour__filter__header__label">Dni</div>
               <ul id="events-days-list" class="month-list" tabindex="-1" aria-hidden="true">
                  <li class="saturday" data-day="1"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-01">1</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="2"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-02">2</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="3"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-03">3</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday first last selected" data-day="4"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-04">4</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="5"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-05">5</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="6"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-06">6</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="7"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-07">7</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="8"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-08">8</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="9"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-09">9</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="10"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-10">10</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday" data-day="11"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-11">11</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="12"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-12">12</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="13"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-13">13</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="14"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-14">14</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="15"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-15">15</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="16"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-16">16</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="17"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-17">17</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday" data-day="18"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-18">18</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="19"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-19">19</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="20"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-20">20</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="21"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-21">21</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="22"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-22">22</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="23"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-23">23</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="24"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-24">24</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="tuesday" data-day="25"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-25">25</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="wednesday" data-day="26"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-26">26</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="thursday" data-day="27"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-27">27</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="friday" data-day="28"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-28">28</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="saturday" data-day="29"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-29">29</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="sunday" data-day="30"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-30">30</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
                  <li class="monday" data-day="31"><span class="drag drag-left"></span><span class="drag drag-left"></span><a href="https://teatrwielki.pl/#" tabindex="-1">
                     <time datetime="2018-12-31">31</time>
                     </a><span class="drag drag-right"></span><span class="drag drag-right"></span>
                  </li>
               </ul>
            </div>
            <div class="tour__filter__header">
               <ul id="events-category-list" class="category-list" aria-label="Kategorie" style="margin: 0 auto;">
                    <li class="data-category-all data-category-0 selected"><a href="#/0">Wszystkie</a></li>
                    <li class="data-category-1"><a href="#/1">Opera</a></li>        
                    <li class="data-category-2"><a href="#/2">Balet</a></li>        
                    <li class="data-category-3"><a href="#/3">Koncert</a></li>        
                    <li class="data-category-4"><a href="#/4">Wydarzenia specjalne</a></li>        
                    <li class="data-category-5"><a href="#/5">W trasie</a></li>       
                    <li class="data-category-6"><a href="#/6">Koprodukcje</a></li>        
                    <li class="data-category-7"><a href="#/7">Dla dzieci</a></li>    
                </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <section id="content">
                  <ul id="events-list" class="month-full-list">
                     <li class="data-multiple data-event data-day-1 data-category-7 data-category-2 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">1</span>
                        <span class="weekday">sobota</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-1 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/termin/2018-12-01_11-00/">
                                       <!--CONTENT_replaced-->Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">11:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Galeria Opera</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Warsztaty przy wystawie I.J. Paderewski. Anatomia geniuszu
                                    <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                                 <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-1 data-category-2 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/" tabindex="-1"><img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Darkness"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-01_19-00/">
                                       <!--CONTENT_replaced-->Darkness<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Balet</span> / <span class="hall"><a href="https://teatrwielki.pl/teatr/miejsce/sale/sala-mlynarskiego/">Sala Młynarskiego</a></span></p>
                                 <h4>Bach, Vivaldi, Glass, Amar / Izadora Weiss</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Balet w&nbsp;dwóch częściach | Libretto: Izadora Weiss <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilety<span class="visuallyhidden"> wydarzenia Darkness</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-2 data-category-7 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">2</span>
                        <span class="weekday">niedziela</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-2 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/malowane-dzwieki-warsztaty-przy-wystawie-paderewski-anatomia-geniuszu/termin/2018-12-02_11-00/">
                                       <!--CONTENT_replaced-->Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">11:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Galeria Opera</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Warsztaty przy wystawie I.J. Paderewski. Anatomia geniuszu
                                    <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Malowane dźwięki – warsztaty przy wystawie Paderewski. Anatomia geniuszu</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-2 data-category-1 premiere hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Król Roger"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-02_18-00/">
                                       <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                                    </a>
                                    <span class="badge">Premiera</span>
                                 </h3>
                                 <p><span class="hour">18:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Karol Szymanowski</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Król Roger</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-4 data-category-4 data-category-7 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">4</span>
                        <span class="weekday">wtorek</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-4 data-category-4 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/opera-w-kadrze/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Opera w kadrze"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/opera-w-kadrze/termin/2018-12-04_12-00/">
                                       <!--CONTENT_replaced-->Opera w&nbsp;kadrze<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">12:00</span> / <span class="category">Wydarzenia specjalne, Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4>Projekt edukacyjny</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced--><!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Opera w kadrze</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-4 data-category-1 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1"><img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Król Roger"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-04_19-00/">
                                       <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Karol Szymanowski</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Król Roger</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-event data-day-5 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">5</span>
                        <span class="weekday">środa</span>
                        </time>
                        <div class="event-img">
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/" tabindex="-1">
                           <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Madame Butterfly"></a>
                        </div>
                        <div class="event">
                           <h3>
                              <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/termin/2018-12-05_19-00/">
                                 <!--CONTENT_replaced-->Madame Butterfly<!--CONTENT_replaced-->
                              </a>
                           </h3>
                           <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                           <h4>Giacomo Puccini</h4>
                           <p class="teaser">
                              <!--CONTENT_replaced-->Tragedia japońska w&nbsp;trzech aktach | Libretto: Giuseppe Giacosa, Luigi Illica | Oryginalna włoska wersja językowa z&nbsp;polskimi napisami<!--CONTENT_replaced-->
                           </p>
                        </div>
                        <div class="price">
                        <a class="btn btn--large btn--brown" href="#">Kup bilet</a>
                        </div>
                     </li>
                     <li class="data-event data-day-6 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">6</span>
                        <span class="weekday">czwartek</span>
                        </time>
                        <div class="event-img">
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1">
                           <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Król Roger"></a>
                        </div>
                        <div class="event">
                           <h3>
                              <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-06_19-00/">
                                 <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                              </a>
                           </h3>
                           <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                           <h4>Karol Szymanowski</h4>
                           <p class="teaser">
                              <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                           </p>
                        </div>
                        <div class="price">
                        <a class="btn btn--large btn--brown" href="#">Kup bilet</a>
                        </div>
                     </li>
                     <li class="data-multiple data-event data-day-7 data-category-2 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">7</span>
                        <span class="weekday">piątek</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-7 data-category-2 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Darkness"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-07_18-00/">
                                       <!--CONTENT_replaced-->Darkness<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">18:00</span> / <span class="category">Balet</span> / <span class="hall"><a href="https://teatrwielki.pl/teatr/miejsce/sale/sala-mlynarskiego/">Sala Młynarskiego</a></span></p>
                                 <h4>Bach, Vivaldi, Glass, Amar / Izadora Weiss</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Balet w&nbsp;dwóch częściach | Libretto: Izadora Weiss <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Darkness</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-7 data-category-1 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Madame Butterfly"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/madame-butterfly/termin/2018-12-07_19-00/">
                                       <!--CONTENT_replaced-->Madame Butterfly<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Giacomo Puccini</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Tragedia japońska w&nbsp;trzech aktach | Libretto: Giuseppe Giacosa, Luigi Illica | Oryginalna włoska wersja językowa z&nbsp;polskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Madame Butterfly</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-8 data-category-4 data-category-7 data-category-2 data-category-1 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">8</span>
                        <span class="weekday">sobota</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-8 data-category-4 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-doroslych/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Uniwersytet wiedzy operowej dla dorosłych"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-doroslych/termin/2018-12-08_10-00/">
                                       <!--CONTENT_replaced-->Uniwersytet wiedzy operowej dla dorosłych<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">10:00</span> / <span class="category">Wydarzenia specjalne</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Program edukacyjny; wykład dla dorosłych<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Uniwersytet wiedzy operowej dla dorosłych</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-8 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-dzieci/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Uniwersytet wiedzy operowej dla dzieci"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/uniwersytet-wiedzy-operowej-dla-dzieci/termin/2018-12-08_12-00/">
                                       <!--CONTENT_replaced-->Uniwersytet wiedzy operowej dla dzieci<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">12:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Program edukacyjny; wykład dla dzieci w&nbsp;wieku 10-13 lat<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Uniwersytet wiedzy operowej dla dzieci</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-8 data-category-2 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Darkness"></a>
                              </div>

                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/darkness/termin/2018-12-08_18-00/">
                                       <!--CONTENT_replaced-->Darkness<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">18:00</span> / <span class="category">Balet</span> / <span class="hall"><a href="https://teatrwielki.pl/teatr/miejsce/sale/sala-mlynarskiego/">Sala Młynarskiego</a></span></p>
                                 <h4>Bach, Vivaldi, Glass, Amar / Izadora Weiss</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Balet w&nbsp;dwóch częściach | Libretto: Izadora Weiss <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Darkness</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-8 data-category-1 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Król Roger"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-08_19-00/">
                                       <!--CONTENT_replaced-->Król Roger<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Opera</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4>Karol Szymanowski</h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Opera w&nbsp;trzech aktach | Libretto: Karol Szymanowski, Jarosław Iwaszkiewicz | Oryginalna polska wersja językowa z&nbsp;angielskimi napisami<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Król Roger</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                     <li class="data-multiple data-event data-day-9 data-category-7 data-category-3 hidden" tabindex="-1">
                        <time datetime="2013-11-29">
                        <span class="day">9</span>
                        <span class="weekday">niedziela</span>
                        </time>
                        <ul>
                           <li class="data-event data-day-9 data-category-7 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/poranek-muzyczny/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="Poranek muzyczny"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/poranek-muzyczny/termin/2018-12-09_12-00/">
                                       <!--CONTENT_replaced-->Poranek muzyczny<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">12:00</span> / <span class="category">Dla dzieci</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sale Redutowe</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Niedzielne spotkania dla dzieci łączące koncert z&nbsp;opowieściami o&nbsp;instrumentach i&nbsp;kompozytorach, a&nbsp;także zagadkami muzycznymi<!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia Poranek muzyczny</span></a>
                              </div>
                           </li>
                           <li class="data-event data-day-9 data-category-3 hidden" tabindex="-1">
                              <div class="event-img">
                                 <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/bmw-art-club/" tabindex="-1">
                                 <img src="../images-frogriot/opera_w_kadrze_mini_.jpg" alt="BMW Art Club"></a>
                              </div>
                              <div class="event">
                                 <h3>
                                    <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/bmw-art-club/termin/2018-12-09_19-00/">
                                       <!--CONTENT_replaced-->BMW Art Club<!--CONTENT_replaced-->
                                    </a>
                                 </h3>
                                 <p><span class="hour">19:00</span> / <span class="category">Koncert</span> / <span class="hall"><a href="https://teatrwielki.pl/">Sala Moniuszki</a></span></p>
                                 <h4></h4>
                                 <p class="teaser">
                                    <!--CONTENT_replaced-->Koncert muzyki K. Pendereckiego, W. Lutosławskiego, H. M. Góreckiego i&nbsp;A. Panufnika
                                    <!--CONTENT_replaced-->
                                 </p>
                              </div>
                              <div class="price">
                              <a class="btn btn--large btn--brown" href="#">Kup bilet<span class="visuallyhidden"> wydarzenia BMW Art Club</span></a>
                              </div>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </section>
            </div>
         </div>
      </div>
   </section>
</div>
<div class="container">
    <?php include 'include/footer-kalendarium.php' ?>
</div>