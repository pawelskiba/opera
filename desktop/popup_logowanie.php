<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?> 

      <div class="page__main">         

      <section class="main" role="main">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>Zaloguj się i kontynuuj zakupy</strong></div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>ADRES E-MAIL</strong></label>
                              <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong> HASŁO</strong></label>
                              <input class="form__input" type="password" />
                              <a class="form__link" href="#">Zapomniałeś hasła?</a>
                           </div>
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class=" btn btn--large btn--brown">ZALOGUJ</a>
                           </div>
                        </div>

                        <hr class="form__hr" />

                        <div class="fr-popup__text fr-popup__text--nmb">
                           Nie masz jeszcze konta? <a href="#"><strong>Zarejestruj się</strong></a>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>
                    
      </section>
          
      </div>      

      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>       