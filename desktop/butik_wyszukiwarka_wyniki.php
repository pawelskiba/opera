<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<div class="page__main">
   <section class="main" role="main">
      <div class="container">
         <nav class="butik__nav">
            <ul>
               <li><a href="#"><img src="../images-frogriot/icon-home.png" alt=""></a></li>
               <li><a href="#">Publikacje</a></li>
               <li><a href="#">Gadżety</a></li>
               <li><a href="#">Stacjonarnie</a></li>
               <li><a href="#">Moniuszko200</a></li>
               <li><a href="#">Dla dzieci</a></li>
               <li><a href="#">Plakaty</a></li>
               <li><a href="#">Bilety</a></li>
               <li><a href="#"><img src="../images-frogriot/icon-loupe.png" alt=""></a></li>
            </ul>
         </nav>
         <div class="butik__search__engine">
            <div class="container-inner">
               <input type="text" class="butik__search__engine__text">
               <button class="butik__search__engine__cta btn btn--brown btn--large">
               Szukaj
               </button>
            </div>
         </div>

         <div class="butik__filter">
            <div class="container-inner">
               <ul class="breadcrumbs breadcrumbs--nopadding">
                  <li><a class="breadcrumbs-home" href="#">Strona główna</a></li>
                  <li>wyniki wyszukiwania</li>
               </ul>
               <div class="butik__filter__inner">
                  <div class="row">
                     <div class="col-sm-6">

                        <div class="butik-changeview">
                           <div class="butik-changeview__label">Zobacz:</div>
                           <div class="butik-changeview__icon butik-changeview__icon--blocks">
                              <img src="../images-frogriot/svg/grid.svg" />
                           </div>
                           <div class="butik-changeview__icon butik-changeview__icon--list">
                              <img src="../images-frogriot/svg/viewp_point.svg" />
                           </div>

                           <div class="butik-changeview__select">
                              <div>Sortuj wg</div>
                              <select id="sortby">
                                 <option value="original-order">--</option>
                                 <option value="sortname" data-direction="asc">Nazwa produktu: od A do Z</option>
                                 <option value="sortname" data-direction="desc">Nazwa produktu: od Z do A</option>
                                 <option value="sortprice" data-direction="asc">Cena produktu od najniższej</option>
                                 <option value="sortprice" data-direction="desc">Cena produktu od najwyższej</option>
                              </select>
                           </div>

                        </div>

                     </div>
                     <div class="col-sm-6">
                        <div class="butik-showperpage">
                           <div>Pokaż</div>
                           <select>
                              <option>12</option>
                              <option>24</option>
                              <option>48</option>							  							  <option>96</option>
                           </select>
                           <div>na stronę</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="butik__search__engine__result" style="background-color: #fff">
            <div class="container-inner">
               <div id="specialproducts" class="special__products grid">
                  <h2 class="butik__search__engine__result__header">Znaleźliśmy 6 produktów dla zapytania "OPERA"</h2>
                  <div class="row special__products__items" id="specialproducts-content">

                     <div class="special__products__item col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div>
                              <div class="product__item__header">
                                 <div class="header sortname">Koncert inauguracyjny otwarcia wystawy Polin</div>
                                 <div class="price">
                                    <div class="actual sortprice">125.00 PLN</div>
                                 </div>
                              </div>
                              <div class="product__item__content">
                                 Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     <div class="special__products__item col-sm-4">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div>
                              <div class="product__item__header">
                                 <div class="header sortname">Kalendarz na 2018 rok</div>
                                 <div class="price">
                                    <div class="disqount">125.00 PLN</div>
                                    <div class="actual sortprice">50.00 PLN</div>
                                 </div>
                              </div>
                              <div class="product__item__content">
                                 Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="special__products__item col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-03.jpg" alt="">
                           </div>
                           <div>
                              <div class="product__item__header sortname">
                                 <div class="header">Ryszard Winiarski katalogo wystawy</div>
                                 <div class="price">
                                    <div class="actual sortprice">15.00 PLN</div>
                                 </div>
                              </div>
                              <div class="product__item__content">
                                 Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="special__products__item col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search-04.jpg" alt="">
                           </div>
                           <div>
                              <div class="product__item__header">
                                 <div class="header sortname">Koncert inauguracyjny otwarcia wystawy Polin</div>
                                 <div class="price">
                                    <div class="actual sortprice">125.00 PLN</div>
                                 </div>
                              </div>
                              <div class="product__item__content">
                                 Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="special__products__item col-sm-4">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div>
                              <div class="product__item__header">
                                 <div class="header sortname">Kalendarz na 2018 rok</div>
                                 <div class="price">
                                    <div class="disqount">125.00 PLN</div>
                                    <div class="actual sortprice">50.00 PLN</div>
                                 </div>
                              </div>
                              <div class="product__item__content">
                                 Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="special__products__item col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div>
                              <div class="product__item__header">
                                 <div class="header sortname">Ryszard Winiarski katalogo wystawy</div>
                                 <div class="price">
                                    <div class="actual sortprice">15.00 PLN</div>
                                 </div>
                              </div>
                              <div class="product__item__content">
                                 Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
               <div class="bestsellers">
                  <h2>Polecamy</h2>
                  <div class="produts__items">
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers_00.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Spinka do krawata
                           </div>
                           <div class="price"> 125,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Marcella Sembrich-Kochańska Artystka świata
                           </div>
                           <div class="price"> 50,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-09.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Pągowski w Operze katalog wystawy
                           </div>
                           <div class="price"> 25,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-10.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Bransoletka na  sznurku
                           </div>
                           <div class="price"> 155,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-11.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Ryszard Winiarski katalog wystawy
                           </div>
                           <div class="price"> 68,00 PLN</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-inner">
               <div class="shopping_box">
                  <div class="shopping_box__icons">
                     <h2>Łatwe zakupy online</h2>
                     <div class="items">
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-zakupy.jpg" alt="">
                           </div>
                           <div class="txt">Bezpieczne<br>zakupy</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-samolot.jpg" alt="">
                           </div>
                           <div class="txt">Wysyłamy<br>wszędzie</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kalendarz.jpg" alt="">
                           </div>
                           <div class="txt">30 dni<br>na zwrot</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kciuk.jpg" alt="">
                           </div>
                           <div class="txt">Pozytywne opinie<br>klientów</div>
                        </div>
                     </div>
                  </div>
                  <div class="shopping_box__payments">
                     <h2>Metody płatności</h2>
                     <div class="icons">
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-02.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-03.jpg" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include 'include/footer-butik.php' ?>     
      </div>
   </section>
</div>