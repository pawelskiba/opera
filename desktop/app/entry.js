require('./scss/app.scss');

//-- bootstrap
import {util, collapse, tabs} from 'bootstrap';

import "magnific-popup";

//-- zbędne jsy jeśli obsługa sal będzie inna

require('../../jsy_do_widokow_z_salami/curve.js');
require('../../jsy_do_widokow_z_salami/sale/parter_rows.js');
require('../../jsy_do_widokow_z_salami/sale/amfiteatr_rows.js');
require('../../jsy_do_widokow_z_salami/sale/balkon1.js');
require('../../jsy_do_widokow_z_salami/sale/balkon2.js');
require('../../jsy_do_widokow_z_salami/sale/balkon3.js');

//-- zbędne jsy jeśli obsługa sal będzie inna


//-- tooltipster

require('../../js_wspolne/plugins/tooltipster.bundle.min.js');

require('../../js_wspolne/plugins/isotope.js');

$(document).ready(function() {

    var $isotope = $('.special__products__items');
    $isotope.isotope({
        itemSelector: '.special__products__item',
        layoutMode: 'fitRows',
        getSortData: {
            sortname: '.sortname',
            sortprice: '.sortprice parseInt',
        }
    });

    $('.image-link').magnificPopup({
        type:'image',
        mainClass: 'mfp-fade',
        gallery:{
            enabled:true
        }
    });

    $('.butik-changeview__icon').on('click', function(){
        var sp = $('#specialproducts');
        if( $(this).hasClass('butik-changeview__icon--blocks') ){
            sp.removeClass('list').addClass('grid');
            $isotope.isotope('layout');
        }else{
            sp.removeClass('grid').addClass('list');
            $isotope.isotope('layout');
        }
    });

    // bind sort button click
    $('#sortby').on( 'change', function() {

        var sortValue = this.value;
        var sortDirection = $(this).find('option:selected').data('direction'),
            sortBool = true;

        if( sortDirection == 'asc' ){
            sortBool = true;
        }else{
            sortBool = false;
        }

        if( sortValue == 'original-order' ){
            $isotope.isotope({ sortBy : 'original-order' });
        }else{
            $isotope.isotope({ sortBy: sortValue, sortAscending: sortBool });
        }

    });

});

$('.tooltip').tooltipster({
    contentAsHTML:true,
    interactive:true,
    minWidth: 1168,
    maxWidth: 1168,
    distance:7,
    side: ['top'],
    trigger: 'click',
    functionPosition: function(instance, helper, position){
        position.coord.left = (helper.geo.document.size.width - position.size.width) / 2;
        return position;
    }
 });

 $('.fr-close--tooltip').on('click', function(e){
     e.preventDefault();
     $('.tooltip').tooltipster('close');
 });

 $('.tooltip-box').tooltipster({
    side: 'bottom',
    delay: 0,
    trigger: 'hover',
    maxWidth: 500,
    minWidth: 290,
});

//-- faktura osoba prywatna/firma
$('#invoice-radioboxes').find('input').change(function(){
    
    var private_person = $('#invoice-privat_person'),
               company = $('#invoice-company');

    if (private_person.is(':checked')) {
        $('.private_invoice').css('display', 'block')
        $('.company_invoice').css('display', 'none')
        $('#nip').css('display', 'none')
    } 
    
    if (company.is(':checked')) {
        $('.private_invoice').css('display', 'none')
        $('.company_invoice').css('display', 'block')
        $('#nip').css('display', 'block')
    } 
})

//-- abonamenty 'więcej'
$('.more-abonament').click(function(e){
    e.preventDefault();
    $(this).parents('.spektakl__item').addClass('expand')
})

$('.less-abonament').click(function(e){
    e.preventDefault();
    $(this).parents('.spektakl__item').removeClass('expand')
})