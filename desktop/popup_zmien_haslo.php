<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?>

      <div class="page__main">     

      <section class="main" role="main">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>Zmień hasło</strong></div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>STARE HASŁO</strong></label>
                              <input class="form__input" type="password" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>NOWE HASŁO</strong>&nbsp;(min. 5 znaków w tym jedna cyfra i jeden znak specjalny)</label>
                              <input class="form__input" type="password" />
                           </div>
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn--half-popup btn btn--large btn--white">ANULUJ</a>
                              <a href="#" class="form__btn--half-popup btn btn--large btn--brown">ZAPISZ ZMIANY</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>
                    
      </section>
          
      </div>      
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>