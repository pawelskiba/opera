<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>   
<div class="page__main">
   <section class="main" role="main">
      <div class="abonaments__dates__wrapper">
         <div class="container">
            <div class="container__inner">
               <div class="abonaments__dates__head__wrapper">
                  <div class="abonaments__dates__head">
                     <div class="image">
                        <img src="../images-frogriot/abonamenty_header.jpg" alt="">
                     </div>
                     <div class="txt">
                        Opera i balet<br>a to polska<br>właśnie 
                     </div>
                  </div>
               </div>
               <div class="abonaments__dates abonaments__no_padd_left">
                  <div class="abonaments__dates__layout">
                     <div class="abonaments__dates__content abonaments__dates__content--center">
                        <h2 class="abonaments__header__main">Liczba abonamentów i strefa cenowa</h2>
                        <div class="abonaments__dates__header">
                           <div class="abonaments__dates__header__row">
                              <div class="abonaments__dates__header__col abonaments__dates__header__col--abonaments">
                                 <div class="box">
                                    <span class="txt">Liczba abonamentów</span>
                                    <div class="counter">
                                       <div class="counter">
                                          <div class="input_div">
                                             <button type="button" onclick="counter_minus(this)"><span></span></button>
                                             <input type="text" size="25" value="1" class="count" id="abonaments__count">   
                                             <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="abonaments__dates__header__col abonaments__dates__header__col--details">
                                    <div class="room__row">
                                       <span class="txt">Pokaż strefę cenową na widoku sali</span>
                                       <div class="checkbox-switch-ctn">
                                          <input type="checkbox" name="checkbox1" id="checkbox1" class="ios-toggle"/>
                                          <label for="checkbox1" class="checkbox-label" data-off="off" data-on="on"></label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="abonaments__switched__contents">
                           <div id="abonaments__content_1"class="abonaments__content">
                              <div class="item__spektakl__summary__container">
                                 <h2>Daty spektakli</h2>
                                 <div class="item__spektakl__summary__items">
                                    <div class="item__spektakl__summary">
                                       <div class="img-box">
                                          <img src="../images-frogriot/spektakl_thumb.jpg" alt="">
                                       </div>
                                       <div class="content-box">
                                          <div class="header">Jezioro łabędzie</div>
                                          <div class="content">
                                             22 sierpnia 2018, poniedziałek 19:00<br>
                                             Warszawa, sala Moniuszki
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item__spektakl__summary">
                                       <div class="img-box">
                                          <img src="../../images-frogriot/spektakl_thumb-02.jpg" alt="">
                                       </div>
                                       <div class="content-box">
                                          <div class="header">czarodziejski flet</div>
                                          <div class="content">
                                             27 grudnia 2017, środa 19:00<br>
                                             Warszawa, sala Moniuszki
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item__spektakl__summary">
                                       <div class="img-box">
                                          <img src="../../images-frogriot/spektakl_thumb-03.jpg" alt="">
                                       </div>
                                       <div class="content-box">
                                          <div class="header">Dziadek do Orzechów i Król Myszy</div>
                                          <div class="content">
                                             29 grudnia 2017, piątek 19:00<br>
                                             Warszawa, sala Młynarskiego
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item__spektakl__summary">
                                       <div class="img-box">
                                          <img src="../../images-frogriot/spektakl_thumb-04.jpg" alt="">
                                       </div>
                                       <div class="content-box">
                                          <div class="header">Cud albo Krakowiaki i Górale</div>
                                          <div class="content">
                                             27 grudnia 2017, środa 19:00<br>
                                             Warszawa, sala Młynarskiego
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="center-block">
                                    <a href="#" class="btn btn--large btn--brown">Dalej</a>
                                 </div>
                              </div>
                           </div>
                           <div id="abonaments__content_2" style="display: none" class="abonaments__content">
                              <div class="abonaments__content_room__header">
                                 <h2>Plan sali moniuszki</h2>
                              </div>
                              <img src="../images-frogriot/plan_sali_moniuszki.jpg" alt="">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<div class="container">
   <?php include 'include/footer-butik.php' ?>   
</div>