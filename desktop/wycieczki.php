<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>   
<div class="page__main">
   <section class="main" role="main">
      <div class="tours">
         <div class="container">
            <div class="tours__header">
               <h2>Wycieczki po teatrze</h2>
            </div>
            <div class="container__inner">
               <div class="tours__content">
                  <h3>Lorem ipsum dolor sit amet</h3>
                  <div class="tours__layout">
                     <div class="tours__layout__main">
                        <p>
                           Zwiedzanie kulis Teatru Wielkiego - Opery Narodowej to okazja, by stanąć na jednej z największych scen operowych świata, zobaczyć jej techniczne możliwości, zajrzeć do malarni, modelatorni, pracowni szewskiej czy pracowni krawieckiej, sprawdzić, jak głęboki jest kanał orkiestry, zgubić się w ogromnych magazynach kostiumów, a przy odrobinie szczęścia przez uchylone drzwi podsłuchać artystów podczas próby. To okazja, by niezwykły świat opery obejrzeć od środka, znaleźć się „po drugiej stronie lustra”.
                        </p>
                        <p>
                           Zapraszamy wszystkich zainteresowanych – osoby indywidualne, rodziny z dziećmi, grupy szkolne lub zorganizowane grupy dorosłych.  
                        </p>
                        <p>
                           SPOSOBY REZERWACJI BILETÓW:
                        </p>
                        <p>
                           Trasa wycieczki obejmuje: prelekcję o historii gmachu, kulisy sceny, spacer po rozmaitych pracowniach artystycznych i magazynie kostiumów teatralnych, widownię Sali Moniuszki, foyer oraz wizytę w garderobie artystów.
                        </p>
                        <p>
                           Teatr Wielki - Opera Narodowa zastrzega sobie prawo do odwołania wycieczki w każdym czasie, ze względu na dynamiczny charakter prac artystycznych na scenie i w jej obrębie. 
                        </p>
                        <p>
                           INFORMACJE:
                        </p>
                        <ul class="tours__details">
                           <li class="tours__details__item">
                              <div class="tours__details__item__icon">
                                 <img src="../images-frogriot/png/zegar.jpg" alt="">
                              </div>
                              <div class="tours__details__item__txt">
                                 <b>Czas zwiedzania</b><br>
                                 ok. 1h 30min.
                              </div>
                           </li>
                           <li class="tours__details__item">
                              <div class="tours__details__item__icon">
                                 <img src="../images-frogriot/png/bilet.jpg" alt="">
                              </div>
                              <div class="tours__details__item__txt">
                                 <b>Bilet normalny</b><br>
                                 20zł (osoby dorosłe)
                              </div>
                           </li>
                           <li class="tours__details__item">
                              <div class="tours__details__item__icon">
                                 <img src="../images-frogriot/png/niepelnosprawny.jpg" alt="">
                              </div>
                              <div class="tours__details__item__txt">
                                 <b>Udogodnienia dla osób niepełnosprawnych</b><br>
                                 poinformuj nas 
                                 o specjanych potrzebach 
                                 uczestników
                              </div>
                           </li>
                           <li class="tours__details__item">
                              <div class="tours__details__item__icon">
                                 <img src="../images-frogriot/png/dziecko.jpg" alt="">
                              </div>
                              <div class="tours__details__item__txt">
                                 <b>poinformuj nas o specjanych potrzebach uczestników</b><br>
                                 5 lat
                              </div>
                           </li>
                           <li class="tours__details__item">
                              <div class="tours__details__item__icon">
                                 <img src="../images-frogriot/png/bilet.jpg" alt="">
                              </div>
                              <div class="tours__details__item__txt">
                                 <b>Bilet ulgowy</b><br>
                                 10zł<br>
                                 (dzieci i młodzież szkolna)
                              </div>
                           </li>
                           <li class="tours__details__item">
                              <div class="tours__details__item__icon">
                                 <img src="../images-frogriot/png/flaga.jpg" alt="">
                              </div>
                              <div class="tours__details__item__txt">
                                 <b>Oprowadzanie w językach obcych</b>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <div class="tours__layout__side">
                        <a href="#" class="btn btn--large btn--brown">kup bilet</a>
                     </div>
                  </div>
                  <div class="tours__images">
                     <div class="torus__images__item">
                        <img src="../images-frogriot/tours.jpg" alt="">
                     </div>
                     <div class="torus__images__item">
                        <img src="../images-frogriot/tours-02.jpg" alt="">
                     </div>
                     <div class="torus__images__item">
                        <img src="../images-frogriot/tours-03.jpg" alt="">
                     </div>
                  </div>
                  <div class="tours__foot">
                     <div class="tours__foot__header">
                        <img src="../images-frogriot/png/email.jpg" alt="">
                        <b>Kontakt</b>
                     </div>
                     tel. 603 960 428<br>
                     e-mail: <a href="ekrahel@teatrwielki.pl">ekrahel@teatrwielki.pl</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<div class="container">
   <?php include 'include/footer-butik.php' ?>
</div>