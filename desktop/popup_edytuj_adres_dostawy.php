<?php include 'include/head.php' ?>
      
      <?php include 'include/header.php' ?> 

      <div class="page__main">         

      <section class="main" role="main">

        <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

                <div class="fr-popup__content">

                    <div class="fr-close"></div>

                    <form class="form--popup">

                        <div class="form__section form__section--last">

                            <div class="form__section-header"><strong>Edytuj adres dostawy</strong></div>

                            <!-- form row -->
                            <div class="form__row">
                                <div class="form__col">
                                    <label class="form__label"><strong>IMIĘ I NAZWISKO LUB NAZWA</strong></label>
                                    <input class="form__input" type="text" />
                                </div>
                                </div>

                                <!-- form row -->
                                <div class="form__row">
                                <div class="form__col">
                                    <label class="form__label"><strong>ULICA</strong></label>
                                    <input class="form__input" type="text" />
                                </div>
                                <div class="form__col form__col--small form__col--right">
                                    <label class="form__label"><strong>NUMER</strong></label>
                                    <input class="form__input" type="text" />
                                </div>
                                </div>

                                <!-- form row -->
                                <div class="form__row">
                                <div class="form__col form__col--small form__col--left">
                                    <label class="form__label"><strong>KOD POCZTOWY</strong></label>
                                    <input class="form__input" type="text" />
                                </div>
                                <div class="form__col">
                                    <label class="form__label"><strong>MIEJSCOWOŚĆ</strong></label>
                                    <input class="form__input" type="text" />
                                </div>
                                </div>

                                <!-- form row -->
                                <div class="form__row">
                                <div class="form__col form__col--half">
                                    <label class="form__label"><strong>PAŃSTWO</strong></label>
                                    <select class="form__select">
                                        <option>Polska</option>
                                        <option>Polska</option>
                                        <option>Polska</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Przyciski -->
                            <div class="form__section form__section--last">
                                <div class="form__btns">
                                    <a href="#" class="form__btn--half-popup btn btn--large btn--white">ANULUJ</a>
                                    <a href="#" class="form__btn--half-popup btn btn--large btn--brown">ZAPISZ ZMIANY</a>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </div>
                    
      </section>
          
      </div>          
      
      <div class="container">
         <?php include 'include/footer-butik.php' ?> 
      </div>        