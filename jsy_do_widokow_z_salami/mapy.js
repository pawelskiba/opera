var step = 50;

$("#tickets__prices input").click(function() {
    $('.map__cover').removeClass('map__cover__hide')

    if ($(this).is(":checked")) {

        var item = $(this).attr('data-price')

        if (item == "all") {

            $('.map__map__map').removeClass('active')
            $('.map__map__map--main').addClass('active')

            $('.choose_from_list').css('display', 'none')
            $('.places__map').css('display', 'none')
            $('.places__map--main').css('display', 'block')
        }
        if (item == "150") {
            $('.map__map__map').removeClass('active')
            $('.map__map__map--150').addClass('active')
            $('.sala__image__desktop .places__map--150').find('.map__map').attr('src', '../images-frogriot/plan_mapy_redutowe_sektora.png')
            $('.places__map').css('display', 'none')
            $('.choose_from_list').css('display', 'none')
            $('.places__map--150').css('display', 'table')

            $('.places__map--main').css('display', 'none')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }
        if (item == "120") {
            $('.map__map__map').removeClass('active')
            $('.map__map__map--mlynarskiego').addClass('active')
            $('.sala__image__desktop .places__map--mlynarskiego').find('.map__map').attr('src', '../images-frogriot/sala_mlynarskiego.png')
            $('.places__map').css('display', 'none')
            $('.choose_from_list').css('display', 'none')
            $('.places__map--mlynarskiego').css('display', 'table')

            $('.places__map--main').css('display', 'none')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }
        if (item == "100") {
            $('.map__map__map').removeClass('active')
            $('.map__map__map--sale__redutowe').addClass('active')
            $('.sala__image__desktop .places__map--sale__redutowe').find('.map__map').attr('src', '../images-frogriot/sala_mapy_redutowe.png')
            $('.places__map').css('display', 'none')
            $('.choose_from_list').css('display', 'none')
            $('.places__map--sale__redutowe').css('display', 'table')

            $('.places__map--main').css('display', 'none')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }
        if (item == "60") {

            $('.choose_from_list').css('display', 'none')
            $('.places__map').css('display', 'none')
            $('.places__map--main').css('display', 'none')
            $('.sala__image__desktop').find('img').css('display', 'block')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }
        if (item == "30") {

            $('.choose_from_list').css('display', 'none')
            $('.places__map--main').css('display', 'none')
            $('.places__map').css('display', 'none')
            $('.sala__image__desktop').find('img').css('display', 'block')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }
        if (item == "20") {

            $('.choose_from_list').css('display', 'none')
            $('.places__map').css('display', 'none')
            $('.places__map--main').css('display', 'none')
            $('.sala__image__desktop').find('img').css('display', 'block')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }
        if (item == "10") {
            $('.sala__image__desktop').find('img').attr('src', '../images-frogriot/sala-08.jpg')
            $('.choose_from_list').css('display', 'none')
            $('.places__map--main').css('display', 'none')
            $('.places__map').css('display', 'none')
            $('.sala__image__desktop').find('img').css('display', 'block')
            $('.tickets__facilities input').each(function() {
                $(this).prop('checked', false);
            });
        }

    }

    seatSizes()

    setTimeout(function() {

        if (!($('.sala__main__map').hasClass('active'))) {
            var parterH = $('.sala__image .map__map__map.active .map__map').height()

            $('.desktop .map__map__map.active .main__map__area__content').css('height', parterH + 10)
        }
        $('.map__cover').addClass('map__cover__hide')

    }, 500)
})
$("#facilities-01").click(function() {
    if ($(this).is(":checked")) {

        if ($('#facilities-02').is(":checked")) {
            $("#facilities-02").prop('checked', false)
            $("#facilities-03").prop('checked', false)
        }

        $('.main__map').removeClass('petla')
        $('.main__map').addClass('niepelno')

        $('.map__map__map').removeClass('active')
        $('.map__map__map--main').addClass('active')

        $('*[data-price="all"]').click()
        $('.sala__image__desktop').find('img').css('display', 'block')

        $('.choose_from_list').css('display', 'none')
        $('.places__map').css('display', 'none')
        $('.places__map--main').css('display', 'block')
    } else {
        $('*[data-price="all"]').click()
        $('.map__cover').removeClass('map__cover__hide')
        $('.main__map').removeClass('niepelno')
    }

    seatSizes()
})
$("#facilities-02").click(function() {
    if ($(this).is(":checked")) {
        if ($('#facilities-02').is(":checked")) {
            $("#facilities-01").prop('checked', false)
            $("#facilities-03").prop('checked', false)
        }

        $('.main__map').addClass('petla')
        $('.main__map').removeClass('niepelno')

        $('.map__map__map').removeClass('active')
        $('.map__map__map--main').addClass('active')

        $('*[data-price="all"]').click()
        $('.sala__image__desktop').find('img').css('display', 'block')

        //$('.sala__image__desktop .main__map__parter').find('.map__map').attr('src', '../images-frogriot/sale/parter.png')
        $('.choose_from_list').css('display', 'none')
        $('.places__map').css('display', 'none')
        $('.places__map--main').css('display', 'block')
    } else {
        $('*[data-price="all"]').click()
        $('.map__cover').removeClass('map__cover__hide')
        $('.main__map').removeClass('petla')
    }

    seatSizes()
})
$("#facilities-03").click(function() {

    if ($(this).is(":checked")) {

        $('*[data-price="all"]').click()

        if ($('#facilities-03').is(":checked")) {
            $("#facilities-01").prop('checked', false)
            $("#facilities-02").prop('checked', false)
        }

        $('.choose_from_list').css('display', 'block')
        $('.places__map').css('display', 'none')
        $('.places__map--main').css('display', 'none')

        $('.tickets').addClass('list')
    } else {
        $('.tickets').removeClass('list')
        $('.choose_from_list').css('display', 'none')
        $('.places__map').css('display', 'block')
        $('.places__map--main').css('display', 'block')
        $('*[data-price="all"]').click()

        seatSizes()
    }
})

function sizes() {
    var mapH = $('.main__map').height()

    var parterH = $('.main__map__map > img').height()

    $('.mobile__sector__map').css('height', parterH)

    var amfiteatrH = $('.main__map__amfiteatr img').height()
    $('.main__map__amfiteatr').css('height', amfiteatrH)

    var balkon1H = $('.main__map__balkon1 img').height()
    $('.main__map__balkon1').css('height', balkon1H)

    var balkon2H = $('.main__map__balkon2 img').height()
    $('.main__map__balkon2').css('height', balkon2H)

    var balkon3H = $('.main__map__balkon3 img').height()
    $('.main__map__balkon3').css('height', balkon3H)
}

function seatSizes() {
    var paterSizeW = $('.main__map__parter img').width();
    var paterSizeH = $('.main__map__parter img').height();
    var paterSeatSizeW = paterSizeW * 0.012;
    var paterSeatSizeH = paterSizeH * 0.0175;

    $('.main__map__parter .seat-ctn').css('width', paterSeatSizeW)
    $('.main__map__parter .seat-ctn').css('height', paterSeatSizeH)

    var amfiteatrSizeW = $('.main__map__parter img').width();
    var amfiteatrSizeH = $('.main__map__parter img').height();
    var amfiteatrSeatSizeW = amfiteatrSizeW * 0.012;
    var amfiteatrSeatSizeH = amfiteatrSizeH * 0.0175;

    $('.main__map__amfiteatr .seat-ctn').css('width', amfiteatrSeatSizeW)
    $('.main__map__amfiteatr .seat-ctn').css('height', amfiteatrSeatSizeH)

    var balkon1SizeW = $('.main__map__balkon1 img').width();
    var balkon1SizeH = $('.main__map__balkon1 img').height();
    var balkon1SeatSizeW = balkon1SizeW * 0.010;
    var balkon1SeatSizeH = balkon1SizeH * 0.015;

    $('.main__map__balkon1 .seat-ctn').css('width', balkon1SeatSizeW)
    $('.main__map__balkon1 .seat-ctn').css('height', balkon1SeatSizeH)

    var balkon2SizeW = $('.main__map__balkon1 img').width();
    var balkon2SizeH = $('.main__map__balkon1 img').height();
    var balkon2SeatSizeW = balkon2SizeW * 0.010;
    var balkon2SeatSizeH = balkon2SizeH * 0.015;

    $('.main__map__balkon2 .seat-ctn').css('width', balkon2SeatSizeW)
    $('.main__map__balkon2 .seat-ctn').css('height', balkon2SeatSizeH)

    var balkon3SizeW = $('.main__map__balkon1 img').width();
    var balkon3SizeH = $('.main__map__balkon1 img').height();
    var balkon3SeatSizeW = balkon3SizeW * 0.010;
    var balkon3SeatSizeH = balkon3SizeH * 0.015;

    $('.main__map__balkon3 .seat-ctn').css('width', balkon3SeatSizeW)
    $('.main__map__balkon3 .seat-ctn').css('height', balkon3SeatSizeH)
}

$(document).ready(function() {

    $(window).on('load', function() {
        if ($('.tickets').length > 0) {
            $('img[usemap]').rwdImageMaps();
        }
    })

    $('.tickets .plus').click(function() {

        if ($('.sala__main__map').hasClass('active')) {

            var mapContainerDW = $(this).parents('.tickets').find('.map__map__map .whole__map__container').width() + step;
            var mapContainerDH = $(this).parents('.tickets').find('.map__map__map .whole__map__container').height() + step;

            $('.whole__map__container').css('width', mapContainerDW)

            seatSizes()
            sizes()

        } else {
            var parterDW = $(this).parents('.tickets').find('.map__map__map.active .map__map').width() + step;
            var parterDH = $(this).parents('.tickets').find('.map__map__map.active .map__map').height();

            $(this).parents('.tickets').find('.map__map__map.active .map__wrapper').css({
                'width': parterDW,
                'height': parterDH
            })
            setTimeout(function() {
                seatSizes()
            }, 100)
        }
    })

    $('.tickets .minus').click(function() {

        if ($('.sala__main__map').hasClass('active')) {

            var mapContainerDW = $(this).parents('.tickets').find('.whole__map__container').width() - step;
            var mapContainerDH = $(this).parents('.tickets').find('.whole__map__container').height() - step;

            $('.whole__map__container').css('width', mapContainerDW)

            seatSizes()
            sizes()

        } else {
            var parterDW = $(this).parents('.tickets').find('.map__map__map.active .map__map').width() - step;
            var parterDH = $(this).parents('.tickets').find('.map__map__map.active .map__map').height();

            $(this).parents('.tickets').find('.map__map__map.active .map__wrapper').css({
                'width': parterDW,
                'height': parterDH
            })

            setTimeout(function() {
                seatSizes()
            }, 100)
        }
    })

    $('.fr-popup--choose_tickets--tickets .plus').click(function() {

        var parterW = $(this).parents('.mobile__tickets__content').find('.map__wrapper .map__map').width() + step;
        var parterH = $(this).parents('.mobile__tickets__content').find('.map__wrapper .map__map').height();

        $(this).parents('.mobile__tickets__content').find('.map__wrapper').css({
            'width': parterW,
            'height': parterH
        })
        setTimeout(function() {
            seatSizes()
        }, 100)
    })

    $('.fr-popup--choose_tickets--tickets .minus').click(function() {

        var parterW = $(this).parents('.mobile__tickets__content').find('.map__wrapper .map__map').width() - step;
        var parterH = $(this).parents('.mobile__tickets__content').find('.map__wrapper .map__map').height();

        $(this).parents('.mobile__tickets__content').find('.map__wrapper').css({
            'width': parterW,
            'height': parterH
        })
        setTimeout(function() {
            seatSizes()
        }, 100)

    })

    seatSizes()

    sizes()

    $(window).resize(function() {
        sizes()
        seatSizes()
        setMapZoomW()

        var parterH = $('.sala__image .map__map__map.active .map__map').height()

    })

    function setMapZoomW() {
        var _w = $('.map__single.active .main__map__area__content').width()
        $('.map__single.active .main__map__area__content').css('width', _w)
    }

    $(window).on('load', function() {

        var width = $('.tickets__main').width()

        $('.map__single .main__map__area__content').css('width', width)

        $('.map__cover').addClass('map__cover__hide')

        var parterH = $('.sala__image .map__map__map.active .map__map').height()

    })

    var timer;
    var delay = 300;

    $('.seat-ctn').hover(function(event) {
        // on mouse in, start a timeout

        timer = setTimeout(function() {

            var left = event.pageX
            var top = event.pageY

            if ($('body').find('.place__popup').length == 0) {

                $('body').prepend('<div class="place__popup"></div>')

                if ($('body').hasClass('fr-body-mobile')) {

                    $('body').find('.place__popup').prepend('<div class="seat-view"><div class="fr-close"></div><div class="img-box"><img src="../images-frogriot/seat-hover.jpg" alt=""></div><div class="details-box"><div class="row1"><div class="col-box">Balkon<br>Rząd XI, miejsca nienumerowane</div><div class="col-box">60 PLN</div></div><div class="row2"><div class="img-box"><img src="../images-frogriot/eye.jpg" alt=""></div><div class="content-box">Zdjęcia mają charakter poglądowy. W zależności od inscenizacji oraz wzrostu innych widzów widok na scenę może się różnić</div></div></div></div>')

                    $('.fr-body-mobile .fr-close').unbind('click.remove')
                    $('.fr-body-mobile .fr-close').bind('click.remove', function() {
                        $('body').find('.place__popup').remove()
                    })

                } else {
                    $('body').find('.place__popup').prepend('<div class="seat-view"><div class="img-box"><img src="../../images-frogriot/seat-hover.jpg" alt=""></div><div class="details-box"><div class="row1"><div class="col-box">Balkon<br>Rząd XI, miejsca nienumerowane</div><div class="col-box">60 PLN</div></div><div class="row2"><div class="img-box"><img src="../../images-frogriot/eye.jpg" alt=""></div><div class="content-box">Zdjęcia mają charakter poglądowy. W zależności od inscenizacji oraz wzrostu innych widzów widok na scenę może się różnić</div></div></div></div>')
                }

            }

            $('body').find('.place__popup').css({
                'top': top,
                'left': left
            })

        }, delay);
    }, function() {

        // on mouse out, cancel the timer  
        $('body').find('.place__popup').remove()
        clearTimeout(timer);

    });

    $("#area__parter").click(function() {
        $('.main__map__parter .seat-ctn').css('z-index', 2)
        $('.main__map__amfiteatr .seat-ctn').css('z-index', -1)
        $('.main__map__balkon1 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon2 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon3 .seat-ctn').css('z-index', -1)
    })
    $("#area__amfiteatr").click(function() {

        $('.main__map__parter .seat-ctn').css('z-index', -1)
        $('.main__map__amfiteatr .seat-ctn').css('z-index', 2)
        $('.main__map__balkon1 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon2 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon3 .seat-ctn').css('z-index', -1)
    })
    $("#area__balkon1").click(function() {
        $('.main__map__parter .seat-ctn').css('z-index', -1)
        $('.main__map__amfiteatr .seat-ctn').css('z-index', -1)
        $('.main__map__balkon1 .seat-ctn').css('z-index', 2)
        $('.main__map__balkon2 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon3 .seat-ctn').css('z-index', -1)
    })
    $("#area__balkon2").click(function() {
        $('.main__map__parter .seat-ctn').css('z-index', -1)
        $('.main__map__amfiteatr .seat-ctn').css('z-index', -1)
        $('.main__map__balkon1 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon2 .seat-ctn').css('z-index', 2)
        $('.main__map__balkon3 .seat-ctn').css('z-index', -1)
    })
    $("#area__balkon3").click(function() {
        $('.main__map__parter .seat-ctn').css('z-index', -1)
        $('.main__map__amfiteatr .seat-ctn').css('z-index', -1)
        $('.main__map__balkon1 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon2 .seat-ctn').css('z-index', -1)
        $('.main__map__balkon3 .seat-ctn').css('z-index', 2)
    })

})