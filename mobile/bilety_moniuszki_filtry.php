<?php include 'include/head.php' ?>
<div id="wrapper">
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="mobile-page">
      <div class="fr-popup fr-popup--choose_tickets fr-popup--choose_tickets--tickets">
         <div class="fr-popup--choose_tickets__head">
            <div class="fr-popup--choose_tickets__head__left ">
               <img src="../images-frogriot/spektakl_thumb.jpg" alt="">
            </div>
            <div class="fr-popup--choose_tickets__head__right">
               <div class="name">Jezioro Łabędzie</div>
               <div class="hour">22 sierpnia 2018, poniedziałek 19:00, Sala moniuszki</div>
            </div>
         </div>
         <div class="mobile__tickets__content">
            <div class="mobile__tickets__content__header">
               <div class="label-box">Wybierz poziom sali</div>
               <div class="content-box">
                  <select>
                     <option>Pokaż filtry</option>
                     <option>Filtr 1</option>
                     <option>Filtr 2</option>
                     <option>Filtr 3</option>
                  </select>
               </div>
            </div>
            <div class="mobile__tickets__content__content">
               <div class="mobile__tickets__choose__sector__main__area mobile__tickets__choose__sector__mlynarskiego">
                  <img src="../images-frogriot/mlynarskiego.png" alt="">
                  <button class="choose__sector choose__sector--rzedy__1">Rzędy I - IV</button>
                  <button class="choose__sector choose__sector--rzedy__2">Rzędy V - IX</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>