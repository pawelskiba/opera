<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">

      <div class="fr-wrapper">

         <!--   -->

         <div class="fr-popup">

            <div class="fr-popup__content">

               <div class="fr-close"></div>

                  <form class="Form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>POWIADOM O DOSTĘPNOŚCI BILETÓW</strong></div>
                        
                        <div class="fr-popup__text">
                        Zaloguj się lub zarejestruj, aby otrzymać informację o dostępności biletów na wybrany spektakl. Informacja zostanie przesłana na adres e-mail podany w trakcie rejestracji w przypadku zwolnienia większej puli biletów.
.<br/><br/>

Dostępność biletów warto sprawdzać również samodzielnie. Zwolnienie się pojedynczego biletu nie gwarantuje otrzymania automatycznej wiadomości.
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>ADRES E-MAIL</strong></label>
                              <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>HASŁO</strong></label>
                              <input class="form__input" type="password" />
                              <a class="form__link" href="#">Zapomniałeś hasła?</a>
                           </div>
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn">ZALOGUJ SIĘ</a>
                           </div>
                        </div>

                        <hr class="form__hr" />

                        <div class="fr-popup__text fr-popup__text--nmb">
                           Nie masz jeszcze konta? <a href="#"><strong>Zarejestruj się</strong></a>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>

      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   