<?php include 'include/head.php' ?>
<div id="wrapper">
   <?php include 'include/header.php' ?>
   <section class="main" role="main">
      <div style="display:none;">
         <div id="tooltip">
            <div class="tooltip-inner">
               Bilety ulgowe są dostępne dla: <br><br>
               <ul>
                  <li>uczniów, studentów do ukończenia 26 roku życia</li>
                  <li>emerytów i rencistów oraz osób po 70 roku życia ze zniżką 20%</li>
                  <li>szkół, uczelni wyższych, ośrodków wychowawczych, domów opieki społecznej</li>
                  <li>stowarzyszeń osób niepełnosprawnych</li>
                  <li>stowarzyszeń emerytów i rencistów przy zakupie biletów grupowych powyżej 15 osób ze zniżką 30%</li>
                  <li>uczniów szkół muzycznych i baletowych oraz studentów uczelni muzycznych i muzykologii ze zniżką do 75%
                     Cena biletu ulgowego nie może być niższa niż cena biletu w najtańszej strefie cenowej na dane wydarzenie (dla Sali Moniuszki) 
                     lub cena wejściówki ustalana na dane wydarzenie (dla Sali Młynarskiego i innych sal)
                  </li>
                  <li>posiadaczy Karty Dużej Rodziny ze zniżką 25%</li>
               </ul>
            </div>
         </div>
      </div>
      <div class="mobile-page">
         <div class="fr-popup fr-popup--choose_tickets fr-popup--choose_tickets--tickets">
            <div class="fr-popup--choose_tickets__head">
               <div class="fr-popup__close fr-close"></div>
               <div class="fr-popup--choose_tickets__head__left ">
                  <img src="../images-frogriot/spektakl_thumb.jpg" alt="">
               </div>
               <div class="fr-popup--choose_tickets__head__right">
                  <div class="name">Jezioro Łabędzie</div>
                  <div class="hour">22 sierpnia 2018, poniedziałek 19:00, Sala moniuszki</div>
               </div>
            </div>
            
            <div class="fr-popup__content">
               <div class="tickets__backwrapp"><a class="btn btn--brown btn--large" href="#">WRÓĆ DO KALENDARIUM</a></div>
               <form class="fr-form fr-form--popup">
                  <div class="fr-form__head__note">Tylko miejsca nienumerowane</div>
                  <h2 class="fr-form__h2">Wybierz bilety</h2>
                  <table class="choose_tickets_table">
                     <tr class="fr-form__row">
                        <th class="fr-form__col">
                           <div class="fr-form__col__header">Liczba</div>
                        </th>
                        <th class="fr-form__col">
                           <div class="fr-form__col__header">Cena</div>
                        </th>
                        <th class="fr-form__col">
                           <div class="fr-form__col__header"></div>
                        </th>
                     </tr>
                     <tr class="fr-form__row">
                        <td class="fr-form__col">
                           <div class="counter">
                              <div class="counter">
                                 <div class="input_div">
                                    <button type="button" onclick="counter_minus(this)"><span></span></button>
                                    <input type="text" size="25" value="1" class="count">   
                                    <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                                 </div>
                              </div>
                           </div>
                        </td>
                        <td class="fr-form__col">
                           <div class="fr-popup--choose_tickets__price">
                              30  pln
                           </div>
                        </td>
                        <td class="fr-form__col">
                           <div class="trash">
                              <img src="../images-frogriot/kosz.png" alt="">
                           </div>
                        </td>
                     </tr>
                  </table>
                  <div class="fr-form__col">
                     <div class="fr-form__col__header">Typ
                        <span class="tooltip-box" data-tooltip-content="#tooltip"> <img src="../images-frogriot/icon-info.svg" class="icon-info" class="icon-info"" alt=""></span>
                     </div>
                  </div>
                  <div class="fr-form__col">
                     <select>
                        <option>Normalny</option>
                        <option>Ulgowy</option>
                     </select>
                  </div>
                  <div class="fr-form-row fr-form-row--extra_row">
                     <img src="../images-frogriot/icon_plus.jpg" alt="">Dodaj kolejny bilet
                  </div>
                  <div class="form__section form__section--last">
                     <div class="form__btns form__btns--vertical">
                        <a href="#" class="form__btn--half-popup btn btn--large btn--white">DALEJ</a>
                        <a href="#" class="form__btn--half-popup btn btn--large btn--brown">ZAMKNIJ</a>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </section>
</div>
<?php include 'include/footer-butik.php' ?>