<?php include 'include/head.php' ?>
<div id="wrapper">
   <?php include 'include/header.php' ?> 
   <section class="main" role="main">
      <div class="abonaments">
         <div class="abonaments__header">
            <h2>abonamenty</h2>
            <div class="note">TEATR WIELKI - OPERA NARODOWA   <br>SEZON 2017 / 2018</div>
         </div>
         <div class="abonaments__rules">
            <div class="container">
               <div class="abonaments__rules__header">Zasady działania abonamentów</div>
               <div class="abonaments__rules__layout">
                  <div class="contnet">
                     W sezonie 2017/2018 Teatr Wielki – Opera Narodowa przygotował wybór siedmiu abonamentów, które widzowie mogą indywidualnie dostosować do swoich zainteresowań. Pakiety jednoosobowych i jednorazowych biletów stanowią gwarancję miejsca na wskazane spektakle. W pięciu zestawach abonamentowych bilety zostały objęte 10% zniżką (Opera i balet: A to Polska właśnie; Opera: Władcy, herosi, amanci; Opera: Muzy, heroiny, primadonny; Balet: Wielka klasyka; Balet: Tylko u nas).<br><br>
                     W nadchodzącym sezonie kontynuujemy sprzedaż abonamentów edukacyjnych, dedykowanych dzieciom i ich opiekunom, ale także pakiet pomyślany dla osób dorosłych. W zestawach edukacyjnych znajdują się cykl Poranków muzycznych, Spotkania z wirtuozem instrumentu oraz Uniwersytet wiedzy operowej dla dzieci oraz osobno dla widzów dorosłych.  
                  </div>
                  <div class="sidebar">
                     <h3>Benefity:</h3>
                     <ul>
                        <li>Zdjęcia: Robert Wolański / SONY</li>
                        <li>Tekst: Iwona Witkowska</li>
                        <li>Na zdjęciach: Amelia i Maksim Woitiul</li>
                        <li>Oprawa: twarda</li>
                        <li>Liczba stron: 112</li>
                        <li>Wymiary: 24,5 x 32,5 cm</li>
                        <li>ISBN 978-83-65161-66-6</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="spektakl__item">
               <div class="main__content">
                  <div class="spektakl__item__content">
                     <h2>Premiery - sala moniuszki</h2>
                     <div>
                        <div class="spectact__item__row">
                           <div class="spektakl__item__col">
                              Abonament na wszystie premiery operowe i baletowe w Sali Moniuszki (6 spektakli):
                           </div>
                           <div class="spektakl__item__col">
                              Eros i Psyche | X 2017
                              Balety polskie | XI 2017
                              Peleas i Melizanda | I 2018
                              Dama kameliowa | IV 2018
                              Ognisty anioł | V 2018
                              Carmen | VI 2018
                           </div>
                        </div>
                        <div class="spectact__item__row">
                           <div class="spektakl__item__col">
                                <div class="area__row">
                                    <div class="box">
                                        <span class="txt">Strefa cenowa</span>
                                        <select>
                                            <option>1 Strefa 250 PLN</option>
                                            <option>2 Strefa 200 PLN</option>
                                            <option>3 Strefa 150 PLN</option>
                                        </select>
                                    </div>
                                </div>
                              <a href="abonamenty_daty.html" class="btn btn--large btn--brown">Kup abonament</a>
                              <a href="#" class="btn btn--large btn--white btn--short more-abonament">Więcej</a>
                           </div>
                           <div class="spektakl__item__col">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="spektakl__item__image">
                     <img src="../images-serwer/img-spektakl.jpg" alt="">
                  </div>
               </div>

               <!-- more content -->

               <div class="more__content">
                  <div class="close-btn less-abonament"><img src="../images-frogriot/icon_close.jpg" alt=""></div>
                  <h2>OPERA I BALET: A TO POLSKA WŁAŚNIE</h2>
                  <div class="more__content__row">
                     <div class="details__box">
                        <p>
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                        </p>
                        <p>
                           euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
                        </p>
                     </div>
                     <div class="dates__box">
                        <div class="header">Daty spektakli</div>
                        <div class="dates__row">
                           <div class="spectacl__date__item">
                              <div class="date__header">EROS I PSYCHE</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">STRASZNY DWÓR</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">EROS I PSYCHE</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">STRASZNY DWÓR</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">EROS I PSYCHE</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">STRASZNY DWÓR</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">EROS I PSYCHE</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                           <div class="spectacl__date__item">
                              <div class="date__header">STRASZNY DWÓR</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                              <div class="date__content">15.X.2017r.  godz. 18.00 i 21.00</div>
                           </div>
                        </div>
                     </div>
                     <p>
                        Cena jest uzależniona <br>
                        od wybranej strefy
                    </p>
                    <div class="price__label">CENA</div>
                    <div class="price__value">331-1154</div>
                    <div class="area__row">
                        <div class="box">
                            <span class="txt">Strefa cenowa</span>
                            <select>
                                <option>1 Strefa 250 PLN</option>
                                <option>2 Strefa 200 PLN</option>
                                <option>3 Strefa 150 PLN</option>
                            </select>
                        </div>
                    </div>
                    <a href="#" class="btn btn--brown btn--large">KUP ABONAMENT</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<?php include 'include/footer-butik.php' ?>