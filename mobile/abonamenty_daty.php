<?php include 'include/head.php' ?>
<div id="wrapper">
   <?php include 'include/header.php' ?> 
   <section class="main mobile-page" role="main">
      <div class="abonaments__dates__wrapper">
         <div class="abonaments__dates__head">
            <div class="image">
               <img src="../images-frogriot/abonamenty_header.jpg" alt="">
            </div>
            <div class="txt">
               Opera i balet 
               <nobr>a to polska</nobr>
               <nobr>właśnie</nobr>
            </div>
         </div>
         <div class="container">
            <div class="abonaments__dates">
               <div style="background-color: #fff;">
                  <h2 class="abonaments__header__main">Liczba abonamentów i strefa cenowa</h2>
                  <div class="abonaments__dates__layout">
                     <div class="abonaments__dates__content">
                        <div class="abonaments__dates__header">
                           <div class="abonaments__dates__header__row">
                              <div class="abonaments__dates__header__col abonaments__dates__header__col--abonaments" style="margin-bottom: 30px">
                                 <span class="txt">Liczba abonamentów</span>
                                 <div class="counter">
                                    <div class="counter">
                                       <div class="input_div">
                                          <button type="button" onclick="counter_minus(this)"><span></span></button>
                                          <input type="text" size="25" value="1" class="count" id="abonaments__count">   
                                          <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <button class="show__area__view" id="show__area__view">
                              Pokaż strefę cenową na widoku sali
                              </button> 
                              <div class="sala__mobile__switch">
                                 <h2 class="header">Plan sali moniuszki</h2>
                                 <img src="../../images-frogriot/plan_sali_moniuszki.jpg" alt="">
                              </div>
                           </div>
                        </div>
                        <div class="abonaments__switched__contents">
                           <div id="abonaments__content_1" class="abonaments__content">
                              <h2>Wybierz daty spektakli (4)</h2>
                              <div class="abonaments__dates__row">
                                 <h2>Jezioro Łabędzie</h2>
                                 <div class="abonaments__dates__slider">
                                    <label class="abonaments__dates__item" data-cart="1">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="2">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="3">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="4">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                 </div>
                              </div>
                              <div class="abonaments__dates__row">
                                 <h2>Czarodziejski flet</h2>
                                 <div class="abonaments__dates__slider">
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="6">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="7">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="8">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="9">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="10">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                 </div>
                              </div>
                              <div class="abonaments__dates__row">
                                 <h2>Dziadek do Orzechów i Król Myszy</h2>
                                 <div class="abonaments__dates__slider">
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="5">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="11">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="12">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="13">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="14">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="15">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                 </div>
                              </div>
                              <div class="abonaments__dates__row">
                                 <h2>Dziadek do Orzechów i Król Myszy</h2>
                                 <div class="abonaments__dates__slider">
                                    <label class="abonaments__dates__item" data-cart="16">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="17">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="18">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="19">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="20">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                 </div>
                              </div>
                              <div class="abonaments__dates__row">
                                 <h2>Cud albo krakowiaki i górale</h2>
                                 <div class="abonaments__dates__slider">
                                    <label class="abonaments__dates__item" data-cart="21">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="22">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="23">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="24">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                    <label class="abonaments__dates__item" data-cart="25">
                                       <input type="checkbox">
                                       <div class="abonaments__dates__item__content">
                                          <div class="date">07 grudnia 2017</div>
                                          <div class="day">piątek 19:00</div>
                                          <span class="btn btn--grey">wybieram</span>
                                       </div>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div id="abonaments__content_2" style="display: none" class="abonaments__content">
                              <div class="abonaments__content_room__header">
                                 <h2>Plan sali moniuszki</h2>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="abonaments__dates__side" style="display: none">
                        <h2>Podsumowanie wyboru</h2>
                        <div id="cart-items">
                           <!--<div class="cart__item__row">
                              <div class="cart__item__row__name">Jezioro Łabędzie</div>    
                              <div class="cart__item__row__date">
                                  09 Grudnia 2017, piątek 21:00
                              </div>
                              <button class="cart__item__row__trash confirm"></button>
                              </div> 
                              <div class="cart__item__row">
                              <div class="cart__item__row__name">Jezioro Łabędzie</div>    
                              <div class="cart__item__row__date">
                              09 Grudnia 2017, piątek 21:00
                              </div>
                              <button class="cart__item__row__trash confirm">
                              <img src="../images-frogriot/svg/Kosz.svg" alt="">
                              </button>
                              </div>-->
                        </div>
                        <div class="cart__item__foot">
                           <a href="abonamenty_daty_2.html" class="btn btn--large btn--grey">Dalej</a>
                           <div class="cart__item__foot__note">
                              Należy wybrać daty wszystkich spektakli w ramach abonamentu aby przejść dalej
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<?php include 'include/footer-butik.php' ?>