function slider() {
  let owl = $('.owl-carousel').owlCarousel({
    responsive: {
      0: {
        items: 1
      }
      , 992: {
        items: 2
      }
      , 1366: {
        items: 3
      }
    },
    margin: 20,
    loop: true,
    stagePadding: 0,
    nav: true,
    navText: ["<div class='left noselect clickable'><img class='movePrevCarousel' style='transform: rotate(180deg); background: white;' src='./images/icons/arrow.png' alt='prev' /></div>",
     "<div class='right noselect clickable'><img class='moveNextCarousel' style='background: white;' src='./images/icons/arrow.png' alt='next' /></div>"],
    dots: false,
    lazyLoad: false,
    autoplay: false,
    autoplaySpeed: 800,
    autoplayHoverPause: true
  })
  owl.trigger('next.owl.carousel', [1]);
}