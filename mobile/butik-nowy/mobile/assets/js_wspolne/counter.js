var count = 0;

function counter_plus(elem) {
  count = $(elem)
    .parents(".counter")
    .find(".count")
    .val();
  count++;
  $(elem)
    .parents(".counter")
    .find(".count")
    .val(count);
}

function counter_minus(elem) {
  count = $(elem)
    .parents(".counter")
    .find(".count")
    .val();
  if (count > 0) {
    count--;
  }
  $(elem)
    .parents(".counter")
    .find(".count")
    .val(count);
}
