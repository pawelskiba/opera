var TWON = function() {
  this.init();
};

TWON.prototype.init = function() {
  var _this = this;

  _this.plugins();
  _this.events();
};

TWON.prototype.plugins = function() {
  var _this = this;

  /* custom select */
  _this.selects = document.querySelectorAll("select:not([no-change])");
  for (var i = 0; i < _this.selects.length; i++)
    _this.selects[i].customSelect();

  /* show more content */
  config.windowload.push(function() {
    _this.eventsList = document.querySelectorAll(".show-more");
    for (var i = 0; i < _this.eventsList.length; i++)
      _this.eventsList[i].showMore(".event");
  });

  /* swiper */
  _this.swipers = document.querySelectorAll(".swiper-container");
  for (var i = 0; i < _this.swipers.length; i++) _this.swipers[i].swiperInit();

  /* accordion */
  _this.accordions = document.querySelectorAll(".accordion .toggle");
  for (var i = 0; i < _this.accordions.length; i++)
    _this.accordions[i].accordion();
};

TWON.prototype.events = function() {
  var _this = this;

  /* remove size from images */
  var imgs = document.querySelectorAll("img");
  for (var i = 0; i < imgs.length; i++) {
    imgs[i].removeAttribute("width");
    imgs[i].removeAttribute("height");
  }
  /* main menu */
  let el = document.querySelector("#main-menu .hamburger");

  if (el) {
    el.attach(config.e.click, function(e) {
      e.preventDefault();
      document.body.cssClass("toggle", "open-menu");
      document.body.cssClass("remove", "open-my-account");
    });
  }

  el = document.querySelector("#main-menu .my-account");
  if (el) {
    el.attach(config.e.click, function(e) {
      e.preventDefault();
      document.body.cssClass("toggle", "open-my-account");
      document.body.cssClass("remove", "open-menu");
    });
  }

  el = document.querySelector("#overlay");
  if (el) {
    el.attach(config.e.click, function(e) {
      e.preventDefault();
      document.body.cssClass("remove", "open-menu");
    });
  }

  /* monit */
  if (document.querySelector(".monit")) {
    var height = document.querySelector(".monit").clientHeight;
    document.querySelector(".monit").style.height = height + "px";

    if (localStorage.monit === "hidden") {
      document.querySelector(".show-monit").cssClass("remove", "hidden");
      document.querySelector(".monit").cssClass("add", "inactive");
    }
    document.querySelector(".monit").attach("click", function(e) {
      document.querySelector(".show-monit").cssClass("remove", "hidden");
      document.querySelector(".monit").cssClass("add", "inactive");
      localStorage.monit = "hidden";
      e.preventDefault();
    });
    document.querySelector(".show-monit").attach("click", function(e) {
      document.querySelector(".show-monit").cssClass("add", "hidden");
      document.querySelector(".monit").cssClass("remove", "inactive");
      localStorage.monit = "visible";
      e.preventDefault();
    });
  }

  /* change calendar category */
  /*if (document.querySelector("#calendar-category")) {
    var events = document.querySelectorAll(".event");
    if (events.length === 0)
      document.querySelector("#no-events").cssClass("remove", "inactive");
    document
      .querySelector("#calendar-category select")
      .attach("change", function(e) {
        var events = document.querySelectorAll(".event"),
          count = 0;
        if (this.value === "category-0") {
          for (var i = 0; i < events.length; i++) {
            count++;
            events[i].cssClass("remove", "inactive");
          }
        } else {
          for (var i = 0; i < events.length; i++) {
            if (events[i].hasClass("data-" + this.value)) {
              count++;
              events[i].cssClass("remove", "inactive");
            } else events[i].cssClass("add", "inactive");
          }
        }
        console.log(count);
        if (count > 0)
          document.querySelector("#no-events").cssClass("add", "inactive");
        else
          document.querySelector("#no-events").cssClass("remove", "inactive");
      });
  }*/

  /* change cast */
  var casts = document.querySelectorAll(".cast-cnt");
  for (var i = 0; i < casts.length; i++) casts[i].cssClass("add", "inactive");
  if (document.querySelector("#cast")) {
    document
      .querySelector(".cast-" + document.querySelector("#cast").value)
      .cssClass("remove", "inactive");
    document.querySelector("#cast").attach("change", function(e) {
      var casts = document.querySelectorAll(".cast-cnt");
      for (var i = 0; i < casts.length; i++)
        casts[i].cssClass("add", "inactive");
      document
        .querySelector(".cast-" + this.value)
        .cssClass("remove", "inactive");
      if (
        document
          .querySelector(".cast-" + this.value)
          .querySelector(".cast-title") != undefined &&
        document
          .querySelector(".cast-" + this.value)
          .querySelector(".cast-title").innerHTML != ""
      ) {
        document.querySelector(
          ".cast-header"
        ).innerHTML = document
          .querySelector(".cast-" + this.value)
          .querySelector(".cast-title").innerHTML;
      } else {
        document.querySelector(
          ".cast-header"
        ).innerHTML = document
          .querySelector(".cast-header")
          .getAttribute("data-title");
      }
    });
  }
  /* add photo info */
  var infos = document.querySelectorAll(".full-size");
  for (var i = 0; i < infos.length; i++) {
    var text = "";
    if (infos[i].children[0] != undefined) text = infos[i].children[0].alt;
    if (text) {
      var i = document.createElement("span");
      i.className = "info";
      i.innerHTML = text + '<a href="#" class="icon"></a>';
      infos[0].appendChild(i);

      i.attach(config.e.click, function(e) {
        e.preventDefault();
        this.cssClass("toggle", "open");
      });
    }
  }
  /* generate videos */
  if (document.querySelector(".video-cnt")) {
    if (document.querySelector("#html-video")) {
      var cnt = document.querySelector("#html-video"),
        video = document.createElement("video"),
        videos = cnt.prop("get", "data-src").split("|");

      video.setAttribute("poster", document.querySelector(".poster img").src);
      video.setAttribute("controls", "controls");
      cnt.appendChild(video);
      for (var i = 0; i < videos.length; i++) {
        var source = document.createElement("source");
        source.src = videos[i];
        source.setAttribute(
          "type",
          "video/" + videos[i].split(".")[videos[i].split(".").length - 1]
        );
        video.appendChild(source);
      }
      document.querySelector(".video-cnt").children[0].style.height =
        parseInt(window.innerWidth * 0.5625, 10) + "px";
      config.windowresize.push(function() {
        document.querySelector(".video-cnt").children[0].style.height =
          parseInt(window.innerWidth * 0.5625, 10) + "px";
      });
    }
    if (document.querySelector("#youtube-video")) {
      var cnt = document.querySelector("#youtube-video"),
        video = document.createElement("a"),
        youtube = cnt.prop("get", "data-src");

      video.cssClass("add", "play");
      video.setAttribute("target", "_blank");
      video.href = youtube;
      video.innerHTML =
        '<img alt="" src="' +
        document.querySelector(".poster img").src +
        '"><span class="icon"></span>';

      cnt.appendChild(video);
    }
  }
  /* next'n'prev news */
  if (document.querySelector("#news-switch")) {
    var currentId = document
        .querySelector("#news-switch")
        .prop("get", "data-news"),
      currentNews = document.querySelector(
        '.next-prev-news-list li[data-news="' + currentId + '"]'
      );

    if (currentNews.previousElementSibling)
      document.querySelector("#news-switch .next").href =
        currentNews.previousElementSibling.children[0].href;
    else
      document.querySelector("#news-switch .next").cssClass("add", "inactive");
    if (currentNews.nextElementSibling)
      document.querySelector("#news-switch .prev").href =
        currentNews.nextElementSibling.children[0].href;
    else
      document.querySelector("#news-switch .prev").cssClass("add", "inactive");
    document.querySelector(".next-prev-news-list").cssClass("add", "inactive");
  }
  /* calendar lang switch */
  if (document.querySelector("#calendar-date")) {
    var langs = document.querySelectorAll(".lang-switch");
    for (var i = 0; i < langs.length; i++) {
      if (!langs[i].hasClass("current")) {
        var a = langs[i].querySelector("a");
        a.href =
          a.href.split("data")[0] +
          (document.location.pathname.split("data")[1]
            ? "data" + document.location.pathname.split("data")[1]
            : "");
      }
    }
  }
  /* go to desktop version */
  if (document.querySelector("#goto-desktop")) {
    document.querySelector("#goto-desktop").attach("click", function(e) {
      e.preventDefault();
      document.cookie = "mobile=1; path=/";
      document.location = this.href;
    });
  }
  window.onload = function() {
    for (var i = 0; i < config.windowload.length; i++) config.windowload[i]();
  };
  window.onorientationchange = function() {
    for (var i = 0; i < config.windoworientation.length; i++)
      config.windoworientation[i]();
  };
  window.onresize = function() {
    for (var i = 0; i < config.windowresize.length; i++)
      config.windowresize[i]();
  };
};

var twon = new TWON();
