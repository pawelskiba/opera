<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">
         <!--   -->

         <div class="fr-container">

               <div class="fr-container__inner">

                  <div class="form__section-header form__section-header--center text-center"><strong>Dziękujemy za złożenie zamówienia</strong></div>
                  <div class="form__output form__output--center">
                     Czekamy na potwierdzenie płatności. Po jej zaksięgownaniu otrzymasz wiadomość e-mail.
                  </div>
                  <div class="form-typ-btn-wrapper">
                     <a href="#" class="form__btn">WRÓC DO SKLEPU</a>
                  </div>

                  <div class="row">
                     <div class="col-12"><div class="form__section-header"><strong>ZOBACZ INNE SPEKTAKLE</strong></div></div>
                     <div class="col-12 col-md-6 form-typ-col">
                        <div class="cart-prod">
                           <div class="cart-prod__thumb">
                              <img src="pliki-serwer/jezioro_labedzie.jpg" alt="miniaturka">
                           </div>
                           <div class="cart-prod__content">
                              <h3>JEZIORO ŁABĘDZIE</h3>
                              <p>22 sierpnia 2018, poniedziałek 19:00</p>
                              <p>Warszawa, sala Moniuszki</p>
                           </div>
                        </div>
                        <a href="#" class="form__btn">KUP BILET</a>
                     </div>
                     <div class="col-12 col-md-6 form-typ-col">
                        <div class="cart-prod">
                           <div class="cart-prod__thumb">
                              <img src="pliki-serwer/dziadek_do_orzechow_i_krol_myszy.png" alt="miniaturka">
                           </div>
                           <div class="cart-prod__content">
                              <h3>DZIADEK DO ORZECHÓW I KRÓL MYSZY</h3>
                              <p>22 sierpnia 2018, poniedziałek 19:00</p>
                              <p>Warszawa, sala Moniuszki</p>
                           </div>
                        </div>
                        <a href="#" class="form__btn">KUP BILET</a>
                     </div>
                  </div>

               </div>

            </div>

      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   