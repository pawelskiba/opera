<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<section class="main mobile-page" role="main">
   <div class="container">
      <div class="container-inner">
         <div class="butik_2" style="background: #fff; margin-bottom: 60px;">
            <ul class="breadcrumbs">
               <li><a href="#">Publikacje</a></li>
               <li>Mój tata tańczy w balecie</li>
            </ul>
            <div class="butik_desc_head">
               <span class="etiquete etiquete--black">tylko u nas</span>
               <div class="butik_desc_head__images">
                  <div class="butik_desc_head_slider">
                     <div class="butik_desc_head__main">
                        <a class="image-link" href="../images-frogriot/butik3/main.jpg"><img src="../images-frogriot/butik3/main.jpg" alt=""></a>
                        <a class="image-link" href="../images-frogriot/butik3/main.jpg"><img src="../images-frogriot/butik3/main.jpg" alt=""></a>
                        <a class="image-link" href="../images-frogriot/butik3/main.jpg"><img src="../images-frogriot/butik3/main.jpg" alt=""></a>
                        <a class="image-link" href="../images-frogriot/butik3/main.jpg"><img src="../images-frogriot/butik3/main.jpg" alt=""></a>
                        <a class="image-link" href="../images-frogriot/butik3/main.jpg"><img src="../images-frogriot/butik3/main.jpg" alt=""></a>
                        <!-- <img src="../images-frogriot/butik3/main.jpg" alt="">
                        <img src="../images-frogriot/butik3/main.jpg" alt="">
                        <img src="../images-frogriot/butik3/main.jpg" alt="">
                        <img src="../images-frogriot/butik3/main.jpg" alt="">
                        <img src="../images-frogriot/butik3/main.jpg" alt=""> -->
                     </div>
                  </div>
               </div>
               <div class="butik_desc_head__content">
                  <div class="header">
                     <span class="txt">Mój tata tańczy w balecie</span> 
                  </div>
                  <div class="price price--disqount">49,00 PLN</div>
                  <div class="price">39,00 PLN</div>
                  <ul class="details">
                     <li>Metalowy pin z autorskim nadrukiem</li>
                     <li>Średniaca : ok 3,7cm</li>
                     <li>zdjęcia Robert Wolański / SONY</li>
                  </ul>
                  <div class="avaliable">
                     <img src="../images-frogriot/butik2/tick.jpg" alt="">
                     Dostępny w sklepie
                  </div>
                  <div class="butik_desc_head__content__foot">
                     <div class="quantity">
                        <span class="txt">Ilość produktów</span>
                        <div class="counter">
                           <div class="counter">
                              <div class="input_div">
                                 <button type="button" onclick="counter_minus(this)"><span></span></button>
                                 <input type="text" size="25" value="1" class="count">   
                                 <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <button class="btn btn--brown btn--large butik_desc_head__content__btn">dodaj do koszyka</button>
                  </div>
               </div>
            </div>
            <div class="butic_more_info">
               <div class="row">
                  <div class="col-sm-7">
                     <img src="../images-frogriot/butik3/more_info.jpg" alt="">
                  </div>
                  <div class="col-sm-5">
                     <h2>Więcej informacji</h2>
                     <p>
                        Mela ma siedem lat, kocha Mimi, lalę, którą uszyła dla niej mama i swojego tatę tancerza, który zabrał ją do swojej pracy. Niezwykłej pracy. Czym można zdziwić się w teatrze, co zobaczyć za kulisami, jakie skarby odkryć, kogo przestraszyć się na scenie? 
                     </p>
                     <ul>
                        <li>Zdjęcia: Robert Wolański / SONY</li>
                        <li>Tekst: Iwona Witkowska</li>
                        <li>Na zdjęciach: Amelia i Maksim Woitiul</li>
                        <li>Oprawa: twarda</li>
                        <li>Liczba stron: 112</li>
                        <li>Wymiary: 24,5 x 32,5 cm</li>
                        <li>ISBN 978-83-65161-66-6</li>
                     </ul>
                  </div>
               </div>
               <div class="special__products">
                  <h2>dostęne również</h2>
                  <div class="row">
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/butik2/related.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Koncert inauguracyjny otwarcia wystawy Polin
                              </div>
                              <div class="price">
                                 <div class="actual">125.00 PLN</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/butik2/related-06.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Kalendarz na 2018 rok
                              </div>
                              <div class="price">
                                 <div class="disqount">125.00 PLN</div>
                                 <div class="actual">
                                    50.00 PLN
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/butik2/related-07.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Ryszard Winiarski katalogo wystawy
                              </div>
                              <div class="price">
                                 <div class="actual">
                                    15.00 PLN
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="bestsellers">
                  <h2>Polecamy</h2>
                  <div class="produts__items">
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers_00.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Spinka do krawata
                           </div>
                           <div class="price"> 125,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Marcella Sembrich-Kochańska
                              Artystka świata
                           </div>
                           <div class="price"> 50,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-09.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Pągowski w Operze katalog wystawy
                           </div>
                           <div class="price"> 25,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-10.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Bransoletka na  sznurku
                           </div>
                           <div class="price"> 155,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-11.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Ryszard Winiarski
                              katalog wystawy
                           </div>
                           <div class="price"> 68,00 PLN</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="shopping_box">
                  <div class="shopping_box__icons">
                     <h2>Łatwe zakupy online</h2>
                     <div class="items">
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-zakupy.jpg" alt="">
                           </div>
                           <div class="txt">Bezpieczne<br>zakupy</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-samolot.jpg" alt="">
                           </div>
                           <div class="txt">Wysyłamy<br>wszędzie</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kalendarz.jpg" alt="">
                           </div>
                           <div class="txt">30 dni<br>na zwrot</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kciuk.jpg" alt="">
                           </div>
                           <div class="txt">Pozytywne opinie<br>klientów</div>
                        </div>
                     </div>
                  </div>
                  <div class="shopping_box__payments">
                     <h2>Metody płatności</h2>
                     <div class="icons">
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-02.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-03.jpg" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="shooping__shop">
      <div class="img-box">    
         <img src="../images-frogriot/butik/zakupy.jpg" alt="">
      </div>
      <div class="shopping__shop__txt">
         <h2>Zakupy w naszym sklepie stacjonarnym</h2>
         <p>
            Lorem ipsum dolor sit amet, consectetur 
            adipiscing elit. Aenean euismod bibendum 
            laoreet. Proin gravida dolor sit amet lacus 
            accumsan et viverra justo commodo. Proin
            sodales pulvinar tempor. Cum sociis
         </p>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>