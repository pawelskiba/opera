<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">
         <!--   -->

         <div class="fr-container">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs cart-tabs" id="cartTab" role="tablist">
               <li class="nav-item cart-tabs__item">
                  <a class="nav-link" data-toggle="tab" href="#koszyk1" role="tab" aria-controls="koszyk1" aria-selected="true">
                     <span>
                        <span class="first">1.</span>
                        <span>TWÓJ KOSZYK</span>
                     </span>
                  </a>
               </li>
               <li class="nav-item cart-tabs__item">
                  <a class="nav-link active" data-toggle="tab" href="#koszyk2" role="tab" aria-controls="koszyk2" aria-selected="false">
                     <span>
                        <span class="first">2.</span>
                        <span>DOSTAWA</span>
                     </span>
                  </a>
               </li>
               <li class="nav-item cart-tabs__item cart-tabs__item--last">
                  <a class="nav-link" data-toggle="tab" href="#koszyk3" role="tab" aria-controls="koszyk3" aria-selected="false">
                     <span>
                        <span class="first">3.</span>
                        <span>PODSUMOWANIE<br/>I PŁATNOŚĆ</span>
                     </span>
                  </a>
               </li>
            </ul>

            <div class="lockbar">
               <div class="lockbar__inner">
                  <div class="lockbar__clock">
                     <img src="../ikony/zegar.png" alt="ikona" />
                     <div>14:59</div>
                  </div>
                  <p>
                     Miejsca zostały obecnie zablokowane na potrzeby transakcji.
                  </p>
               </div>
            </div>

            <!-- Tab panes -->
            <form class="form--cart">
               <div class="tab-content">
                  <div class="tab-pane fade show active" id="koszyk1" role="tabpanel" aria-labelledby="koszyk1">

                     <section>
                        <div class="cart-container">

                           <div class="form__section-header"><strong>Bilety</strong></div>

                           <div class="cart-prod">
                              <div class="cart-prod__thumb">
                                 <img src="pliki-serwer/jezioro_labedzie.jpg" alt="miniaturka" />
                              </div>
                              <div class="cart-prod__content">
                                 <h3>JEZIORO ŁABĘDZIE</h3>
                                 <p>22 sierpnia 2018, poniedziałek 19:00</p>
                                 <p>Warszawa, sala Moniuszki</p>
                              </div>
                           </div>

                           <div class="cart-table">

                              <div class="row cart-table__row cart-table__row--icon">

                                 <div class="col-8 d-flex align-items-center cart-table__mb-20">
                                    <div>Balkon I, loża T, miejsce 3</div>
                                 </div>

                                 <div class="col-4 cart-table__price text-right d-flex align-items-center cart-table__mb-20">
                                    <a href="#"><img src="../ikony/usun.png" alt="ikona" /></a>
                                 </div>
                                 
                                 <div class="col-8 d-flex align-items-center">
                                    <select class="form__select form__select--small d-flex align-items-center">
                                       <option>Normalny</option>
                                       <option>Normalny</option>
                                       <option>Normalny</option>
                                    </select>
                                 </div>

                                 <div class="col-4 cart-table__price text-right d-flex align-items-center">
                                    <strong>30,00 PLN</strong>
                                 </div>

                              </div>

                              <div class="row cart-table__row cart-table__row--icon">

                                 <div class="col-8 d-flex align-items-center cart-table__mb-20">
                                    <div>Balkon I, loża T, miejsce 3</div>
                                 </div>

                                 <div class="col-4 cart-table__price text-right d-flex align-items-center cart-table__mb-20">
                                    <a href="#"><img src="../ikony/usun.png" alt="ikona" /></a>
                                 </div>
                                 
                                 <div class="col-8 d-flex align-items-center">
                                    <select class="form__select form__select--small d-flex align-items-center">
                                       <option>Normalny</option>
                                       <option>Normalny</option>
                                       <option>Normalny</option>
                                    </select>
                                 </div>

                                 <div class="col-4 cart-table__price text-right d-flex align-items-center">
                                    <strong>30,00 PLN</strong>
                                 </div>

                              </div>

                           </div>
                           <div class="row cart-table__summary">
                              <div class="col-12">
                                 <div class="cart-table__separator-2"></div>
                              </div>
                              <div class="col-6">
                                 <strong>Suma</strong>
                              </div>
                              <div class="col-6 text-right cart-table__price">
                                 <strong>62,00 PLN</strong>
                              </div>
                           </div>

                        </div>
                     </section>

                     <section>
                        <div class="cart-container">

                           <div class="form__section-header"><strong>Produkty</strong></div>

                           <div class="cart-prod">
                              <div class="cart-prod__thumb">
                                 <img src="pliki-serwer/miodownik.jpg" alt="miniaturka" />
                              </div>
                              <div class="cart-prod__content">
                                 <h3>MIODOWNIK</h3>
                              </div>
                           </div>

                           <div class="cart-table">

                              <div class="row cart-table__row">
                                 <div class="col-8 d-flex align-items-center">
                                    <div class="cart-table__sit"><strong>Liczba</strong></div>
                                 </div>
                                 <div class="col-4 cart-table__price text-right d-flex align-items-center">
                                    <strong>Cena</strong>
                                 </div>
                                 <div class="col-12">
                                    <div class="cart-table__separator cart-table__separator--bottom"></div>
                                 </div>
                              </div>

                              <div class="row cart-table__row">
                                 <div class="col-8 d-flex align-items-center">
                                    <div class="cart-table__sit cart-table__sit--icon">

                                    <div class="counter">
                                          <div class="counter">
                              <div class="input_div">
                                  <button type="button" onclick="counter_minus(this)"><span></span></button>
                                <input type="text" size="25" value="1" class="count">   
                                  <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                            </div>
                          </div>
                                  </div>

                                    </div>
                                 </div>
                                 <div class="col-4 cart-table__price cart-table__price--icon text-right d-flex align-items-center">
                                    <strong>30,00 PLN</strong>
                                    <a href="#"><img src="../ikony/usun.png" alt="ikona" /></a>
                                 </div>
                              </div>

                           </div>

                           <div class="row cart-table__summary cart-table__summary--honey">
                              <div class="col-12">
                                 <div class="cart-table__separator-2"></div>
                              </div>
                              <div class="col-6">
                                 <strong>Suma</strong>
                              </div>
                              <div class="col-6 text-right cart-table__price">
                                 <strong>62,00 PLN</strong>
                              </div>
                           </div>

                        </div>
                     </section>

                     <section class="recprod">
                        <div class="cart-container">

                           <div class="form__section-header"><strong>Rekomendowane produkty</strong></div>

                           <div class="recprod__inner prod-carousel">

                              <div class="recprod__item">
                                 <div class="recprod__thumb"><img src="pliki-serwer/produkt-2.jpg" alt="produkt" /></div>
                                 <div class="recprod__content">
                                    <div class="recprod__title">JEZIORO ŁABĘDZIE'17 - program</div>
                                    <div class="recprod__price">20,00 PLN</div>
                                    <a class="form__btn form__btn--addto" href="#">
                                       Dodaj do zamówienia
                                    </a>
                                 </div>
                              </div>

                              <div class="recprod__item">
                                 <div class="recprod__thumb"><img src="pliki-serwer/produkt-1.jpg" alt="produkt" /></div>
                                 <div class="recprod__content">
                                    <div class="recprod__title">JEZIORO ŁABĘDZIE'17 - plakat</div>
                                    <div class="recprod__price">45,00 PLN</div>
                                    <a class="form__btn form__btn--addto" href="#">
                                       Dodaj do zamówienia
                                    </a>
                                 </div>
                              </div>

                              <div class="recprod__item">
                                 <div class="recprod__thumb"><img src="pliki-serwer/produkt-3.jpg" alt="produkt" /></div>
                                 <div class="recprod__content">
                                    <div class="recprod__title">PASTOR</div>
                                    <div class="recprod__price">125,00 PLN</div>
                                    <a class="form__btn form__btn--addto" href="#">
                                       Dodaj do zamówienia
                                    </a>
                                 </div>
                              </div>

                           </div>

                        </div>
                     </section>

                     <section class="ordersumm">
                        <div class="cart-container">

                           <div class="form__section-header"><strong>Podsumowanie zamówienia</strong></div>

                           <div class="row">
                              <div class="col">
                                 <a class="fr-add fr-add--border-margin" href="#">
                                    <span class="fr-add__icon"></span>
                                    Dodaj kupon rabatowy
                                 </a>
                              </div>
                           </div>

                           <div class="cart-hideprice">
                              <div class="checkbox">
                                 <label class="checkbox__inner checkbox__inner--center">
                                    <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                                    <span class="checkbox__checkmark"></span>
                                    <div class="checkbox__content checkbox__content--small">Ukryj cenę na biletach<br/>Zamówione bilety nie będą zawierać informacji o cenie wybranego spektaklu.</div>
                                 </label>
                              </div>
                           </div>

                           <div class="ordersumm__row-wrp">
                              <div class="row">
                                 <div class="col-8">
                                    Prowizja za obsługę transakcji
                                 </div>
                                 <div class="col-4 text-right">
                                    <strong>2,00 PLN</strong>
                                 </div>
                              </div>
                           </div>

                           <div class="ordersumm__row-wrp">
                              <div class="row">
                                 <div class="col-8">
                                    <strong>Rabat okolicznościowy</strong>
                                 </div>
                                 <div class="col-4 text-right">
                                    <strong>-100,00 PLN</strong>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="ordersumm__row-wrp ordersumm__row-wrp--color">
                           <div class="row">
                              <div class="col-6 d-flex align-items-center">
                                 <strong>Do zapłaty</strong>
                              </div>
                              <div class="col-6 d-flex align-items-center text-right">
                                 <div class="ordersumm__finalprice"><strong>731,00 PLN</strong></div>
                              </div>
                           </div>
                        </div>
                        <div class="ordersumm__btns">

                           <a href="#" class="form__btn">ZALOGUJ SIĘ</a>
                           <a href="#" class="form__btn form__btn--grey">ZAREJESTRUJ SIĘ</a>
                           <a href="#" class="form__btn form__btn--grey">KUP BEZ REJESTRACJI</a>

                        </div>
                     </section>

                  </div>
                  <div class="tab-pane fade" id="koszyk2" role="tabpanel" aria-labelledby="koszyk2">koszyk2</div>
                  <div class="tab-pane fade" id="koszyk3" role="tabpanel" aria-labelledby="koszyk3">koszyk3</div>
               </div>
            </form>

         </div>

      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   