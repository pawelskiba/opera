<!DOCTYPE html>
<!-- saved from url=(0030)https://teatrwielki.pl/mobile/ -->
<html class="no-js pl" lang="pl" data-path="fileadmin/templates/default/"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<!-- 
	This website is powered by TYPO3 - inspiring people to share!
	TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
	TYPO3 is copyright 1998-2017 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
	Information and contribution at https://typo3.org/
-->

<!--<base href="https://teatrwielki.pl/">--><base href=".">


<meta name="generator" content="TYPO3 CMS">
<meta name="description" content="Teatr Wielki Opera Narodowa">
<meta name="keywords" content="teatr wielki, opera narodowa">


    <title>Wersja mobilna&nbsp;: Teatr Wielki Opera Narodowa</title>
    <meta name="rating" content="general">
    <meta name="robots" content="noindex, nofollow">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <meta name="viewport" content="user-scalable=0,width=device-width,initial-scale=1,maximum-scale=1">        
    <script>var mobile_404;</script>       
    
    <!--<link rel="stylesheet" href="../../desktop/pliki-serwer/head-f456ecb483759b9a7213f8fde694efd5.merged.gz.css">  -->
    

    <link rel="stylesheet" media="all" href="../css/counter.css">

    <link rel="stylesheet" href="../fonts/fonts.css">
    
    <link rel="stylesheet" href="../css/font-awesome.css">
    
    <link rel="stylesheet" href="../css/miejsca.css">
    
    <link rel="stylesheet" href="../css/mapy.css">
    
    <link rel="stylesheet" href="../css/seat.css">
    
    <link rel="stylesheet" href="../css/radio.css">
    
    <link rel="stylesheet" media="screen" href="../dist/mobile.min.css">



    <link rel="icon" type="image/gif" href="https://teatrwielki.pl/fileadmin/templates/default/img/favicon.gif">     <meta property="og:title" content="Wersja mobilna">
    <meta property="og:description" content="Teatr Wielki Opera Narodowa">
    <meta property="og:type" content="website">
    <meta property="og:image" content="">
    <meta property="og:site_name" content="Teatr Wielki Opera Narodowa">
    
    <link rel="icon" type="image/icon" href="https://teatrwielki.pl/fileadmin/templates/default/img/layout/favicon.ico">
    <link rel="stylesheet" href="pliki-serwer/main.css">
    
    
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TQLWWB');</script><style type="text/css">
:root [href^="//69oxt4q05.com/"],
:root [class^="ad-"],
:root [class^="advertisement"],
:root [class$="-ads"],
:root [class*="cg-template"],
:root #widget-shop,
:root .widget-ads-content,
:root .sliderads,
:root .ppa-slot,
:root #pixad-box,
:root .onnetwork-video,
:root .linkSponsorowany,
:root #advertisments,
:root .adsense,
:root .ads,
:root .adform-slot,
:root [id^="crt-"],
:root #top-reklama,
:root #top_advertisement,
:root #advertising_box,
:root .single_ad_300x250,
:root .td-a-rec,
:root div.ads-s,
:root #image_pino,
:root #reklama_top,
:root .reklamastoproc,
:root .reklama-top,
:root .reklama-rectangle,
:root .reklama_gora,
:root #playerads,
:root #pasekSingle,
:root #LinkArea\:gry,
:root [id^="ad_widget"],
:root #content-stream,
:root .cg3-products,
:root .anal-post-popularne,
:root [href^="http://tc.tradetracker.net/"],
:root [href^="http://s.click.aliexpress.com/"],
:root a[href*="://converti.se/click/"],
:root a[href*="://clicks.fortunaaffiliates.com/"],
:root .adv_container,
:root .adsbyg,
:root .adbox,
:root #ads,
:root .trizer-bnr,
:root [id*="ad-container"],
:root .ad,
:root a[href*="/adserwer/"],
:root #content > #right > .dose > .dosesingle,
:root #content > #center > .dose > .dosesingle
{ display: none !important; }</style>
<!-- End Google Tag Manager -->
<link id="avast_os_ext_custom_font" href="chrome-extension://eofcbnmajmjmplflapaojjnihcjkigck/common/ui/fonts/fonts.css" rel="stylesheet" type="text/css"><style type="text/css">.backpack.dropzone {
  font-family: 'SF UI Display', 'Segoe UI';
  font-size: 15px;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 250px;
  height: 150px;
  font-weight: lighter;
  color: white;
  will-change: right;
  z-index: 2147483647;
  bottom: 20%;
  background: #333;
  position: fixed;
  user-select: none;
  transition: left .5s, right .5s;
  right: 0px; }
  .backpack.dropzone .animation {
    height: 80px;
    width: 250px;
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/hoverstate.png") left center; }
  .backpack.dropzone .title::before {
    content: 'Save to'; }
  .backpack.dropzone.closed {
    right: -250px; }
  .backpack.dropzone.hover .animation {
    animation: sxt-play-anim-hover 0.91s steps(21);
    animation-fill-mode: forwards;
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/hoverstate.png") left center; }

@keyframes sxt-play-anim-hover {
  from {
    background-position: 0px; }
  to {
    background-position: -5250px; } }
  .backpack.dropzone.saving .title::before {
    content: 'Saving to'; }
  .backpack.dropzone.saving .animation {
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/saving_loop.png") left center;
    animation: sxt-play-anim-saving steps(59) 2.46s infinite; }

@keyframes sxt-play-anim-saving {
  100% {
    background-position: -14750px; } }
  .backpack.dropzone.saved .title::before {
    content: 'Saved to'; }
  .backpack.dropzone.saved .animation {
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/saved.png") left center;
    animation: sxt-play-anim-saved steps(20) 0.83s forwards; }

@keyframes sxt-play-anim-saved {
  100% {
    background-position: -5000px; } }
</style></head>
   <body class="fr-body-mobile">