<footer>
   <div class="text black">
      <div class="buttons clearfix">
         <ul class="social">
            <li><a href="https://www.facebook.com/operanarodowa" target="_blank"><img src="pliki-serwer/social-fb.png" alt="Facebook"></a></li>
            <li><a href="https://www.youtube.com/user/operanarodowa" target="_blank"><img src="pliki-serwer/social-yt.png" alt="Youtube"></a></li>
            <li><a href="https://www.instagram.com/operanarodowa/" target="_blank"><img src="pliki-serwer/social-instagram_2016-white.png" alt="Instagram"></a></li>
         </ul>
         <a id="goto-desktop" href="https://teatrwielki.pl/" class="btn goto-desktop">Pełna wersja</a>
      </div>
      <div class="links">
         <ul class="clearfix">
            <li><a href="https://teatrwielki.pl/mobile/kalendarium/">Kalendarium</a></li>
            <li><a href="https://teatrwielki.pl/mobile/aktualnosci/">Aktualności</a></li>
            <li><a href="https://teatrwielki.pl/mobile/sezon-2018-19/">Sezon 2018/19</a></li>
            <li><a href="https://teatrwielki.pl/mobile/bilety-i-abonamenty/">Bilety i abonamenty</a></li>
            <li><a href="https://teatrwielki.pl/mobile/kontakt/">Kontakt</a></li>
            <li><a href="https://teatrwielki.pl/mobile/wystawy-201819/">Wystawy 2018/19</a></li>
            <li><a href="https://teatrwielki.pl/mobile/pasieka/">Pasieka w Teatrze Wielkim</a></li>
         </ul>
      </div>
   </div>
   <div class="text">
      <div class="buttons clearfix">
         <!--<a href="" class="btn" target="_blank">Dla pracowników</a>-->
         <a href="https://teatrwielki.pl/mobile/polityka-prywatnosci/" class="btn">Polityka prywatności</a>
      </div>
      <address>Teatr Wielki - Opera Narodowa, 
         Plac Teatralny 1, 00-950 Warszawa 
         skrytka pocztowa 59
      </address>
      <p class="direct-contact">Rezerwacja miejsc: <a href="tel:48228265019">+48 22 826 50 19</a><br>
         Centrala: tel. <a href="tel:48226920200">+48 22 692 02 00</a><br>
         e-mail: <a href="mailto:office@teatrwielki.pl">office@teatrwielki.pl</a>
      </p>
      <a href="http://www.pois.gov.pl/" target="_blank"><img src="pliki-serwer/ue.gif" alt="EU" class="eu"></a>
      <p class="copyrights">© Teatr Wielki Opera Narodowa, 2016</p>
   </div>
</footer>
   
   <?php include 'javascripts.php' ?>

</body>
</html>