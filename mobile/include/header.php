<header class="top__header">
    <nav id="main-menu">
        <div class="topmenu clearfix">
            <ul>
                <li><a href="https://teatrwielki.pl/" class="hamburger"><span>Menu</span></a></li>
                <li class="lang-switch current"> <a href="https://teatrwielki.pl/mobile/" class="lang"><span>pl</span></a> </li><li class="lang-switch"> <a href="https://teatrwielki.pl/en/mobile/" class="lang"><span>en</span></a> </li>
                <li>
                    <a class="fr-cart" href="#">
        <img src="../ikony/icon-cart.svg" alt="Koszyk">
        <span class="fr-cart__counter">7</span>
    </a>
                </li>
            </ul>
            <a href="butik.php" class="logo">

<img alt="Teatr Wielki Opera Narodowa" src="../ikony/logo.jpg">

</a>
        </div>
        <div class="submenu">

            <ul class="">
                <li><a href="#">Publikacje</a></li>
                <li><a href="#">Gadżety</a></li>
                <li><a href="#">Stacjonarnie</a></li>
                <li><a href="#">Moniuszko200</a></li>
                <li><a href="#">Dla dzieci</a></li>
                <li><a href="#">Plakaty</a></li>
                <li><a href="#">Bilety</a></li>
            </ul>
            
        </div>
    </nav>
</header>