<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>Zapomniałeś hasła?</strong></div>

                        <p class="fr-popup__text">
                        Wprowadź proszę adres e-mail użyty podczas rejestracji.<br/>
                        Na podany adres zostanie wysłany Ci e-mail w celu potwierdzenia zmiany hasła.<br/>
                        Hasło zostanie zmienione dopiero po kliknięciu w znajdujący się tam link.
                        </p>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>ADRES E-MAIL</strong></label>
                              <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn">WYŚLIJ</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>

      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   