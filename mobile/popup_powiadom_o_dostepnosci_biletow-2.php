<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="Form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>POWIADOM O DOSTĘPNOŚCI BILETÓW</strong></div>
                        
                        <div class="fr-popup__text">
                        Twój mail dodano do bazy dostępności biletów na <br/>spektakl "Czarodziejski flet".
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn">ZAMKNIJ</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>

      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   