<?php include 'include/head.php' ?>
<div id="wrapper">
   <?php include 'include/header.php' ?>
   <section class="main" role="main">
      <div class="mobile-page">
         <div class="fr-popup fr-popup--choose_tickets fr-popup--choose_tickets--tickets">
            <div class="fr-popup--choose_tickets__head">
               <div class="fr-popup--choose_tickets__head__left ">
                  <img src="../images-frogriot/spektakl_thumb.jpg" alt="">
               </div>
               <div class="fr-popup--choose_tickets__head__right">
                  <div class="name">Jezioro Łabędzie</div>
                  <div class="hour">22 sierpnia 2018, poniedziałek 19:00, Sala moniuszki</div>
               </div>
            </div>
            <div class="mobile__tickets__content">
               <div class="mobile__tickets__content__header">
                  <div class="label-box">Wybierz poziom sali</div>
                  <div class="content-box">
                    <div class="filters-btn">Pokaż filtry</div>
                  </div>
               </div>

               <div id="filters">
                <div class="tickets__head">
                    <div class="tickets__discount">
                        <div class="fr-form__col__header">Rodzaj zniżki<span class="tooltip-box" data-tooltip-content="#tooltip"> <img src="../images-frogriot/icon-info.svg" class="icon-info" class="icon-info"" alt=""></span></div>
                        <select>
                            <option>Normlany</option>
                            <option>Kolejny</option>
                            <option>Następny</option>
                        </select>
                    </div>
                    <div class="tickets__facilities">
                        <div class="tickets_facilities__row">
                            <span class="label-box">
                            wybieranie miejsca z poziomu listy
                            </span>
                            <div class="checkbox-switch-ctn">
                            <input type="checkbox" name="checkbox3" id="checkbox3" class="ios-toggle">
                            <label for="checkbox3" id="show-filters-list" class="checkbox-label" data-off="off" data-on="on"></label>
                            </div>
                        </div>
                    </div>
                    <div class="choose_from_list">
                        <div class="choose_from_list__row">
                            <div class="label-box">Poziom sali</div>
                            <select>
                                <option>Parter</option>
                                <option>Piętro</option>
                                <option>Piętro II</option>
                            </select>
                        </div>
                        <div class="choose_from_list__row">
                            <div class="label-box">Rząd</div>
                            <select>
                                <option>I</option>
                                <option>II</option>
                                <option>III</option>
                                <option>IV</option>
                                <option>V</option>
                                <option>VI</option>
                                <option>VII</option>
                                <option>VIII</option>
                                <option>IX</option>
                                <option>X</option>
                                <option>XI</option>
                                <option>XII</option>
                            </select>
                        </div>
                        <div class="choose_from_list__row">
                            <div class="label-box">Miejsce</div>
                            <select>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                            </select>
                        </div>
                        <div class="choose_from_list__row">
                            <div class="label-box">Rodzaj zniżki</div>
                            <select>
                                <option>Normalny</option>
                                <option>Kolejny</option>
                                <option>Następny</option>
                            </select>
                        </div>
                        <a href="#" class="btn btn--large btn--brown">dodaj mejsce</a>
                    </div>
                    <div class="strefa_cenowa">
                        <span class="header">strefa cenowa</span>
                        <div class="tickets__prices">
                            <label class="checkbox-ctn checkbox-white">
                            <input type="radio" data-price="all" checked name="price">
                            <span class="state">Wszystkie</span>
                            </label>
                            <label class="checkbox-ctn checkbox-brown">
                            <input type="radio" data-price="150" name="price">
                            <span class="state">150 PLN</span>
                            </label>
                            <label class="checkbox-ctn checkbox-brown-light">
                            <input type="radio" data-price="120" name="price">
                            <span class="state">120 PLN</span>
                            </label>
                            <label class="checkbox-ctn checkbox-grey-light">
                            <input type="radio" data-price="100" name="price">
                            <span class="state">100 PLN</span>
                            </label>
                            <label class="checkbox-ctn checkbox-grey">
                            <input type="radio" data-price="60" name="price">
                            <span class="state">60 PLN</span>
                            </label>
                            <label class="checkbox-ctn checkbox-pink">
                            <input type="radio" data-price="30" name="price">
                            <span class="state">30 PLN</span>
                            </label>
                            <label class="checkbox-ctn checkbox-red">
                            <input type="radio" data-price="20" name="price">
                            <span class="state">20 PLN</span>
                            </label>
                            <!--<label class="checkbox-ctn checkbox-pink">
                            <input type="radio" data-price="10" name="price">
                            <span class="state">10 PLN</span>
                            </label>-->
                        </div>
                    </div>
                    <div class="udogodnienia">
                        <span class="header">udogodnienia</span>
                        <div class="tickets__facilities">
                            <div class="tickets_facilities__row">
                            <span class="label-box">
                            dla osób niepełnosprawnych ruchowo
                            </span>
                            <div class="checkbox-switch-ctn">
                                <input type="checkbox" name="checkbox1" id="checkbox1" class="ios-toggle">
                                <label for="checkbox1" class="checkbox-label" data-off="off" data-on="on"></label>
                            </div>
                            </div>
                            <div class="tickets_facilities__row">
                            <span class="label-box">
                            dla osób niesłyszących (pętla indukcyjna)
                            </span>
                            <div class="checkbox-switch-ctn">
                                <input type="checkbox" name="checkbox2" id="checkbox2" class="ios-toggle">
                                <label for="checkbox2" class="checkbox-label" data-off="off" data-on="on"></label>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                   
                </div> 
            </div>

               <div class="mobile__tickets__content__content">
                  <div class="mobile__tickets__choose__sector__main__area mobile__tickets__choose__sector__redutowe">
                     <img src="../images-frogriot/sala_redutowe.png" alt="">
                     <button class="choose__sector choose__sector--sektor__A">Sektor A</button>
                     <button class="choose__sector choose__sector--sektor__B">Sektor B</button>
                     <button class="choose__sector choose__sector--sektor__C">Sektor C</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<?php include 'include/footer-butik.php' ?>