<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">
         <!--   -->

         <div class="fr-wrapper">
            <div class="fr-container">

               <form class="fr-container__inner">

                  <h1 class="fr-h1">Zarejestruj się już dziś, a zyskasz.</h1>

                  <ul class="form__list">
                     <li>szybszy dostęp do biletów i abonamentów</li>
                     <li>dostęp do promocji</li>
                     <li>dostęp do historii zamówień</li>
                  </ul>

                  <!-- Dane kupującego -->
                  <div class="form__section">

                     <div class="form__section-header"><strong>Dane Kupującego</strong></div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>IMIĘ</strong></label>
                           <input class="form__input" type="text" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>NAZWISKO</strong></label>
                           <input class="form__input" type="text" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>ADRES E-MAIL</strong></label>
                           <input class="form__input" type="email" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>HASŁO</strong></label>
                           <input class="form__input" type="password" />
                        </div>
                     </div>

                     <!-- form row -->
                     <div class="form__row">
                        <div class="form__col">
                           <label class="form__label"><strong>NUMER TELEFONU</strong></label>
                           <input class="form__input" type="text" />
                        </div>
                     </div>

                  </div>

                  <!-- Zgody prawne -->
                  <div class="form__section form__section--no-border form__section--no-margin-bottom">
                     
                     <div class="form__section-header"><strong>Zgody prawne</strong></div>

                     <div class="checkbox">
                        <h3 class="checkbox__header">Zgoda na przetwarzanie danych osobowych</h3>
                        <label class="checkbox__inner">
                           <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                           <span class="checkbox__checkmark"></span>
                           <div class="checkbox__content">Wyrażam zgodę na przechowywanie, przetwarzanie oraz przekazywanie organizatorowi spektaklu / imprezy podanych wyżej danych osobowych z zastrzeżeniem wykorzystania jedynie do realizacji niniejszej rezerwacji.</div>
                        </label>
                     </div>

                     <div class="checkbox">
                        <h3 class="checkbox__header">Regulamin</h3>
                        <label class="checkbox__inner checkbox__inner--center">
                           <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                           <span class="checkbox__checkmark"></span>
                           <div class="checkbox__content">Oświadczam, że zapoznałem/am się z <a href="#"><strong>Regulaminem sprzedaży online</strong></a> i akceptuję jego ustalenia.</div>
                        </label>
                     </div>

                     <div class="checkbox">
                        <h3 class="checkbox__header">Regulamin sklepu</h3>
                        <label class="checkbox__inner checkbox__inner--center">
                           <input class="checkbox__input" type="checkbox" name="zgoda1" required="">
                           <span class="checkbox__checkmark"></span>
                           <div class="checkbox__content">Akceptuję <a href="#">Regulaminem sklepu</a>.</div>
                        </label>
                     </div>

                  </div>

                  <!-- Przyciski -->
                  <div class="form__section form__section--last">
                     <div class="form__btns">
                        <a href="#" class="form__btn">ZAREJESTRUJ SIĘ</a>
                     </div>
                  </div>

               </form> 

            </div>
         </div>
      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   