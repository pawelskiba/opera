<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="mobile-page">
      <div class="container">
         <div class="butik__search__engine">
            <div class="container-inner">
               <input type="text" class="butik__search__engine__text">
               <button class="butik__search__engine__cta btn btn--brown btn--large">
               Szukaj
               </button>
            </div>
         </div>

         <div class="butik__filter">
            <div class="container-inner">
               <div class="butik__filter__inner">
                  <div class="row">
                     <div class="butik__filter__left col-sm-7">

                        <div class="butik-changeview">

                           <div class="butik-changeview__select">
                              <div>Sortuj wg</div>
                              <select id="sortby">
                                 <option value="original-order">--</option>
                                 <option value="sortname" data-direction="asc">Nazwa produktu: od A do Z</option>
                                 <option value="sortname" data-direction="desc">Nazwa produktu: od Z do A</option>
                                 <option value="sortprice" data-direction="asc">Cena produktu od najniższej</option>
                                 <option value="sortprice" data-direction="desc">Cena produktu od najwyższej</option>
                              </select>
                           </div>

                        </div>

                     </div>
                     <div class="butik__filter__right col-sm-5">
                        <div class="butik-showperpage">
                           <div>Pokaż</div>
                           <select>
                              <option>12</option>
                              <option>24</option>
                              <option>48</option>							  							  <option>96</option>
                           </select>
                           <div>na stronę</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="butik__search__engine__result" style="background-color: #fff">
            <div class="container-inner">
               <div class="special__products special__products--2">
                  <h2 class="butik__search__engine__result__header">Znaleźliśmy 6 produktów dla zapytania "OPERA"</h2>
                  <div class="row special__products__items">
                     <div class="special__products__item">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header sortname">
                                 CKoncert inauguracyjny otwarcia wystawy Polin
                              </div>
                              <div class="price">
                                 <div class="actual sortprice">1125.00 PLN</div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="special__products__item">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header sortname">
                                 Kalendarz na 2018 rok
                              </div>
                              <div class="price">
                                 <div class="disqount">125.00 PLN</div>
                                 <div class="actual sortprice">
                                    50.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="special__products__item">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-03.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header sortname">
                                 WRyszard Winiarski katalogo wystawy
                              </div>
                              <div class="price">
                                 <div class="actual sortprice">
                                    25.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="special__products__item">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header sortname">
                                 Doncert inauguracyjny otwarcia wystawy Polin
                              </div>
                              <div class="price">
                                 <div class="actual sortprice">55.00 PLN</div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="special__products__item">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header sortname">
                                 Aalendarz na 2018 rok
                              </div>
                              <div class="price">
                                 <div class="disqount">125.00 PLN</div>
                                 <div class="actual sortprice">
                                    70.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="special__products__item">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-03.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header sortname">
                                 Cyszard Winiarski katalogo wystawy
                              </div>
                              <div class="price">
                                 <div class="actual sortprice">
                                    715.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                  </div>
<!--                   <div class="row">
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search-04.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Koncert inauguracyjny otwarcia wystawy Polin
                              </div>
                              <div class="price">
                                 <div class="actual">125.00 PLN</div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Kalendarz na 2018 rok
                              </div>
                              <div class="price">
                                 <div class="disqount">125.00 PLN</div>
                                 <div class="actual">
                                    50.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Ryszard Winiarski katalogo wystawy
                              </div>
                              <div class="price">
                                 <div class="actual">
                                    15.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                  </div> -->
               </div>
               <div class="bestsellers">
                  <h2>Polecamy</h2>
                  <div class="produts__items">
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers_00.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Spinka do krawata
                           </div>
                           <div class="price"> 125,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Marcella Sembrich-Kochańska
                              Artystka świata
                           </div>
                           <div class="price"> 50,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-09.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Pągowski w Operze katalog wystawy
                           </div>
                           <div class="price"> 25,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-10.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Bransoletka na  sznurku
                           </div>
                           <div class="price"> 155,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-11.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Ryszard Winiarski
                              katalog wystawy
                           </div>
                           <div class="price"> 68,00 PLN</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="shopping_box">
                  <div class="shopping_box__icons">
                     <h2>Łatwe zakupy online</h2>
                     <div class="items">
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-zakupy.jpg" alt="">
                           </div>
                           <div class="txt">Bezpieczne<br>zakupy</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-samolot.jpg" alt="">
                           </div>
                           <div class="txt">Wysyłamy<br>wszędzie</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kalendarz.jpg" alt="">
                           </div>
                           <div class="txt">30 dni<br>na zwrot</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kciuk.jpg" alt="">
                           </div>
                           <div class="txt">Pozytywne opinie<br>klientów</div>
                        </div>
                     </div>
                  </div>
                  <div class="shopping_box__payments">
                     <h2>Metody płatności</h2>
                     <div class="icons">
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-02.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-03.jpg" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>