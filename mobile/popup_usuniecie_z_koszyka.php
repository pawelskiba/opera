<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">

         <div style="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="form--popup">

                     <p class="fr-popup__text">
                        Czy na pewno chcesz usunąć tę pozycje z koszyka?
                     </p>

                     <!-- Przyciski -->
                     <div class="form__section form__section--last">
                        <div class="form__btns">
                           <a href="#" class="form__btn">TAK</a>
                           <a href="#" class="form__btn form__btn--white">NIE</a>
                        </div>
                     </div>

                  </form>

               </div>

            </div>

         </div>
      
      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   