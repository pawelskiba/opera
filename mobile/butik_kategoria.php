<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="mobile-page">
      <div class="container">
         <div class="butik__search__engine">
            <div class="container-inner">
               <input type="text" class="butik__search__engine__text">
               <button class="butik__search__engine__cta btn btn--brown btn--large">
               Szukaj
               </button>
            </div>
         </div>
         <div class="butik__search__engine__result" style="background-color: #fff">
            <div class="container-inner">
               <div class="special__products">
                  <h2 class="butik__search__engine__result__header">Publikacje</h2>
                  <div class="row">
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Koncert inauguracyjny otwarcia wystawy Polin
                              </div>
                              <div class="price">
                                 <div class="actual">125.00 PLN</div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Kalendarz na 2018 rok
                              </div>
                              <div class="price">
                                 <div class="disqount">125.00 PLN</div>
                                 <div class="actual">
                                    50.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-03.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Ryszard Winiarski katalogo wystawy
                              </div>
                              <div class="price">
                                 <div class="actual">
                                    15.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">  
                              <img src="../images-frogriot/product_search-04.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Koncert inauguracyjny otwarcia wystawy Polin
                              </div>
                              <div class="price">
                                 <div class="actual">125.00 PLN</div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item  product__item--disqount">
                           <div class="label-box">
                              -30%
                           </div>
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Kalendarz na 2018 rok
                              </div>
                              <div class="price">
                                 <div class="disqount">125.00 PLN</div>
                                 <div class="actual">
                                    50.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="product__item product__item--disqount">
                           <div class="product__item__image">
                              <img src="../images-frogriot/product_search-02.jpg" alt="">
                           </div>
                           <div class="product__item__header">
                              <div class="header">
                                 Ryszard Winiarski katalogo wystawy
                              </div>
                              <div class="price">
                                 <div class="actual">
                                    15.00 PLN
                                 </div>
                              </div>
                           </div>
                           <div class="product__item__content">
                              Książeczka powstała w związku z wtstawą prac Józefa Wilkonia zorganizowaną w Galerii Opera Teatru Wielkiego - Opery Narodowej.
                           </div>
                        </div>
                     </div>
                  </div>
                  <nav class="pagination-wrapper">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Poprzednia</a></li>
                        <li class="page-item"><a class="page-link active" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">Następna</a></li>
                    </ul>
                </nav>
               </div>
               <div class="bestsellers">
                  <h2>Polecamy</h2>
                  <div class="produts__items">
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers_00.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Spinka do krawata
                           </div>
                           <div class="price"> 125,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Marcella Sembrich-Kochańska
                              Artystka świata
                           </div>
                           <div class="price"> 50,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-09.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Pągowski w Operze katalog wystawy
                           </div>
                           <div class="price"> 25,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-10.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Bransoletka na  sznurku
                           </div>
                           <div class="price"> 155,00 PLN</div>
                        </div>
                     </div>
                     <div class="product__item product__item--vertical">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik2/bestsellers-11.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Ryszard Winiarski
                              katalog wystawy
                           </div>
                           <div class="price"> 68,00 PLN</div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="shopping_box">
                  <div class="shopping_box__icons">
                     <h2>Łatwe zakupy online</h2>
                     <div class="items">
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-zakupy.jpg" alt="">
                           </div>
                           <div class="txt">Bezpieczne<br>zakupy</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-samolot.jpg" alt="">
                           </div>
                           <div class="txt">Wysyłamy<br>wszędzie</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kalendarz.jpg" alt="">
                           </div>
                           <div class="txt">30 dni<br>na zwrot</div>
                        </div>
                        <div class="item">
                           <div class="img-box">
                              <img src="../images-frogriot/butik/ikona-kciuk.jpg" alt="">
                           </div>
                           <div class="txt">Pozytywne opinie<br>klientów</div>
                        </div>
                     </div>
                  </div>
                  <div class="shopping_box__payments">
                     <h2>Metody płatności</h2>
                     <div class="icons">
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-02.jpg" alt="">
                        </div>
                        <div class="item">
                           <img src="../images-frogriot/butik/ikona_platnosci-03.jpg" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>