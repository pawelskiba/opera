<?php include 'include/head.php' ?>
<section class="main mobile-page" role="main">
   <?php include 'include/header.php' ?>   
   <div class="container">
      <div class="butik" style="background-color: #fff">
         <div class="butik__main-slider"  style="background: #22135c">
            <div class="butik__main-slider__item">
               <img src="../images-frogriot/butik/butik-slider.png" alt="">
               <div class="butik__main-slider__content">
                  <div class="butik__main-slider__txt">
                     Znajdź prezent na Święta Bożego Narodzenia
                  </div>
                  <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
               </div>
            </div>
            <div class="butik__main-slider__item">
               <img src="../images-frogriot/butik/butik-slider.png" alt="">
               <div class="butik__main-slider__content">
                  <div class="butik__main-slider__txt">
                     Znajdź prezent na Święta Bożego Narodzenia
                  </div>
                  <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
               </div>
            </div>
            <div class="butik__main-slider__item">
               <img src="../images-frogriot/butik/butik-slider.png" alt="">
               <div class="butik__main-slider__content">
                  <div class="butik__main-slider__txt">
                     Znajdź prezent na Święta Bożego Narodzenia
                  </div>
                  <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
               </div>
            </div>
            <div class="butik__main-slider__item">
               <img src="../images-frogriot/butik/butik-slider.png" alt="">
               <div class="butik__main-slider__content">
                  <div class="butik__main-slider__txt">
                     Znajdź prezent na Święta Bożego Narodzenia
                  </div>
                  <a href="#" class="btn btn--large btn--brown">Sprawdź nasze propozycje</a>
               </div>
            </div>
         </div>
         <div class="butik__news">
            <h2>Bilety</h2>
            <div class="butik__news__slider__order">
               <div class="butik__news__slider">
                  <div class="butik__news__slider__item">
                     <div class="butik__news__slider__item__image">
                        <img src="../images-frogriot/butik/akt_spektakl.jpg" alt="">
                     </div>
                  </div>
                  <div class="butik__news__slider__item">
                     <div class="butik__news__slider__item__image">
                        <img src="../images-frogriot/butik/akt_spektakl-02.jpg" alt="">
                     </div>
                  </div>
                  <div class="butik__news__slider__item">
                     <div class="butik__news__slider__item__image">
                        <img src="../images-frogriot/butik/akt_spektakl-03.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="butik__news__slider__text">
                  <p>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan dolor sit amet lacus accumsan.
                  </p>
                  <div class="mobile-xs-d-none mobile-sm-d-none mobile-md-d-none">
                     <a href="#">Zobacz wszystkie plakaty</a>
                     <a href="#">Zobacz wszystkie programy</a>
                     <a href="#">Zobacz wszystkie katalogi</a>
                  </div>
               </div>
            </div>
         </div>
         <div class="butik__publikacje">
            <h2>Publikacje</h2>
            <div class="butik__publikacje__row">
               <div class="butik__publikacje__image">
                  <img src="../images-frogriot/butik/nasze_publikacje.jpg" alt="">
                  <div class="butik__publikacje__image__extra">
                     <img src="../images-frogriot/butik/produkt.jpg" alt="">
                  </div>
               </div>
               <div class="product__item">
                  <div class="product__item__header">
                     <div class="header">
                        Prywatny alfabet śpiewaków
                     </div>
                     <div class="price">30.00 PLN</div>
                  </div>
                  <p>
                     W Prywatnym alfabecie śpiewaków Jacek Marczyński w formie przewodnika przedstawił sylwetki stu dwudziestu trzech śpiewaków, którzy odgrywają dziś ważne role na największych i najbardziej prestiżowych scenach operowych.
                  </p>
               </div>
            </div>
            <div class="special__products">
               <div class="row">
                  <div class="col col-sm-6">
                     <div class="product__item product__item--bestseller">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik/produkt-02.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Tomasz Ciecierski katalog wystawy
                           </div>
                           <div class="price">
                              <div class="actual">125.00 PLN</div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col col-sm-6">
                     <div class="product__item product__item--bestseller">
                        <div class="product__item__image">
                           <img src="../images-frogriot/butik/produkt-03.jpg" alt="">
                        </div>
                        <div class="product__item__header">
                           <div class="header">
                              Pągowski w Operze katalog wystawy
                           </div>
                           <div class="price">
                              <div class="disqount">125.00 PLN</div>
                              <div class="actual">
                                 50.00 PLN
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="special_edition">
               <h2>Edycja limitowana</h2>
               <div class="special_edition__box">
                  <div class="images">  
                     <img src="../images-frogriot/butik/special_edition_mobile.jpg" alt="">
                  </div>
                  <div class="special_edition__items">
                     <div class="special_edition__item">
                        <div class="header">
                           <div class="name">Miodownik</div>
                           <div class="price">125.00 PLN</div>
                        </div>
                        <div class="txt">
                           Inspirowany architekturą<br>
                           plastra miodu
                        </div>
                     </div>
                     <div class="special_edition__item">
                        <div class="header">
                           <div class="name">Napój Miłosny</div>
                           <div class="price">55.00 PLN</div>
                        </div>
                        <div class="txt">
                           Operowy miód wielokwiatowy<br> z teatralnej pasieki
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="shopping_box">
               <div class="shopping_box__icons">
                  <h2>Łatwe zakupy online</h2>
                  <div class="items">
                     <div class="item">
                        <div class="img-box">
                           <img src="../images-frogriot/butik/ikona-zakupy.jpg" alt="">
                        </div>
                        <div class="txt">Bezpieczne<br>zakupy</div>
                     </div>
                     <div class="item">
                        <div class="img-box">
                           <img src="../images-frogriot/butik/ikona-samolot.jpg" alt="">
                        </div>
                        <div class="txt">Wysyłamy<br>wszędzie</div>
                     </div>
                     <div class="item">
                        <div class="img-box">
                           <img src="../images-frogriot/butik/ikona-kalendarz.jpg" alt="">
                        </div>
                        <div class="txt">30 dni<br>na zwrot</div>
                     </div>
                     <div class="item">
                        <div class="img-box">
                           <img src="../images-frogriot/butik/ikona-kciuk.jpg" alt="">
                        </div>
                        <div class="txt">Pozytywne opinie<br>klientów</div>
                     </div>
                  </div>
               </div>
               <div class="shopping_box__payments">
                  <h2>Metody płatności</h2>
                  <div class="icons">
                     <div class="item">
                        <img src="../images-frogriot/butik/ikona_platnosci.jpg" alt="">
                     </div>
                     <div class="item">
                        <img src="../images-frogriot/butik/ikona_platnosci-02.jpg" alt="">
                     </div>
                     <div class="item">
                        <img src="../images-frogriot/butik/ikona_platnosci-03.jpg" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="shooping__shop">
      <div class="img-box">    
         <img src="../images-frogriot/butik/zakupy.jpg" alt="">
      </div>
      <div class="shopping__shop__txt">
         <h2>Zakupy w naszym sklepie stacjonarnym</h2>
         <p>
            Lorem ipsum dolor sit amet, consectetur 
            adipiscing elit. Aenean euismod bibendum 
            laoreet. Proin gravida dolor sit amet lacus 
            accumsan et viverra justo commodo. Proin
            sodales pulvinar tempor. Cum sociis
         </p>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>