<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="tours tour__calendar">
   <div class="tours__header">
      <h1>Kalendarium</h1>
      <div class="tour__head">
         <ul id="calendar-date" class="switch">
            <li>
               <span>Sezon 2018/2019</span>    
            </li>
            <li><span>styczeń 2019</span>  </li>
         </ul>
         <div id="calendar-category" class="select clearfix">
            <label>Kategoria:</label>
            <div class="custom-select">
               <span class="selected">Wszystkie</span>
               <select class="select-96223882">
                  <option value="category-0" selected="selected">Wszystkie</option>
                  <option value="category-1">Opera</option>
                  <option value="category-2">Balet</option>
                  <option value="category-3">Koncert</option>
                  <option value="category-4">Wydarzenia specjalne</option>
                  <option value="category-5">W trasie</option>
                  <option value="category-6">Koprodukcje</option>
                  <option value="category-7">Dla dzieci</option>
               </select>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <section id="content">
         <ul id="events-list" class="month-full-list">
            <li class="data-event data-day-11 data-category-1 data-event__tour" tabindex="0">
               <time datetime="2013-11-29">
               <span class="day">22</span>
               <span class="weekday">piątek</span>
               </time>
               <div class="data-event__events">
                  <div class="data-event__item">
                     <div class="event">
                        <h3>
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                              <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                           </a>
                           <div class="data-event__hour">
                              9:15
                           </div>
                        </h3>
                     </div>
                     <div class="price data-event__price">
                        <a class="btn btn--large btn--brown data-event__button" href="#">kup bilet</a>
                        <div class="data-event__available_tickets">Dostępne bilety: 17</div>
                     </div>
                  </div>
                  <div class="data-event__item">
                     <div class="event">
                        <h3>
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                              <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                           </a>
                           <div class="data-event__hour">
                              9:15
                           </div>
                        </h3>
                     </div>
                     <div class="price data-event__price">
                        <a class="btn btn--large btn--brown data-event__button" href="#">kup bilet</a>
                        <div class="data-event__available_tickets">Dostępne bilety: 5</div>
                     </div>
                  </div>
                  <div class="data-event__item">
                     <div class="event">
                        <h3>
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                              <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                           </a>
                           <div class="data-event__hour">
                              9:15
                           </div>
                        </h3>
                     </div>
                     <div class="price data-event__price">
                        <a class="btn btn--large btn--grey btn--inactive data-event__button" href="#">kup bilet</a>
                        <div class="data-event__available_tickets">Brak dostępnych miejsc</div>
                     </div>
                  </div>
                  <div class="data-event__item">
                     <div class="event">
                        <h3>
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                              <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                           </a>
                           <div class="data-event__hour">
                              9:15
                           </div>
                        </h3>
                     </div>
                     <div class="price data-event__price">
                        <a class="btn btn--large btn--grey btn--inactive data-event__button" href="#">kup bilet</a>
                        <div class="data-event__available_tickets">Brak dostępnych miejsc</div>
                     </div>
                  </div>
                  <div class="data-event__item">
                     <div class="event">
                        <h3>
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                              <!--CONTENT_replaced-->Wycieczka grupowa<!--CONTENT_replaced-->
                           </a>
                           <div class="data-event__hour">
                              9:15
                           </div>
                        </h3>
                     </div>
                     <div class="price data-event__price">
                        <a class="btn btn--large btn--brown data-event__button" href="#">kup bilet</a>
                        <div class="data-event__available_tickets">Dostępne bilety: 5</div>
                     </div>
                  </div>
               </div>
            </li>
            <li class="data-event data-day-11 data-category-1 data-event__tour data-event__tour--delay" tabindex="1">
               <time datetime="2013-11-29">
               <span class="day">23</span>
               <span class="weekday">sobota</span>
               </time>
               <div class="data-event__events">
                  <div class="data-event__item data-event__item__delay">
                     <div class="event">
                        <h3>
                           <a href="https://teatrwielki.pl/repertuar/kalendarium/2018-2019/krol-roger/termin/2018-12-11_19-00/">
                              <!--CONTENT_replaced-->Wycieczka indywidualna<!--CONTENT_replaced-->
                           </a>
                           <div class="data-event__hour">
                              9:15
                           </div>
                        </h3>
                     </div>
                     <div class="price data-event__price">
                        <span class="btn btn--large btn--black">Sprzedaż wycieczek ruszy od 14 grudnia 2017</span>
                     </div>
                  </div>
               </div>
            </li>
         </ul>
      </section>
      <div class="tour__side">
         <h2>Pamiętaj</h2>
         <div class="tour__side__item">
            <div class="header"><img src="../images-frogriot/png/icon__tour.png" alt=""><span class="txt">Bilety</span></div>
            <div class="txt">
               Darmowy bilet dla opiekuna<br>
               przysługuje na każde<br>
               10 biletów ulgowych<br><br>
               Bilet ulgowy przysługuje osobom,<br> 
               które nie ukończyły 18 roku życia
            </div>
         </div>
         <div class="tour__side__item">
            <div class="header"><img src="../images-frogriot/png/icon__tour-02.png" alt=""><span class="txt">Poinformuj</span></div>
            <div class="txt">
               Powiedz nam o specjalnych 
               potrzebach uczestników:
               <ul>
                  <li>udogodnienia dla osób niepełnosprawnych</li>
               </ul>
            </div>
         </div>
         <div class="tour__side__item">
            <div class="header"><img src="../images-frogriot/png/icon__tour-03.png" alt=""><span class="txt">Oprowadzanie w języku obcym</span></div>
            <div class="txt">
               Prosimy o wcześniejszy kontakt 
               telefoniczny  
            </div>
         </div>
         <div class="tour__side__item">
            <div class="header"><img src="../images-frogriot/png/icon__tour-04.png" alt=""><span class="txt">Skontakuj się z nami</span></div>
            <div class="txt">
               <a href="mailto:wycieczki@teatrwielki.pl">wycieczki@teatrwielki.pl</a>
               <a href="tel:+48 22 692 02 00">+48 22 692 02 00</a>
            </div>
         </div>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>