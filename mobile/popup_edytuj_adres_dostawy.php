<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">
      <div class="fr-wrapper">

         <!--   -->

         <div class="fr-popup">

            <div class="fr-popup__content">

               <div class="fr-close"></div>

                  <form class="form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>Edytuj adres dostawy</strong></div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                                 <label class="form__label"><strong>NAZWA</strong></label>
                                 <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                                 <label class="form__label"><strong>ULICA</strong></label>
                                 <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                                 <label class="form__label"><strong>NUMER</strong></label>
                                 <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                                 <label class="form__label"><strong>KOD POCZTOWY</strong></label>
                                 <input class="form__input" type="text" />
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                                 <label class="form__label"><strong>MIEJSCOWOŚĆ</strong></label>
                                 <input class="form__input" type="text" />
                           </div>
                        </div>

                           <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label"><strong>PAŃSTWO</strong></label>
                              <select class="form__select">
                                 <option>Polska</option>
                                 <option>Polska</option>
                                 <option>Polska</option>
                              </select>
                           </div>
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn form__btn--white">ANULUJ</a>
                              <a href="#" class="form__btn">ZAPISZ ZMIANY</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>
      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   