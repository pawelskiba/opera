<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">
         <div class="fr-wrapper">
            <div class="fr-container">

               <div class="fr-container__inner">

                  <h1 class="fr-h1">Historia zamówienia z 22 września 2017 r.</h1>
                  
                     <!-- Dane osobowe -->
                     <div class="form__section">

                        <div class="form__section-header"><strong>Dane Kupującego</strong></div>

                        <!-- form row -->
                        <div class="row">
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>IMIĘ</strong></label>
                              <div class="form__output">Maria Anna</div>
                           </div>
                           <div class="form__mb col-xs-12 col-sm-6 form__col--lastname">
                              <label class="form__label form__label--nmb"><strong>NAZWISKO</strong></label>
                              <div class="form__output">Kowalska</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>ADRES E-MAIL</strong></label>
                              <div class="form__output">mariakowalska@gmail.com</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>NUMER TELEFONU</strong></label>
                              <div class="form__output">501 265 897</div>
                           </div>
                        </div>

                     </div>

                     <!-- Adres -->
                     <div class="form__section">

                        <div class="form__section-header"><strong>Adres</strong></div>

                        <!-- form row -->
                        <div class="row">
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                              <div class="form__output">Wrocławska</div>
                           </div>
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                              <div class="form__output">12/13</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="row">
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                              <div class="form__output">156-40</div>
                           </div>
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                              <div class="form__output">Warszawa</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                              <div class="form__output">Polska</div>
                           </div>
                        </div>

                     </div>

                     <!-- Dane do faktury -->
                     <div class="form__section">

                        <div class="form__section-header"><strong>Faktura VAT</strong></div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>NAZWA FIRMY / OSOBA PRYWATNA</strong></label>
                              <div class="form__output">Kovalsky Sp. z o.o</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>NIP</strong></label>
                              <div class="form__output">9900343565</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="row">
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                              <div class="form__output">Wrocławska</div>
                           </div>
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                              <div class="form__output">12/13</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="row">
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                              <div class="form__output">156-40</div>
                           </div>
                           <div class="form__mb col-xs-12 col-sm-6">
                              <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                              <div class="form__output">Warszawa</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                              <div class="form__output">Polska</div>
                           </div>
                        </div>

                     </div>

                     <!-- Dostawa produktów -->
                     <div class="form__section">

                        <div class="form__section-header"><strong>Dostawa produktów</strong></div>

                        <div class="delivery">
                           <img class="delivery__img" src="../ikony/icon-tickets.svg" alt="Bilety" />
                           <div class="deliver__content">
                              <div class="delivery__text"><strong>Bilety przesłano na adres e-mail</strong></div>
                              <div class="delivery__text">mariakowalska@gmail.com</div>
                           </div>
                        </div>

                     </div>

                     <!-- Adres dostawy -->
                     <div class="form__section form__section--no-border form__section--no-margin-bottom">

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                              <div class="form__output">Wrocławska</div>
                           </div>
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                              <div class="form__output">12/13</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                              <div class="form__output">156-40</div>
                           </div>
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                              <div class="form__output">Warszawa</div>
                           </div>
                        </div>

                        <!-- form row -->
                        <div class="form__row">
                           <div class="form__col">
                              <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                              <div class="form__output">Polska</div>
                           </div>
                        </div>

                     </div>

                        <!-- Podsumowanie -->
                        <div class="fr-label fr-summ-header">PODSUMOWANIE ZAMÓWIENIA</div>

                        <!-- bilety -->
                        <div class="fr-summ">

                           <div class="fr-label">Bilety</div>

                           <div class="row">
                              <div class="col-7"><strong>Balet: Jezioro Łabędzie</strong></div>
                              <div class="col-2 text-right"><strong>Ilość</strong></div>
                              <div class="col-3 text-right"><strong>Cena</strong></div>
                           </div>

                           <div class="row fr-mb-15">
                              <div class="col-7">Normalny</div>
                              <div class="col-2 text-right">2</div>
                              <div class="col-3 text-right">60,00 PLN</div>
                           </div>

                           <div class="row">
                              <div class="col-9">Prowizja za obsługę transakcji</div>
                              <div class="col-3 text-right">2,00 PLN</div>
                              <div class="w-100 fr-summ-sepa"></div>
                           </div>

                           <div class="row">
                              <div class="col-9"><strong>Suma</strong></div>
                              <div class="col-3 text-right"><strong>62,00 PLN</strong></div>
                           </div>

                           <div class="row">
                              <div class="col-9">Rabat</div>
                              <div class="col-3 text-right">-20,00 PLN</div>
                              <div class="w-100 fr-summ-sepa ="></div>
                           </div>

                           <div class="row">
                              <div class="col-6"><strong>Suma po rabacie</strong></div>
                              <div class="col-6 text-right"><strong>42,00 PLN</strong></div>
                           </div>

                        

                        <!-- abonamenty -->
                        <div class="fr-summ">

                           <div class="fr-label">Abonamenty</div>

                           <div class="row">
                              <div class="col-7"><strong>Opera i balet: a to Polska właśnie</strong></div>
                              <div class="col-2 text-right"><strong>Ilość</strong></div>
                              <div class="col-3 text-right"><strong>Cena</strong></div>
                           </div>

                           <div class="row">
                              <div class="col-7">Stefa I</div>
                              <div class="col-2 text-right">2</div>
                              <div class="col-3 text-right">500,00 PLN</div>
                              <div class="w-100 fr-summ-sepa"></div>
                           </div>

                           <div class="row">
                              <div class="col-6"><strong>Suma</strong></div>
                              <div class="col-6 text-right"><strong>500,00 PLN</strong></div>
                           </div>

                        </div>

                        <!-- Wycieczki -->
                        <div class="fr-summ">

                           <div class="fr-label">Wycieczki</div>

                           <div class="row">
                              <div class="col-7"><strong>22 Sierpnia 2018, poniedziałek 19:00</strong></div>
                              <div class="col-2 text-right"><strong>Ilość</strong></div>
                              <div class="col-3 text-right"><strong>Cena</strong></div>
                           </div>

                           <div class="row">
                              <div class="col-7">Normalny</div>
                              <div class="col-2 text-right">2</div>
                              <div class="col-3 text-right">60,00 PLN</div>
                           </div>

                           <div class="row">
                              <div class="col-7">Ulgowy</div>
                              <div class="col-2 text-right">2</div>
                              <div class="col-3 text-right">300,00 PLN</div>
                           </div>

                           <div class="row">
                              <div class="col-7">Dla opiekuna</div>
                              <div class="col-2 text-right">1</div>
                              <div class="col-3 text-right">0 PLN</div>
                              <div class="w-100 fr-summ-sepa"></div>
                           </div>

                           <div class="row">
                              <div class="col-6"><strong>Suma</strong></div>
                              <div class="col-6 text-right"><strong>360,00 PLN</strong></div>
                           </div>

                        </div>

                        <!-- Produkty -->
                        <div class="fr-summ fr-summ--last">

                           <div class="fr-label">Produkty</div>

                           <div class="row">
                              <div class="col-7">Miodownik</div>
                              <div class="col-2 text-right">1</div>
                              <div class="col-3 text-right">20,00 PLN</div>
                           </div>

                           <div class="row">
                              <div class="col-7">JEZIORO ŁABĘDZIE '17 - program</div>
                              <div class="col-2 text-right">2</div>
                              <div class="col-3 text-right">45,00 PLN</div>
                           </div>

                           <div class="row">
                              <div class="col-9">Przesyłka kurierska UPC</div>
                              <div class="col-3 text-right">15,00 PLN</div>
                              <div class="w-100 fr-summ-sepa"></div>
                           </div>

                           <div class="row">
                              <div class="col-9"><strong>Suma</strong></div>
                              <div class="col-3 text-right"><strong>65,00 PLN</strong></div>
                              <div class="w-100 fr-summ-sepa fr-summ-sepa--x2"></div>
                           </div>

                        </div>

                        <!-- Zapłacono -->
                        <div class="row">
                           <div class="col-xs-12 col-sm-8 fr-label text-right text-sm-left">Do zapłaty (w tym VAT)</div>
                           <div class="col-xs-12 col-sm-4 fr-label text-right">967,00 PLN</div>
                        </div>
                        
                     </div>

                  

               </div>

            </div>
         </div>
      </div>
   </div>

   <?php include 'include/footer-butik.php' ?>   