<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="mobile-page">
      <div class="fr-popup fr-popup--choose_tickets">
         <div class="fr-popup--choose_tickets__head">
            <div class="fr-popup__close fr-close"></div>
            <div class="fr-popup--choose_tickets__head__left">
               <span class="num">19</span>
               <div class="day">Poniedziałek</div>
            </div>
            <div class="fr-popup--choose_tickets__head__right">
               <div class="name">Indywidualna wycieczka</div>
               <div class="hour">9:15</div>
            </div>
         </div>
         <div class="fr-popup__content">
            <div class="fr-popup__close"></div>
            <form class="fr-form fr-form--popup">
               <h2 class="fr-form__h2">Wybierz bilety</h2>
               <table class="choose_tickets_table">
                  <tr class="fr-form__row">
                     <th class="fr-form__col">
                        <div class="fr-form__col__header">Liczba</div>
                     </th>
                     <th class="fr-form__col">
                        <div class="fr-form__col__header">Cena</div>
                     </th>
                     <th class="fr-form__col"></th>
                  </tr>
                  <tr class="fr-form__row">
                     <td class="fr-form__col">
                        <div class="counter">
                           <div class="counter">
                              <div class="input_div">
                                 <button type="button" onclick="counter_minus(this)"><span></span></button>
                                 <input type="text" size="25" value="1" class="count">   
                                 <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                              </div>
                           </div>
                        </div>
                     </td>
                     <td class="fr-form__col">
                        <div class="fr-popup--choose_tickets__price">
                           30 PLN
                        </div>
                     </td>
                     <td class="fr-form__col">
                        <div class="trash">
                           <img src="../images-frogriot/kosz.png" alt="">
                        </div>
                     </td>
                  </tr>
               </table>
               <div class="fr-form__col">
                  <div class="fr-form__col__header">Typ<img src="../images-frogriot/icon-info.svg" class="icon-info" class="icon-info"" alt=""></div>
               </div>
               <div class="fr-form__col">
                  <select>
                     <option>Normalny</option>
                     <option>Ulgowy</option>
                     <option>Bezpłatny dla opiekuna</option>
                  </select>
               </div>
               <div class="fr-form-row fr-form-row--extra_row">
                  <img src="../images-frogriot/icon_plus.jpg" alt="">Dodaj kolejny bilet
               </div>
               <div class="form__section form__section--last">
                  <div class="form__btns form__btns--vertical">
                     <a href="#" class="form__btn--half-popup btn btn--large btn--white">DALEJ</a>
                     <a href="#" class="form__btn--half-popup btn btn--large btn--brown">ZAMKNIJ</a>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>