<?php include 'include/head.php' ?>
<div id="wrapper">
   <?php include 'include/header.php' ?>    
   <section class="main mobile-page" role="main">
      <div class="abonaments__dates__wrapper">
         <div class="abonaments__dates__head">
            <div class="image">
               <img src="../images-frogriot/abonamenty_header.jpg" alt="">
            </div>
            <div class="txt">
               Opera i balet<br>a to polska<br>właśnie
            </div>
         </div>
         <div class="container">
            <div class="abonaments__dates abonaments__dates--wybrane">
               <div style="background-color: #fff;">
                  <h2 class="abonaments__header__main">
                     <nobr>Liczba abonamentów</nobr>
                     <nobr>i strefa cenowa</nobr>
                  </h2>
                  <div class="abonaments__dates__layout">
                     <div class="abonaments__dates__content">
                        <div class="abonaments__dates__header">
                           <div class="abonaments__dates__header__row">
                              <div class="abonaments__dates__header__col abonaments__dates__header__col--abonaments" style="margin-bottom: 30px">
                                 <!---<span class="txt">Liczba abonamentów</span>-->
                                 <div class="counter">
                                    <div class="counter">
                                       <div class="input_div">
                                          <button type="button" onclick="counter_minus(this)"><span></span></button>
                                          <input type="text" size="25" value="1" class="count">   
                                          <button type="button" onclick="counter_plus(this)"><img src="../ikony/icon-add.svg" alt=""></button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <button class="show__area__view" id="show__area__view">
                              Pokaż strefę cenową na widoku sali
                              </button> 
                              <div class="sala__mobile__switch">
                                 <h2 class="header">Plan sali moniuszki</h2>
                                 <img src="../../images-frogriot/plan_sali_moniuszki.jpg" alt="">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div>
                        <div id="abonaments__content_1"class="abonaments__content">
                           <div class="item__spektakl__summary__container item__spektakl__summary__container--dates">
                              <h2>Daty spektakli</h2>
                              <div class="item__spektakl__summary__items">
                                 <div class="item__spektakl__summary">
                                    <div class="img-box">
                                       <img src="../../images-frogriot/spektakl_thumb.jpg" alt="">
                                    </div>
                                    <div class="content-box">
                                       <div class="header">Jezioro łabędzie</div>
                                       <div class="content">
                                          22 sierpnia 2018, poniedziałek 19:00<br>
                                          Warszawa, sala Moniuszki
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item__spektakl__summary">
                                    <div class="img-box">
                                       <img src="../../images-frogriot/spektakl_thumb-02.jpg" alt="">
                                    </div>
                                    <div class="content-box">
                                       <div class="header">czarodziejski flet</div>
                                       <div class="content">
                                          27 grudnia 2017, środa 19:00<br>
                                          Warszawa, sala Moniuszki
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item__spektakl__summary">
                                    <div class="img-box">
                                       <img src="../../images-frogriot/spektakl_thumb-03.jpg" alt="">
                                    </div>
                                    <div class="content-box">
                                       <div class="header">Dziadek do Orzechów i Król Myszy</div>
                                       <div class="content">
                                          29 grudnia 2017, piątek 19:00<br>
                                          Warszawa, sala Młynarskiego
                                       </div>
                                    </div>
                                 </div>
                                 <div class="item__spektakl__summary">
                                    <div class="img-box">
                                       <img src="../../images-frogriot/spektakl_thumb-04.jpg" alt="">
                                    </div>
                                    <div class="content-box">
                                       <div class="header">Cud albo Krakowiaki i Górale</div>
                                       <div class="content">
                                          27 grudnia 2017, środa 19:00<br>
                                          Warszawa, sala Młynarskiego
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <a href="#" class="btn btn--large btn--brown" style="width: auto; padding-left: 160px; padding-right: 160px;">Dalej</a>
                           </div>
                        </div>
                        <div id="abonaments__content_2" style="display: none" class="abonaments__content">
                           <div class="abonaments__content_room__header">
                              <h2>Plan sali moniuszki</h2>
                           </div>
                           <img src="../images-frogriot/plan_sali_moniuszki.jpg" alt="">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<?php include 'include/footer-butik.php' ?>