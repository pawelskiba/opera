<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">

         <div class="fr-wrapper">

            <!--   -->

            <div class="fr-popup">

               <div class="fr-popup__content">

                  <div class="fr-close"></div>

                  <form class="Form--popup">

                     <div class="form__section form__section--last">

                        <div class="form__section-header"><strong>SESJA WYGASŁA</strong></div>
                        
                        <div class="fr-popup__text">
                        Nie odnotowaliśmy zakupu biletow w ciągu 40 minut.<br/>
                        Ze względów bezpieczeństwa Twoja sesja wygasła.
                        </div>

                        <!-- Przyciski -->
                        <div class="form__section form__section--last">
                           <div class="form__btns">
                              <a href="#" class="form__btn form__btn--white">ZAMKNIJ</a>
                              <a href="#" class="form__btn">Powrót do sklepu</a>
                           </div>
                        </div>

                     </div>

                  </form>

               </div>

            </div>

         </div>

      </div>

      

      <div id="overlay"></div>

   </div>

   <?php include 'include/footer-butik.php' ?>   