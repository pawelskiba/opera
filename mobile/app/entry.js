require('./scss/app.scss');

//-- bootstrap
import {util, collapse, tabs} from 'bootstrap';

import "magnific-popup";

//-- zbędne jsy jeśli obsługa sal będzie inna

require('../../jsy_do_widokow_z_salami/curve.js');
require('../../jsy_do_widokow_z_salami/sale/parter_rows.js');
require('../../jsy_do_widokow_z_salami/sale/amfiteatr_rows.js');
require('../../jsy_do_widokow_z_salami/sale/balkon1.js');
require('../../jsy_do_widokow_z_salami/sale/balkon2.js');
require('../../jsy_do_widokow_z_salami/sale/balkon3.js');

//-- zbędne jsy jeśli obsługa sal będzie inna
//-- tooltipster

require('../../js_wspolne/plugins/tooltipster.bundle.min.js');

/* require('../../js_wspolne/plugins/isotope.js');
require('../../js_wspolne/plugins/horiz.js'); */

$(document).ready(function() {

    $('.image-link').magnificPopup({
        type:'image',
        mainClass: 'mfp-fade',
        gallery:{
            enabled:true
        }
    });

    var $isotope = $('.special__products__items');
    $isotope.isotope({
        layoutMode: 'horiz',
        itemSelector: '.special__products__item',
        getSortData: {
            sortname: '.sortname',
            sortprice: '.sortprice parseInt',
        }
    });

    // bind sort button click
    $('#sortby').on( 'change', function() {

        var sortValue = this.value;
        var sortDirection = $(this).find('option:selected').data('direction'),
            sortBool = true;

        if( sortDirection == 'asc' ){
            sortBool = true;
        }else{
            sortBool = false;
        }

        if( sortValue == 'original-order' ){
            $isotope.isotope({
                sortBy : 'original-order' 
            });
        }else{
            $isotope.isotope({ 
                sortBy: sortValue, sortAscending: sortBool 
            });
        }

    });
    

});

$('.tooltip').tooltipster({
    contentAsHTML:true,
    interactive:true,
    maxWidth: 1168,
    distance:7,
    side: ['top'],
    trigger: 'click',
    functionPosition: function(instance, helper, position){
        position.coord.left = (helper.geo.document.size.width - position.size.width) / 2;
        return position;
    }
 });

 $('.fr-close--tooltip').on('click', function(e){
     e.preventDefault();
     $('.tooltip').tooltipster('close');
 });

 //slick
 require('../../js_wspolne/plugins/slick.js');

 $('.prod-carousel').slick({
    slidesToShow:1,
    slidesToScroll: 1,
    variableWidth: true,
    arrows:false
  });

  $('.tooltip-box').tooltipster({
    side: 'bottom',
    delay: 0,
    trigger: 'click',
    maxWidth: 500,
    minWidth: 290,
});


//show/hide filters
$('.filters-btn').click(function(){
    $(this).toggleClass('open')
    $("#filters").toggleClass('show')
})

$("#show-filters-list").click(function(){
    $('.choose_from_list').toggleClass('show')
})


//-- faktura osoba prywatna/firma
$('#invoice-radioboxes').find('input').change(function(){
    
    var private_person = $('#invoice-privat_person'),
               company = $('#invoice-company');

    if (private_person.is(':checked')) {
        $('.private_invoice').css('display', 'block')
        $('.company_invoice').css('display', 'none')
        $('#nip').css('display', 'none')
    } 
    
    if (company.is(':checked')) {
        $('.private_invoice').css('display', 'none')
        $('.company_invoice').css('display', 'block')
        $('#nip').css('display', 'block')
    } 
})


//-- abonamenty 'więcej'
$('.more-abonament').click(function(e){
    e.preventDefault();
    $(this).parents('.spektakl__item').addClass('expand')
})

$('.less-abonament').click(function(e){
    e.preventDefault();
    $(this).parents('.spektakl__item').removeClass('expand')
})