<?php include 'include/head.php' ?>
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="tours tour__calendar">
   <div class="tours__header">
      <h1>Kalendarium</h1>
      <div class="tour__head">
         <ul id="calendar-date" class="switch">
            <li>
               <span>Sezon 2018/2019</span>    
            </li>
            <li><span>styczeń 2019</span>  </li>
         </ul>
         <div id="calendar-category" class="select clearfix">
            <label>Kategoria:</label>
            <div class="custom-select">
               <span class="selected">Wszystkie</span>
               <select class="select-96223882">
                  <option value="category-0" selected="selected">Wszystkie</option>
                  <option value="category-1">Opera</option>
                  <option value="category-2">Balet</option>
                  <option value="category-3">Koncert</option>
                  <option value="category-4">Wydarzenia specjalne</option>
                  <option value="category-5">W trasie</option>
                  <option value="category-6">Koprodukcje</option>
                  <option value="category-7">Dla dzieci</option>
               </select>
            </div>
         </div>
      </div>
   </div>
   <div class="container">
      <section id="content">
         
        <div class="events">

            <div class="event data-day-1 data-category-1">
                <div class="text">
                    <div class="header clearfix">
                        <div class="date">
                            <time datetime="2013-11-29">
                                <span class="day">1</span>
                                <span class="weekday">piątek</span>
                            </time>
                        </div>
                        <div class="title">
                        <h3><!--CONTENT_replaced-->Tosca<!--CONTENT_replaced--></h3>
                        <h4>
                            <span class="hour">19:00</span> /
                            <span class="category">Opera</span> /
                            <span class="hall">Sala Moniuszki</span>
                        </h4>

                        <p>Giacomo Puccini</p>
                    </div>
                </div>
                <div class="btns">
                    <a class="btn btn--brown" href="#">Kup bilet</a>
                </div>
            </div>
            <div class="event data-day-1 data-category-5">
                <div class="text">
                    <div class="header clearfix">
                        <div class="date">
                            <time datetime="2013-11-29">
                                <span class="day">1</span>
                                <span class="weekday">piątek</span>
                            </time>
                        </div>
                        <div class="title">
                        <h3><!--CONTENT_replaced-->Jezioro łabędzie / Montreal<!--CONTENT_replaced--></h3>
                        <h4>
                            <span class="hour">20:00</span> /
                            <span class="category">W trasie</span> /
                            <span class="hall">Place des Arts, Montreal</span>
                        </h4>
                        <p>Piotr Czajkowski / Krzysztof Pastor</p>
                    </div>
                </div>
                    <div class="btns">
                        <a class="btn btn--brown" href="#">Kup bilet</a>
                    </div>
                </div>
            </div>
            <div class="event data-day-1 data-category-1">
                <div class="text">
                    <div class="header clearfix">
                        <div class="date">
                            <time datetime="2013-11-29">
                                <span class="day">1</span>
                                <span class="weekday">piątek</span>
                            </time>
                        </div>
                        <div class="title">
                        <h3><!--CONTENT_replaced-->Tosca<!--CONTENT_replaced--></h3>
                        <h4>
                            <span class="hour">19:00</span> /
                            <span class="category">Opera</span> /
                            <span class="hall">Sala Moniuszki</span>
                        </h4>

                        <p>Giacomo Puccini</p>
                    </div>
                </div>
                <div class="btns">
                    <a class="btn btn--brown" href="#">Kup bilet</a>
                </div>
            </div>
            <div class="event data-day-1 data-category-5">
                <div class="text">
                    <div class="header clearfix">
                        <div class="date">
                            <time datetime="2013-11-29">
                                <span class="day">1</span>
                                <span class="weekday">piątek</span>
                            </time>
                        </div>
                        <div class="title">
                        <h3><!--CONTENT_replaced-->Jezioro łabędzie / Montreal<!--CONTENT_replaced--></h3>
                        <h4>
                            <span class="hour">20:00</span> /
                            <span class="category">W trasie</span> /
                            <span class="hall">Place des Arts, Montreal</span>
                        </h4>
                        <p>Piotr Czajkowski / Krzysztof Pastor</p>
                    </div>
                </div>
                    <div class="btns">
                        <a class="btn btn--brown" href="#">Kup bilet</a>
                    </div>
                </div>
            </div>        
                            
        </div>

        </section>  
    </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>