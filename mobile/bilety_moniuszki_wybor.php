<?php include 'include/head.php' ?>
<div id="wrapper">
<?php include 'include/header.php' ?>
<section class="main" role="main">
   <div class="mobile-page">
      <div class="fr-popup fr-popup--choose_tickets fr-popup--choose_tickets--tickets">
         <div class="fr-popup__content">
            <div class="fr-popup__close fr-close" style="right: 15px"></div>
            <form class="fr-form fr-form--popup">
               <div class="fr-popup--choose_tickets fr-popup--choose_tickets__place__picture">
                  <img src="../images-frogriot/bilety__mobile__balkon.jpg" alt="">
               </div>
               <div class="fr-popup--choose_tickets fr-popup--choose_tickets__place__details">
                  <div class="details-row details-row--place">
                     <div class="name">
                        Balkon<br>
                        Rząd XI, miejsce 11
                     </div>
                     <div class="price">
                        <div class="regular">10 PLN</div>
                        <div class="disqount">9 PLN</div>
                     </div>
                  </div>
                  <div class="details-row details-row--desc">
                     <div class="icon-box">
                        <img src="../images-frogriot/eye.jpg" alt="">
                     </div>
                     <div class="txt-box">
                        <b>Miejsce z ograniczoną widocznoscią</b>
                        Zdjęcia mają charakter poglądowy. W zależności od inscenizacji oraz wzrostu innych widzów widok na scenę może się różnić.
                     </div>
                  </div>
               </div>
               <div class="fr-form__col fr-form__col--country choose__disqount__control">
                  <label class="fr-form__label"><span>RODZAJ ZNIŻKI</span></label>
                  <select class="fr-form__select">
                     <option>Normalny</option>
                     <option>Ulgowy</option>
                  </select>
               </div>
               <div class="form__section form__section--last">
                  <div class="form__btns form__btns--vertical">
                     <a href="#" class="form__btn--half-popup btn btn--large btn--white">DALEJ</a>
                     <a href="#" class="form__btn--half-popup btn btn--large btn--brown">ZAMKNIJ</a>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
<?php include 'include/footer-butik.php' ?>