<?php include 'include/head.php' ?>

   <div id="wrapper">

      <?php include 'include/header.php' ?>

      <div id="content" class="mobile-page">
         <div class="fr-wrapper">
            <div class="fr-container">

            <div class="fr-container__inner">

               <h1 class="fr-h1">Twoje konto</h1>

               <!-- Dane osobowe -->
               <div class="form__section">

                  <div class="form__section-header">
                     <img src="../ikony/icon-user.svg" alt="ikona" />
                     <strong>Dane osobowe</strong>
                     <span class="form__change">Zmień</span>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <label class="form__label form__label--nmb"><strong>IMIĘ</strong></label>
                        <div class="form__output">Maria Anna</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col form__col--lastname">
                        <label class="form__label form__label--nmb"><strong>NAZWISKO</strong></label>
                        <div class="form__output">Kowalska</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <label class="form__label form__label--nmb"><strong>ADRES E-MAIL</strong></label>
                        <div class="form__output">mariakowalska@gmail.com</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <label class="form__label form__label--nmb"><strong>NUMER TELEFONU</strong></label>
                        <div class="form__output">501 265 897</div>
                     </div>
                  </div>

               </div>

               <!-- Adres -->
               <div class="form__section">

                  <div class="form__section-header">
                     <img src="../ikony/icon-address.svg" alt="ikona" />
                     <strong>Adres</strong>
                     <span class="form__change">Zmień</span>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                        <div class="form__output">Wrocławska</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                        <div class="form__output">12/13</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                        <div class="form__output">156-40</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                        <div class="form__output">Warszawa</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                        <div class="form__output">Polska</div>
                     </div>
                  </div>

               </div>

               <!-- Twoje kody -->
               <div class="form__section form__section--no-border">
                  <div class="form__section-header form__section-header--discount">
                     <img src="../ikony/icon-codes.svg" alt="ikona" />
                     <strong>Twoje kody</strong>
                  </div>
                  <div class="form__output">Kod rabatowy na <strong>20%</strong> na nasze wydawnictwo <strong>501 823</strong></div>
               </div>

               <!-- Zmień hasło -->
               <div class="form__section">
                  <div class="form__section-header">
                     <img src="../ikony/icon-password.svg" alt="ikona" />
                     <strong>Zmień hasło</strong>
                     <span class="form__change">Zmień</span>
                  </div>
               </div>

               <!-- Adres dostawy dom -->
               <div class="form__section form__section--no-border form__section--no-margin-bottom">
                  <div class="form__section-header form__section-header--discount">
                     <img src="../ikony/icon-delivery.svg" alt="ikona" />
                     <strong>Adres dostawy</strong>
                  </div>

                  <div class="form__output form__output--house-work"><strong>Dom</strong><span class="form__change">Zmień</span></div>
                  
                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                        <div class="form__output">Wrocławska</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                        <div class="form__output">12/13</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                        <div class="form__output">156-40</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                        <div class="form__output">Warszawa</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                        <div class="form__output">Polska</div>
                     </div>
                  </div>

               </div>

               <!-- Adres dostawy praca -->
               <div class="form__section">

                  <div class="form__output form__output--house-work"><strong>Praca</strong><span class="form__change">Zmień</span></div>
                  
                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                        <div class="form__output">Wrocławska</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                        <div class="form__output">12/13</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                        <div class="form__output">156-40</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                        <div class="form__output">Warszawa</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <label class="form__label form__label--nmb"><strong>PAŃSTWO</strong></label>
                        <div class="form__output">Polska</div>
                     </div>
                  </div>

                  <a class="fr-add justify-content-center" href="#">
                     <span class="fr-add__icon"></span>
                     Dodaj adres dostawy
                  </a>

               </div>

               <!-- Dane do faktury vat -->
               <div class="form__section">

                  <div class="form__section-header form__section-header--discount">
                     <img src="../ikony/icon-invoice.svg" alt="ikona" />
                     <strong>Dane do faktury VAT</strong>
                  </div>

                  <div class="form__output form__output--house-work"><strong>Dane do faktury 1</strong><span class="form__change">Zmień</span></div>

                  <!-- form row -->
                  <div class="form__row">
                     <div class="form__col">
                        <div class="form__output">Firma / Instytucja</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>NAZWA</strong></label>
                        <div class="form__output">Kovalsky Sp. z o.o</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>NIP</strong></label>
                        <div class="form__output">9900343565</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>ULICA</strong></label>
                        <div class="form__output">Wrocławska</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>NUMER</strong></label>
                        <div class="form__output">12/13</div>
                     </div>
                  </div>

                  <!-- form row -->
                  <div class="row">
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>KOD POCZTOWY</strong></label>
                        <div class="form__output">156-40</div>
                     </div>
                     <div class="form__mb col-xs-12 col-sm-6">
                        <label class="form__label form__label--nmb"><strong>MIEJSCOWOŚĆ</strong></label>
                        <div class="form__output">Warszawa</div>
                     </div>
                  </div>

                  <a class="fr-add justify-content-center" href="#">
                     <span class="fr-add__icon"></span>
                     Dodaj adres dostawy
                  </a>

               </div>

               <!-- Historia zamówień -->
               <div class="form__section form__section--no-border form__section--no-padding-bottom">

                  <div class="form__section-header form__section-header--discount">
                     <img src="../ikony/icon-history.svg" alt="ikona" />
                     <strong>Historia zamówień</strong>
                  </div>

                  <div class="fr-transactions__item">

                     <div class="row">
                        <div class="col-xs-12 col-sm-3 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>DATA</strong></div>
                           <div class="form__output form__output--small">2017/10/21</div>
                        </div>
                        <div class="col-xs-12 col-sm-6 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>NUMER ZAMÓWIENIA</strong></div>
                           <div class="form__output form__output--small">732487423789</div>
                        </div>
                        <div class="col-xs-12 col-sm-3 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>STATUS</strong></div>
                           <div class="form__output form__output--small">Płatność PayU <br/>Opłacono</div>
                        </div>
                     </div>

                     <div class="row">
                        <div class="col-xs-12 col-sm-9 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>PRODUKT</strong></div>
                           <div class="form__output form__output--small">Mój tata tańczy w balecie</div>
                        </div>
                        <div class="col-xs-12 col-sm-3 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>CENA</strong></div>
                           <div class="form__output form__output--small">50,00 PLN <br/>(w tym VAT)</div>
                        </div>
                     </div>

                     <div>
                        <a href="#" class="fr-transactions__btn">ZOBACZ SZCZEGÓŁY ZAMÓWIENIA</a>
                     </div>

                  </div>

                  <div class="fr-transactions__item fr-transactions__item--no-border">

                     <div class="row">
                        <div class="col-xs-12 col-sm-3 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>DATA</strong></div>
                           <div class="form__output form__output--small">2017/10/21</div>
                        </div>
                        <div class="col-xs-12 col-sm-6 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>NUMER ZAMÓWIENIA</strong></div>
                           <div class="form__output form__output--small">732487423789</div>
                        </div>
                        <div class="col-xs-12 col-sm-3 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>STATUS</strong></div>
                           <div class="form__output form__output--small">Płatność PayU <br/>Opłacono</div>
                        </div>
                     </div>

                     <div class="row">
                        <div class="col-xs-12 col-sm-9 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>PRODUKT</strong></div>
                           <div class="form__output form__output--small">Mój tata tańczy w balecie</div>
                        </div>
                        <div class="col-xs-12 col-sm-3 fr-transactions__col">
                           <div class="form__label form__label--small"><strong>CENA</strong></div>
                           <div class="form__output form__output--small">50,00 PLN <br/>(w tym VAT)</div>
                        </div>
                     </div>

                     <div>
                        <a href="#" class="fr-transactions__btn">ZOBACZ SZCZEGÓŁY ZAMÓWIENIA</a>
                     </div>

                  </div>

               </div>

            </div>

            </div>
         </div>
      </div>
   </div>

   <?php include 'include/footer-butik.php' ?>   