var abonaments_count;

$(document).ready(function() {

    if ($(".abonaments__dates__slider").length > 0) {
        
        $(".abonaments__dates__slider").slick({
            infinite: false,
            slidesToShow: 3,
            mobileFirst: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            }, {
                breakpoint: 301,
                settings: {
                    slidesToShow: 1,
                }
            }, {
                breakpoint: 300,
                settings: "unslick"
            }]
        });

    }

    var number;
    var cartItems = [];
    var aa;
    var dataItem;
    var data;

    $('.abonaments__dates__item input').on('click', function(event) {

        if ($('#cart-items .cart__item__row').length == count) {
            if ($(this).is(':checked')) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            }
        }
    });
    $(".abonaments__dates__item input").change(function(e, data) {
        e.preventDefault()

        var that = $(this).parent().attr("data-cart")
        if ($(this).is(':checked')) {

            if ($('#cart-items .cart__item__row').length !== count) {
                addToCart(that)
            }

            $('#cart-items').find(`[data-cart=` + that + `]`).find('.confirm').on('click', function() {
                aa = $(this).parent().attr("data-cart")
            });
            if ($('#cart-items .cart__item__row').length > 0) {
                $('.abonaments__dates__side').css('display', 'block')
            }
            if ($('#cart-items .cart__item__row').length >= abonaments_count) {
                $(".abonaments__dates__side .btn").removeClass('btn--grey').addClass('btn--brown')
                $(".abonaments__dates__side .cart__item__foot__note").css('display', 'none')
                $('.cart__item__foot .btn').removeClass('inactive')
                $('.cart__item__foot .btn').unbind('click.prevent')
            }
            if ($('#cart-items .cart__item__row').length < abonaments_count) {
                $('.cart__item__foot .btn').addClass('inactive')
                $('.cart__item__foot .btn').bind('click.prevent', function(e) {
                    e.preventDefault()
                })
            }
        } else {
            removeFromCart(that)
            if ($('#cart-items .cart__item__row').length < abonaments_count) {
                $(".abonaments__dates__side .btn").removeClass('btn--brown').addClass('btn--grey')
                $(".abonaments__dates__side .cart__item__foot__note").css('display', 'block')
            }
        }
    })

    function addToCart(num) {

        abonaments_count = $('#abonaments__count').val()

        if ($('#cart-items .cart__item__row').length < abonaments_count) {
            $('#cart-items').append(`<div class="cart__item__row" data-cart="` + num + `">
                    <div class="cart__item__row__name">Jezioro Łabędzie</div>    
                    <div class="cart__item__row__date">
                        09 Grudnia 2017, piątek 21:00
                    </div>
                    <button class="cart__item__row__trash confirm"></button>
                </div> `)
            dataItem = num;
            data = num
        }
    }

    function removeFromCart(num) {
        $('#cart-items .cart__item__row').eq(0).remove();
        setTimeout(function() {
            $("#abonaments__content_1").find(`[data-cart=` + num + `]`).find('input').prop('checked', false);
        }, 0)
        if ($('#cart-items .cart__item__row').length < abonaments_count) {
            $(".abonaments__dates__side .btn").removeClass('btn--brown').addClass('btn--grey')
            $(".abonaments__dates__side .cart__item__foot__note").css('display', 'block')
        }
        if ($('#cart-items .cart__item__row').length >= abonaments_count) {
            $('.cart__item__foot .btn').removeClass('inactive')
        }
        $('.cart__item__foot .btn').unbind('click.prevent')
    }
    $("#checkbox1").change(function(e) {
        if ($(this).is(':checked')) {
            $("#abonaments__content_1").css("display", "none")
            $("#abonaments__content_2").css("display", "block")
        } else {
            $("#abonaments__content_1").css("display", "block")
            $("#abonaments__content_2").css("display", "none")
        }
    });

})