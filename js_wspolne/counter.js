var count = 1;

function counter_plus(elem) {
    count = $(elem).parents('.counter').find('.count').val()
    count++;
    $(elem).parents('.counter').find('.count').val(count)
}

function counter_minus(elem) {
    
    count = $(elem).parents('.counter').find('.count').val()
    if (count > 1) { count-- }
    $(elem).parents('.counter').find('.count').val(count)

};
