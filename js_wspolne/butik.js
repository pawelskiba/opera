$(document).ready(function() {

    if ( $('.butik_desc_head__main').length > 0) {
        $('.butik_desc_head__main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.butik_desc_head__thumbs'
        });
    }

    if ( $('.butik_desc_head__thumbs').length > 0) {
        $('.butik_desc_head__thumbs').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.butik_desc_head__main',
            dots: false,
            arrows: false,
            vertical: true,
            focusOnSelect: true
        });
    }    

    if ( $(".butik__main-slider").length > 0) {
        $(".butik__main-slider").slick({
            infinite: false,
            slidesToShow: 1,
            dots: false,
            prevArrow: '<button type="button" class="slick-prev"><img src="../images-frogriot/arrow_white.png"></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="../images-frogriot/arrow_white.png"></button>',
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    infinite: true
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }, {
                breakpoint: 300,
                settings: "unslick" // destroys slick
            }]
        });
    }    

    if ( $(".butik__news__images").length > 0) {
        $(".butik__news__images").slick({
            infinite: false,
            slidesToShow: 3,
            prevArrow: '<button type="button" class="slick-prev butik__news__images__nav"><img src="../images-frogriot/arrow.png"></button>',
            nextArrow: '<button type="button" class="slick-next butik__news__images__nav"><img src="../images-frogriot/arrow.png"></button>',
        });
    }

    if ( $(".special__products__slider").length > 0) {
        $(".special__products__slider").slick({
            infinite: false,
            slidesToShow: 3,
            prevArrow: '<button type="button" class="slick-prev butik__news__images__nav"><img src="../images-frogriot/arrow.png"></button>',
            nextArrow: '<button type="button" class="slick-next butik__news__images__nav"><img src="../images-frogriot/arrow.png"></button>',
        });
    }

    if ( $(".butik__news__slider").length > 0) {
        $(".butik__news__slider").slick({
            infinite: false,
            slidesToShow: 1,
            prevArrow: '<button type="button" class="slick-prev"><img src="../images-frogriot/arrow.png"></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="../images-frogriot/arrow.png"></button>',
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    infinite: true
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }, {
                breakpoint: 300,
                settings: "unslick" // destroys slick
            }]
        });
    }    

    if ( $('.butik_desc_head__main').length > 0) {
        $('.butik_desc_head__main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            prevArrow: '<button type="button" class="slick-prev"><img src="../images-frogriot/arrow.png"></button>',
            nextArrow: '<button type="button" class="slick-next"><img src="../images-frogriot/arrow.png"></button>',
        });
    }    

})