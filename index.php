
<!doctype html>
<html class="no-js" lang="pl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>index</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&amp;subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="desktop/css/index.css">
    </head>
    <body>

        <div class="page">

            <div class="index">
                <header class="index__top">
                    <div class="index__container">
                        <img src="images-frogriot/logo-frogriot.svg" alt="Logo Frogriot" />
                        <p class="index__label">PROJEKT</p>
                        <h1 class="index__name">Opera Narodowa</h1>
                    </div>
                </header>

                <div class="index__content">
                    <div class="index__container">
                        <table class="i-table i-table--list">
                            <tr class="i-table">
                                <th class="i-table__th"><span class="i-table__name">Nazwa</span></th>
                                <th class="i-table__th i-table__th--desktop"><img src="images-frogriot/desktop.svg" alt="Desktop" /></th>
                                <th class="i-table__th"><span class="i-table__name">Nazwa</span></th>
                                <th class="i-table__th i-table__th--mobile"><img src="images-frogriot/mobile.svg" alt="Mobile" /></th>
                            </tr>

                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">1.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">1.</span>
                                    <a class="i-table__link" href="mobile/bilety_nienumerowane.php">Bilety nienumerowane</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">2.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">2.</span>
                                    <a class="i-table__link" href="mobile/bilety_numerowane.php">Bilety numerowane</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">3.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">3.</span>
                                    <a class="i-table__link" href="mobile/bilety_sala_moniuszki_sektor.php">Sala moniuszki - sektor</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">4.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">4.</span>
                                    <a class="i-table__link" href="mobile/bilety_numerowane_mlynarskiego.php">Sala młynarskiego</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">5.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">5.</span>
                                    <a class="i-table__link" href="mobile/bilety_numerowane_redutowe.php">Sala Redutowe</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">6.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">6.</span>
                                    <a class="i-table__link" href="mobile/bilety_moniuszki_wybor.php">Bilety moniuszki - wybór</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">7.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">7.</span>
                                    <a class="i-table__link" href="mobile/bilety_filtry.php">Bilety moniuszki - sektop - wybrane</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">8.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">8.</span>
                                    <a class="i-table__link" href="mobile/bilety_filtry.php">Bilety - filtry</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">9.</span>
                                    <a class="i-table__link" href="desktop/bilety.php">Bilety</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">9.</span>
                                    <a class="i-table__link" href="mobile/bilety_filtry.php">Bilety moniuszki - sektor - wybrane</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">10.</span>
                                    <a class="i-table__link" href="desktop/abonamenty_daty.php">Abonamenty - daty</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">10.</span>
                                    <a class="i-table__link" href="mobile/abonamenty_daty.php">Abonamenty - daty</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9"
                                    alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">10.</span>
                                    <a class="i-table__link" href="desktop/abonamenty_daty_2.php">Abonamenty - data okreslona</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">10.</span>
                                    <a class="i-table__link" href="mobile/abonamenty_daty_2.php">Abonamenty - data okreslona</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9"
                                    alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">11.</span>
                                    <a class="i-table__link" href="desktop/abonamenty.php">Abonamenty</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">11.</span>
                                    <a class="i-table__link" href="mobile/abonamenty.php">Abonamenty</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>

                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">12.</span>
                                    <a class="i-table__link" href="desktop/wycieczki.php">Wycieczki</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">12.</span>
                                    <a class="i-table__link" href="mobile/wycieczki.php">Wycieczki</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">13.</span>
                                    <a class="i-table__link" href="desktop/wycieczki-kalendarium.php">Wycieczki - kalendarium</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">13.</span>
                                    <a class="i-table__link" href="mobile/wycieczki-kalendarium.php">Wycieczki - kalendarium</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            
                            
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">14.</span>
                                    <a class="i-table__link" href="desktop/butik-nowy/">Butik</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">14.</span>
                                    <a class="i-table__link" href="mobile/butik-nowy/mobile/">Butik</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                           
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">15.</span>
                                    <a class="i-table__link" href="desktop/butik_opis_skrocony.php">Butik - opis skrócony</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">15.</span>
                                    <a class="i-table__link" href="mobile/butik_opis_skrocony.php">Butik - opis skrócony</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">16.</span>
                                    <a class="i-table__link" href="desktop/butik_opis_rozszerzony.php">Butik - opis rozszerzony</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">16.</span>
                                    <a class="i-table__link" href="mobile/butik_opis_rozszerzony.php">Butik - opis rozszerzony</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">17.</span>
                                    <a class="i-table__link" href="desktop/butik_wyszukiwarka_wyniki.php">Butik - wyniki wyszukiwania</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">17.</span>
                                    <a class="i-table__link" href="mobile/butik_wyszukiwarka_wyniki.php">Butik - wyniki wyszukiwania</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>

                            
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">18.</span>
                                    <a class="i-table__link" href="desktop/wycieczki_liczba_osob_normal.php">Popup wycieczki -normalny</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">18.</span>
                                    <a class="i-table__link" href="mobile/wycieczki_liczba_osob_normal.php">Popup wycieczki -normalny</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">19.</span>
                                    <a class="i-table__link" href="desktop/wycieczki_liczba_osob_ulgowy.php">Popup wycieczki -ulgowy</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">19.</span>
                                    <a class="i-table__link" href="mobile/wycieczki_liczba_osob_ulgowy.php">Popup wycieczki -ulgowy</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">20.</span>
                                    <a class="i-table__link" href="desktop/wycieczki_liczba_osob_bezplatny.php">Popup wycieczki -bezplatny</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">20.</span>
                                    <a class="i-table__link" href="mobile/wycieczki_liczba_osob_bezplatny.php">Popup wycieczki -bezplatny</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            
                            
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">21.</span>
                                    <a class="i-table__link" href="desktop/historia_zamowienia.php">Historia_zamowienia</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">21.</span>
                                    <a class="i-table__link" href="mobile/historia_zamowienia.php">Historia_zamowienia</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">22.</span>
                                    <a class="i-table__link" href="desktop/rejestracja.php">Rejestracja</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">22.</span>
                                    <a class="i-table__link" href="mobile/rejestracja.php">Rejestracja</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">23.</span>
                                    <a class="i-table__link" href="desktop/twoje_konto.php">Twoje konto</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">23.</span>
                                    <a class="i-table__link" href="mobile/twoje_konto.php">Twoje konto</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">24.</span>
                                    <a class="i-table__link" href="desktop/popup_zmien_haslo.php">Popup zmień hasło</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">24.</span>
                                    <a class="i-table__link" href="mobile/popup_zmien_haslo.php">Popup zmień hasło</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">25.</span>
                                    <a class="i-table__link" href="desktop/popup_przypomnij_haslo.php">Popup przypomnij hasło</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">25.</span>
                                    <a class="i-table__link" href="mobile/popup_przypomnij_haslo.php">Popup przypomnij hasło</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">26.</span>
                                    <a class="i-table__link" href="desktop/popup_logowanie.php">Logowanie</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">26.</span>
                                    <a class="i-table__link" href="mobile/popup_logowanie.php">Logowanie</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">27.</span>
                                    <a class="i-table__link" href="desktop/popup_edytuj_adres_dostawy.php">Popup edytuj adres dostawy</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">27.</span>
                                    <a class="i-table__link" href="mobile/popup_edytuj_adres_dostawy.php">Popup edytuj adres dostawy</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">28.</span>
                                    <a class="i-table__link" href="desktop/popup_powiadom_o_dostepnosci_biletow-1.php">Popup powiadom o dostepnosci biletow-1</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">28.</span>
                                    <a class="i-table__link" href="mobile/popup_powiadom_o_dostepnosci_biletow-1.php">Popup powiadom o dostepnosci biletow-1</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">29.</span>
                                    <a class="i-table__link" href="desktop/popup_powiadom_o_dostepnosci_biletow-2.php">Popup powiadom o dostepnosci biletow-2</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">29.</span>
                                    <a class="i-table__link" href="mobile/popup_powiadom_o_dostepnosci_biletow-2.php">Popup powiadom o dostepnosci biletow-2</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">30.</span>
                                    <a class="i-table__link" href="desktop/popup_sesja_wygasla.php">Popup sesja wygasla</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">30.</span>
                                    <a class="i-table__link" href="mobile/popup_sesja_wygasla.php">Popup sesja wygasla</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">31.</span>
                                    <a class="i-table__link" href="desktop/popup_usuniecie_z_koszyka.php">Popup usuniecie z koszyka</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">31.</span>
                                    <a class="i-table__link" href="mobile/popup_usuniecie_z_koszyka.php">Popup usuniecie z koszyka</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">32.</span>
                                    <a class="i-table__link" href="desktop/dostawa_faktura.php">Dostawa faktura</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">32.</span>
                                    <a class="i-table__link" href="mobile/dostawa_faktura.php">Dostawa faktura</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>

                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">33.</span>
                                    <a class="i-table__link" href="desktop/dostawa_faktura_blad.php">Dostawa faktura błąd</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">33.</span>
                                    <a class="i-table__link" href="mobile/dostawa_faktura_blad.php">Dostawa faktura błąd</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">34.</span>
                                    <a class="i-table__link" href="desktop/koszyk.php">Koszyk</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">34.</span>
                                    <a class="i-table__link" href="mobile/koszyk.php">Koszyk</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">35.</span>
                                    <a class="i-table__link" href="desktop/koszyk_abonamenty.php">Koszyk abonamenty</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">35.</span>
                                    <a class="i-table__link" href="mobile/koszyk_abonamenty.php">Koszyk abonamenty</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">36.</span>
                                    <a class="i-table__link" href="desktop/koszyk_full.php">Koszyk full</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">36.</span>
                                    <a class="i-table__link" href="mobile/koszyk_full.php">Koszyk full</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">37.</span>
                                    <a class="i-table__link" href="desktop/koszyk_full_edycja_wsparcie.php">Koszyk full edycja wapracie</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">37.</span>
                                    <a class="i-table__link" href="mobile/koszyk_full_edycja_wsparcie.php">Koszyk full edycja wapracie</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">38.</span>
                                    <a class="i-table__link" href="desktop/podsumowanie_i_platnosc.php">Podsumowanie i płatność</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">38.</span>
                                    <a class="i-table__link" href="mobile/podsumowanie_i_platnosc.php">Podsumowanie i płatność</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">39.</span>
                                    <a class="i-table__link" href="desktop/podsumowanie_i_platnosc_ostrzezenie.php">Podsumowanie i płatność ostrzeżenie</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">39.</span>
                                    <a class="i-table__link" href="mobile/podsumowanie_i_platnosc_ostrzezenie.php">Podsumowanie i płatność ostrzeżenie</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">40</span>
                                    <a class="i-table__link" href="desktop/podsumowanie_i_platnosc_ostrzezenie.php">Podsumowanie i płatność ostrzeżenie</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">40.</span>
                                    <a class="i-table__link" href="mobile/podsumowanie_i_platnosc_ostrzezenie.php">Podsumowanie i płatność ostrzeżenie</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">41.</span>
                                    <a class="i-table__link" href="desktop/strona_podziekowania.php">Strona podziękowania</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">41.</span>
                                    <a class="i-table__link" href="mobile/strona_podziekowania.php">Strona podziękowania</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">42.</span>
                                    <a class="i-table__link" href="desktop/przyjecie_zamowienia.php">Przyjęcie zamówienia</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">42.</span>
                                    <a class="i-table__link" href="mobile/przyjecie_zamowienia.php">Przyjęcie zamówienia</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">43.</span>
                                    <a class="i-table__link" href="desktop/przyjecie_zamowienia_udany_zakup.php">Przyjęcie zamówienia - udany zakup</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">43.</span>
                                    <a class="i-table__link" href="mobile/przyjecie_zamowienia_udany_zakup.php">Przyjęcie zamówienia - udany zakup</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">44.</span>
                                    <a class="i-table__link" href="desktop/strona_podziekowania_brak_zaplaty.php">Strona podziękowania brak zapłaty</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">44.</span>
                                    <a class="i-table__link" href="mobile/strona_podziekowania_brak_zaplaty.php">Strona podziękowania brak zapłaty</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">45.</span>
                                    <a class="i-table__link" href="desktop/strona_podziekowania_brak_zaplaty.php">Strona podziękowania brak zapłaty</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">45.</span>
                                    <a class="i-table__link" href="mobile/strona_podziekowania_brak_zaplaty.php">Strona podziękowania brak zapłaty</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">46.</span>
                                    <a class="i-table__link" href="desktop/wycieczki-kalendarium-2.php">kalendarium 2</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">46.</span>
                                    <a class="i-table__link" href="mobile/wycieczki-kalendarium-2.php">kalendarium 2</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>

                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">47.</span>
                                    <a class="i-table__link" href="desktop/wycieczki-kalendarium.php">kalendarium</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">47.</span>
                                    <a class="i-table__link" href="mobile/wycieczki-kalendarium.php">kalendarium</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>
                            
                            <!-- item -->
                            <tr>
                                <td class="i-table__td">
                                    <span class="i-table__num">48.</span>
                                    <a class="i-table__link" href="desktop/butik_kategoria.php">Butik kategoria</a>
                                </td>
                                <td class="i-table__td i-table__td--desktop">
                                    <img src="images-frogriot/tick.svg" alt="Tick" />
                                </td>
                                <td class="i-table__td">
                                    <span class="i-table__num">48.</span>
                                    <a class="i-table__link" href="mobile/butik_kategoria.php">Butik kategoria</a>
                                </td>
                                <td class="i-table__td i-table__td--mobile">
                                    <img src="images-frogriot/tick.svg" width="12" height="9" alt="Tick" />
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>

            </div>

        </div>
    </body>
</html>
