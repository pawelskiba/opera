var path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    entry: {
        mobile: './mobile/app/entry.js',
        desktop: './desktop/app/entry.js',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].min.js',
    },
    module:{
        rules:[
            {
                test: require.resolve('jquery'),
                use: [{
                    loader: 'expose-loader',
                    options: 'jQuery'
                },{
                    loader: 'expose-loader',
                    options: '$'
                }]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader', 'postcss-loader', 'sass-loader',
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules$/,
                query:{
                    presets: ['@babel/preset-env'],
                }
            },
            {test: /\.png|jpg|gif$/, loader: 'file-loader' },
            {test: /\.svg|woff|woff2|ttf|eot|otf(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].min.css",
        }),
        new LiveReloadPlugin(),
    ],
    watch:true,
    devtool: 'source-map',
};
